
Gesloten Dagen
--------------

Met gesloten dagen worden de dagen aangegeven waarop de studiezaal gesloten is.

Voor het definiëren wordt een paneel aan de rechterzijde geopend met een tabel van gesloten dagen. Een gesloten dag wordt gedefinieerd via de volgende velden:

-  **Beschrijving**: Beschrijving van de gesloten dag.
-  **Datum**: Datum van sluiting.

**Kopieer gesloten dagen**

Met het **kopieer gesloten dagen** pictogram bovenaan de lijst worden de gesloten dagen van een jaar gekopieerd naar een aan te geven jaar. Dit geeft een scherm waarin het te kopiëren jaar geselecteerd en het doeljaar aangegeven worden.

**Filteren**

De lijst van gesloten dagen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een gesloten dag wordt toegevoegd door in de tabel op het **+** pictogram (nieuwe toevoegen) te klikken. Hiermee komt een nieuwe regel beschikbaar waarin de gegevens van de gesloten dag ingevoerd worden. Met het **potlood** pictogram komt de regel beschikbaar waarin de gegevens van de gesloten dag gewijzigd worden.

**Sorteren**
De lijst van gesloten dagen kan oplopend of aflopend worden gesorteerd op de velden Beschrijving of Datum via de knopjes voor oplopend/aflopend sorteren.

**Verwijderen**

Met het **prullenbak** pictogram wordt de gesloten dag, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__