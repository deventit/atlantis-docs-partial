
Invoer soorten nadere toegangen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Metadata model**: Hier kan met metadata model voor de soort nadere toegang worden geselecteerd. Dit metadata model bepaalt de standaard velden die in de soort nadere toegang beschikbaar zullen zijn.
-  **Beschrijving**: Beschrijving van de soort nadere toegang.
-  **Openbaarheidsbeperking**: selecteer hier de standaard openbaarheidsbeperking.
-  **Aanvraagbaar**: Geeft aan of objecten van deze soort nadere toegang aangevraagd of gereserveerd kunnen worden.
-  **Bestelbaar**: Geeft aan of voor objecten van deze soort nadere toegang bestellingen van reproducties kunnen worden gedaan.

Vervolgens kan voor maximaal 20 vrije velden worden aangegeven of en hoe deze beschikbaar zijn voor invoer en wijzigen. Met het **+** pictogram wordt een regel voor een vrij veld toegevoegd. Een vrij veld wordt gewijzigd door direct in het desbetreffende veld in de tabel te klikken. Met het **prullenbak** pictogram wordt het vrije veld verwijderd.

Voor het invoeren van een vrij veld zijn de onderstaande velden beschikbaar:

-  **Veldnummer**: Selecteer hier het nummer van het veld.
-  **Veldnaam**: De benaming van het vrije veld.
-  **Veldtype**: Hier kan het veldtype worden geselecteerd. Dit geeft de keuze uit:
    - Getal
    - Jaar
    - Datum
    - Ja/nee
    - Tekst

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__