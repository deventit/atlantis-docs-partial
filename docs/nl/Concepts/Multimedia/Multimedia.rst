
Bestanden
~~~~~~~~~

Met bestanden wordt ieder type bestand bedoeld. Dit kunnen digitale afbeeldingen, audio, video, Word documenten, Excel sheets, PDF bestanden of wat voor bestandstype dan ook zijn. Bestanden worden gerepresenteerd door metadata modellen waarbij op de relatie expliciet is aangegeven dat het een multimediale relatie betreft. 

Als er een relatie is tussen een metadata model en een metadatamodel voor Bestanden, dan kunnen bestanden gekoppeld worden, zonder beperking in type, omvang of aantal.

**Gekoppelde bestanden**

Bestanden die gekoppeld worden aan een object worden 1-op-1 opgeslagen in de database of in een gekoppeld het E-Depot. Vervolgens zal hier een icoon voor worden bepaald.

**Iconen**

Een Bestand wordt gerepresenteerd door een icoon. Afhankelijk van het soort bestand wordt het icoon bepaald als volgt:

1. Is het een digitale afbeelding, dan is het icoon de afbeelding in een lage resolutie, bv. 200x200 pixels. Welke resolutie dit zal zijn wordt bepaald via het "Presentieprofiel".

2. Is het geen digitale afbeelding, dan wordt gekeken naar het type bestand via de extensie van het bestand (bv. doc, docx, pdf, mp4, mov, xls, xlsx, etc.). Voor de extensie wordt nagegaan of er een registratie van een icoon voor dit type bestand is geregistreerd in de "Mediatypes". Als dat zo is, dan wordt dit icoon gekozen. Is dat er niet, dan wordt nagegaan of er een registratie in de Mediatypes is waarbij is aangegeven dat het een standaard icoon is, is dat het geval dan wordt dit icoon gekozen.

3. Leidt bovenstaande niet tot een icoon, dan wordt het icoon van het metadata model gekozen als icoon voor het multimediale bestand.

Is in de licentie de dienst "Digitale informatiebronnen" aanwezig, dan zal voor PDF-bestanden de eerste pagina van het PDF-bestand als icoon genomen worden.

Is het Bestand auteursrechtelijk beschermd en betreft het presentatie op het Internet via de publieksomgeving of een extern gekoppelde omgeving, dan wordt een auteursrechterlijk beschermd icoon gepresenteerd. Dit icoon wordt ingesteld bij een metadata model. Als deze niet is ingesteld, dan wordt het icoon van het standaard mediatype gepresenteerd

**Uitwerking binnen de Atlantis beheeromgeving**

In de Altantis beheeromgeving worden met een object gekoppelde bestanden gepresenteerd in een tabblad "Bestanden" bij de detailinformatie van een object. Dit presenteert de een lijst van iconen, voor ieder gekoppeld bestand één icoon. 

Het aanklikken van een icoon zal deze "afspelen". Hoe er afgespeeld wordt, is afhankelijk van het type bestand. Voor een afbeelding zal een beeldviewer gepresenteerd worden, voor audio/video een mediaplayer en voor andere bestanden een mogelijkheid om te downloaden.

Is in de licentie de dienst "Digitale informatiebronnen" aanwezig, dan wordt voor PDF-bestanden een geïntegreerde PDF-viewer gepresenteerd.

Op iedere pagina waar groepen van objecten worden gepresenteerd zal, indien een object ook gekoppelde bestanden heeft, voor een object het icoon van het eerste gekoppelde bestand worden gepresenteerd. Dit betreffen bv. zoekresultaten, werksets, selecties, gekoppelde objecten, etc. 

Als een object geen gekoppelde bestanden heeft maar wel een toegestane relatie met bestanden, zal het icoon van het metadata model worden gepresenteerd. Indien deze niet beschikbaar is, wordt het icoon van het standaard mediatype gepresenteerd.