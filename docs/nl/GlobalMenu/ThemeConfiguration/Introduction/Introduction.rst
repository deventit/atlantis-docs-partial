
Thema Configuratie
~~~~~~~~~~~~~~~~~~

Beheer Thema Configuraties
--------------------------

In dit paneel worden de Thema's gedefinieerd. Een Thema is een definitie van o.a. het gebruik van kleuren en lettertype binnen de applicatie.

**Filteren**

De lijst van Thema's kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een Thema wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van het Thema ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van het Thema gewijzigd worden.

**Standaard Thema**

Het standaard Thema wordt aangeduid met een **groene vink**. Dit thema kan niet verwijderd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt het Thema, na bevestiging, verwijderd. 

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__