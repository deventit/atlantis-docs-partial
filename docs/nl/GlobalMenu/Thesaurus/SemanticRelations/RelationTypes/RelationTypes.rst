
Relatie Types
^^^^^^^^^^^^^

Relatie types definieert de beschikbare relatie types voor een relatie type.

**Relatie types**

Het scherm voor de relatie types presenteert een paneel aan de linkerzijde. Dit geeft een overzicht van de beschikbare relatie types. 

**Filteren**

De lijst met relatie types kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. 

**Toevoegen** 

Met het **toevoegen** pictogram boven de lijst met relatie types wordt een nieuwe relatie type toegevoegd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de relatie type wordt weergegeven voor invoer.

**Relatie type labels** 

Met het **oog** pictogram naast de naam van een relatie type worden de relatie type labels gepresenteerd voor beheer.

**Wijzigen** 

Een relatie type wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de tabel waar de gegevens van het relatie type ingevoerd worden. Met het **potlood** pictogram komt de regel voor wijziging beschikbaar. 

De volgende velden zijn beschikbaar:

-  **Beschrijving**: De beschrijving van het relatie type.

Met het **potlood** pictogram naast de naam van een relatie type wordt een relatie type gewijzigd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de relatie type wordt weergegeven voor mutatie.

**Verwijderen** 

Met het **prullenbak** pictogram wordt een relatie type verwijderd. Dit kan alleen als de relatie type niet in gebruik is.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__