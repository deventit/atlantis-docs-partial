
Statussen
---------

Beheer Statussen
^^^^^^^^^^^^^^^^

Met statussen worden beperkingen gedefinieerd met betrekking tot openbaarheid, bestellingen en aanvragen en combinaties hiervan. Aan objecten kunnen statussen worden gekoppeld waarmee deze beperkingen op die objecten van toepassing zijn.

**Statussen**

Het scherm voor de statussen presenteert een paneel aan de linkerzijde. Dit geeft een overzicht van de beschikbare statussen. 

**Filteren**

De lijst met statussen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. 

**Toevoegen** 

Met het **toevoegen** pictogram boven de lijst met statussen wordt een nieuwe status toegevoegd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de status wordt weergegeven voor invoer.

**Wijzigen** 

Met het **potlood** pictogram naast de naam van een status wordt een status gewijzigd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de status wordt weergegeven voor mutatie.

**Verwijderen** 

Met het **prullenbak** pictogram wordt een status verwijderd. Dit kan alleen als de status niet in gebruik is.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__