
Configuratie Cockpit
~~~~~~~~~~~~~~~~~~~~

De Configuratie Cockpit is dé omgeving voor de applicatie- en functioneel beheerders. Hiermee wordt een Atlantis omgeving ingericht en geconfigureerd.

Kenmerkend voor Atlantis is de flexibiliteit voor inrichting en configuratie. Als een, in de basis, generiek multimediaal informatiesysteem, wordt via inrichting en configuratie een omgeving naar de smaak en wensen van een gebruikersorganisatie op maat gemaakt.

De Configuratie Cockpit wordt gestart vanuit de beheeromgeving van Atlantis met het gebruikersaccount van de dan ingelogde gebruiker. Deze dient daarom voldoende rechten te hebben om te mogen configureren en in te richten.