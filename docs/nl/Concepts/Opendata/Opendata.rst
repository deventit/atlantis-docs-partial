
Open data
~~~~~~~~~

Open data is de dienst waarmee selecties van objecten beschikbaar gesteld worden in de vorm van bestanden. Open data wordt beschikbaar gesteld via een Open data portaal op de publieksomgeving. Welke objecten in wat voor opmaak en bestandsformaat beschikbaar worden gesteld is te configureerbaar. Het Open data portaal geeft naast de downloadbare bestanden ook een mogelijkheid om eerst een selectie van de gewenste objecten te maken.

**Sets**

Een Open data bestand wordt samengesteld op basis van een set. De set bevat een verwijzing naar een dataset. Een dataset maakt op basis van een database query een selectie van objecten.

**Formaten**

Voor een Open data selectie, de bestandsformaten en opmaak daarvan wordt gedefinieerd door het aangeven van de extensie van het bestand en XSLT expressies voor kop, de opmaak van de objecten en een voet. Dit wordt aangeboden via XSLT Editors.

**Selectie voor download**

In het Open data portaal in de publieksomgeving kan voor een Open data download een selectie gedaan worden op de objecten die voor Open data aangeboden worden. Deze selectie gebeurt via de zoekvelden die zijn ingericht voor het publiek op de betreffende objecten.




