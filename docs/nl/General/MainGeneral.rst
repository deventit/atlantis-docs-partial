Algemene bedieningselementen
****************************

**Algemeen**

Binnen de Configuratie Cockpit komen een aantal bedieningselementen steeds terug. Deze worden hier beschreven zodat dit niet steeds voor iedere pagina herhaald hoeft te worden.

**Hoofdbalk**

Boven ieder scherm zit de hoofdbalk. Deze biedt de volgende mogelijkheden:

-  **Afbeelding**: deze zit aan de linkerkant. Door hierop te klikken wordt naar het Dashboard scherm gegaan. De afbeelding wordt ingesteld door het toegepaste thema.
-  **Pin-knop**: Dit toont en lijst van vastgepinde schermen. Een scherm kan hiermee direct worden opgeroepen. Schermen bevatten eveneens een **pin-knop** waarmee het scherm aan deze lijst wordt toegevoegd. Met de **prullenbak** kan een scherm uit deze lijst verwijderd worden.
-  **Tandwiel**: dit geeft de systeeminstellingen. Dit zijn standaard instellingen die over de gehele Atlantis omgeving van toepassing zijn.
-  **Gebruikersnaam met avatar**: dit toont gebruikersnaam van de ingelogde gebruiker. Door hierop te klikken kan de gebruiker haar profiel bijwerken of uitloggen.
-  **Globale menu**: deze zit aan de rechterkant (de vier vierkantjes). Hiermee wordt het globale menu opgeroepen met alle overige functies van de Configuratie Cockpit.


**Schermen en panelen**

Schermen bestaan vaak uit verticale panelen. Een paneel (met uitzondering van het meest linkse paneel) kan gesloten worden met de **x** knop. Als in het linkerpaneel een functie gekozen wordt die een rechterpaneel opent, dan zal het linkerpaneel doorgaans automatisch inklappen. Als het rechterpaneel gesloten wordt (met de **x** knop), dan zal het linkerpaneel automatisch weer openen. Panelen worden handmatig in- en uitgeklapt via de **<** en **>** knoppen.

Ook een scherm voor invoer en mutatie kan gesloten worden met de **x** knop. Als er nog niet bewaarde invoer is zal gevraagd worden of op het paneel of scherm gebleven moet worden om de invoer alsnog te bewaren.

Als een paneel of scherm verlaten wordt zonder te bewaren zal gevraagd worden of op het paneel of scherm gebleven moet worden om de invoer alsnog te bewaren.

Invoer in panelen, schermen en tabellen worden bewaard met de **Opslaan** knop. Met de **Reset** knop worden de veldwaarden teruggebracht naar de waarden bij het openen of de laatst opgeslagen waarden.

In panelen en schermen worden knoppen aangeboden voor het toevoegen, wijzigen en verwijderen als volgt:

- Toevoegen via een **+** pictogram. 
- Muteren via een **Potlood** pictogram.
- Verwijderen via een **prullenbak** pictogram. Na bevestiging wordt de invoer verwijderd.

**Tooltips**
Door de muis over een knop te laten bewegen wordt een verduidelijkende tekst getoond van de betekenis van de knop. Op een tablet of telefoon komt deze tekst beschikbaar door de knop enige tijd ingedrukt te houden.

**Help pop-up**

Als er een **i** knop beschikbaar is, dan wordt daarmee de help voor het desbetreffende paneel of scherm opgeroepen via een pop-up scherm. In het pop-up scherm staat ook een **i** knop. Daarmee wordt de volledige help van de Configuratie Cockpit opgeroepen met alle helponderwerpen. Hier kan ook in gezocht worden.

Verder biedt de help-pop-up de volgende functies:

-  **Minimaliseer-knop**: hiermee wordt de help-pop-up geminimaliseerd. Met de **+** knop die dan verschijnt wordt de help pop-up weer zichtbaar gemaakt.
-  **Potloodje**:(indien de gebruiker voldoende rechten heeft): hiermee kan de help worden aangepast.
-  **Klant-hulp knop**: hiermee wordt de eigen toegevoegde helptekst voor dit scherm of paneel opgeroepen. Indien de gebruiker voldoende rechten heeft kan deze help worden samengesteld.

Als op het **potloodje** geklikt is, dan kunnen de helpteksten aangepast worden. Met **Opslaan** worden de aanpassingen opgeslagen. Met **Annuleren** worden de wijzigingen geannuleerd. Er komt dan ook een **Algemene hulp** knop beschikbaar waarmee naar de algemene help teruggekeerd wordt.

De gebruiker kan via de **klant-hulp**/**algemene-hulp** knop steeds heen en weer schakelen tussen deze helpteksten.