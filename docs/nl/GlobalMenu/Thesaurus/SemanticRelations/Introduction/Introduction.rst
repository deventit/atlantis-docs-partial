
Semantische Relaties
--------------------

Beheer Semantische Relaties
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Semantische relaties definieert de beschikbare semantische relaties binnen de thesauri. Er kunnen vrij semantische relaties gedefinieerd worden, deze dienen echter altijd van het type Ouder/kind (hiërarchisch), Equivalent (= gelijkwaardig) of Transitief te zijn. Deze relatie kan vervolgens diverse namen gegeven worden en per naam voor iedere taal benoemd worden voor beide zijden van de relatie.

**semantische relaties**

Het scherm voor de semantische relaties presenteert een paneel aan de linkerzijde. Dit geeft een overzicht van de beschikbare semantische relaties. 

**Filteren**

De lijst met semantische relaties kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. 

**Toevoegen** 

Met het **toevoegen** pictogram boven de lijst met semantische relaties wordt een nieuwe semantische relatie toegevoegd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de semantische relatie wordt weergegeven voor invoer.

**Relatietypen** 

Met het **relatietypen** pictogram naast de naam van een semantische relatie worden de relatietypen gepresenteerd met de daarbij behorende benoeming van het relatietype in verschillende talen. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de relatietypen wordt weergegeven voor beheer.

**Wijzigen** 

Met het **potlood** pictogram naast de naam van een semantische relatie wordt een semantische relatie gewijzigd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de semantische relatie wordt weergegeven voor mutatie.

**Verwijderen** 

Met het **prullenbak** pictogram wordt een semantische relatie verwijderd. Dit kan alleen als de semantische relatie niet in gebruik is of is geweest.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Thesauri <../Concepts/Concepts.html#thesauri>`__

