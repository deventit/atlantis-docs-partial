
E-Depot
~~~~~~~

Activeren E-Depot voor metadata model
-------------------------------------

Met het aanvinkveld "Beschikbaar voor E-Depot" wordt het metadata model geactiveerd voor opslag van metadata en gekoppelde bestanden in het E-Depot.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__