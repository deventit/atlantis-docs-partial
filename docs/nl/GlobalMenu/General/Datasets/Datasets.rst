
Datasets
--------

Datasets zijn verzamelingen van objecten die via een query samengesteld worden. Datasets worden gebruikt voor het beschikbaar stellen van objecten via OAI-PMH en als (Linked) Open data.

In dit paneel worden de datasets onderhouden. Gepresenteerd wordt de lijst van Datasets.

**Filteren**

De lijst van datasets wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een dataset wordt toegevoegd door in het paneel met datasets op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst met de volgende velden:

-  **Beschrijving**: Beschrijving van het dataset.
-  **Selectie**: De SQL query. 

**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij voor aanpassing.

**Verwijderen**

Een dataset wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `OAI-PMH <../Concepts/Concepts.html#oai-pmh>`__
