
Reproductiesoorten
------------------

Beheer Reproductiesoorten
^^^^^^^^^^^^^^^^^^^^^^^^^

Bij bestellingen van reproducties kunnen reproductiesoorten aangegeven worden. De beschikbare soorten van reproductie worden hier gedefinieerd. Per soort reproductie worden kosten en eventueel metadata modellen toegekend.

De reproductiesoorten worden gepresenteerd in het paneel aan de linkerkant. 

**Filteren**

Zowel de lijst van reproductiesoorten kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen of wijzigen**

Een reproductiesoort wordt toegevoegd door in het paneel met de reproductiesoorten op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een extra paneel aan de rechterkant waarin de gegevens van de reproductiesoort ingevoerd wordt. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de reproductiesoort gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de reproductiesoort, na bevestiging, verwijderd. 

**Toewijzen van metadata modellen**

Met het **M** pictogram wordt een extra paneel aan de rechterzijde geopend voor het toewijzen van metadata modellen aan de reproductiesoort. Hiermee wordt komt de reproductiesoort alleen beschikbaar bij de toegewezen metadata modellen.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__