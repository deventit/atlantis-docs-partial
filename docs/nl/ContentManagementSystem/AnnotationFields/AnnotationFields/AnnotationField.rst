
Annotatievelden
~~~~~~~~~~~~~~~

Met annotatievelden wordt gedefinieerd op welke velden een bezoeker annotaties kan toevoegen. Als er geen annotatievelden zijn gedefinieerd wordt er via de publieksomgeving ook geen mogelijkheden gegeven voor het toevoegen van annotaties. De dienst "Annotaties" dient daarnaast deel uit te maken van de licentie.

De annotatievelden worden gepresenteerd in een extra paneel aan de rechterzijde. Het paneel aan de linkerzijde zal dan automatisch inklappen.

**Toevoegen, wijzigen en verwijderen**

Een annotatieveld wordt toegevoegd via de **+** knop. Dit voegt een nieuwe regel toe waarop de naam van het annotatieveld wordt ingevoerd. Een annotatieveld wordt gewijzigd met het **potlood** pictogram achter het desbetreffende annotatieveld. Dit maakt het veld beschikbaar voor muteren. 

Met **Opslaan** wordt het annotatieveld bewaard. Met **Reset** wordt teruggekeerd naar de naam bij het openen van dit scherm of laatst bewaarde naam.

Een annotatieveld wordt verwijderd door op het **prullenbak** pictogram achter de naam van het annotatieveld te klikken. Na bevestiging wordt deze verwijderd.

**Filteren**

De lijst van annotatievelden kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Annotaties <../Concepts/Concepts.html#annotaties>`__
