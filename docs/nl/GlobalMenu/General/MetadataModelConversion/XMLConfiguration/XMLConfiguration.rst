
Invoer XML Configuratie
^^^^^^^^^^^^^^^^^^^^^^^

In dit paneel wordt de XML configuratie voor de metadata model conversie ingevoerd of gemuteerd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Metadata modellen <../Concepts/Concepts.html#metadata-modellen>`__