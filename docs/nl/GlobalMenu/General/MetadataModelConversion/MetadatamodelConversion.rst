
Omzetting metadata modellen
---------------------------

Documenten van een metadata model kunnen omgezet worden naar objecten van een ander metadata model. Welke omzettingen mogelijk zijn en hoe deze omzetting plaatsvindt wordt hier gedefinieerd.

In dit paneel worden de metadata model omzettingen onderhouden. Gepresenteerd wordt de lijst van metadata model omzettingen.

**Filteren**

De lijst van metadata model omzettingen wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een metadata model omzetting wordt toegevoegd door in het paneel met de metadata model omzettingen op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst met de volgende velden:

-  **Van**: Selectie van het metadata model van het object dat wordt omgezet (bijvoorbeeld een deelbeschrijving).
-  **Via**: Selectie van het metadata model dat betrokken wordt bij de omzetting. Dit metadata model heeft een relatie met het "Van" metadata model (bijvoorbeeld een verzamelbeschrijving).
-  **Naar**: Selectie van het metadata model waar het object naartoe omgezet moet worden (bijvoorbeeld een enkelvoudige beschrijving)

Met het **XML-configuratie bewerken** pictogram wordt de XML voor de conversie ingevoerd of gemuteerd. Hierin worden de velden van het "Van" en "Via" metadata model gemapped naar het "Naar" metadata model.

**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij voor aanpassing.

**Verwijderen**

Een metadata model omzetting wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Metadata modellen <../Concepts/Concepts.html#metadata-modellen>`__

