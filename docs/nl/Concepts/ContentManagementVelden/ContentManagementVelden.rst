
Content Management Velden
~~~~~~~~~~~~~~~~~~~~~~~~~

Dat Atlantis vrij is van de beschrijvingsstructuren van metadata modellen wordt voor een belangrijk deel geregeld via Content Management Velden (CM-velden).

**XML document van object**

Op een metadata model wordt geconfigureerd hoe de XML presentatie van de beschrijvingsstructuur is opgebouwd. Hierin staan de tabellen en velden in hun onderlinge structuur beschreven voor de specifieke beschrijvingsstructuur. Voor een object wordt bij het bewaren hiermee een XML-document samengesteld en opgeslagen in de database.

CM-velden worden aan dit XML document toegevoegd. De waarde die in een CM-veld wordt geplaatst is het resultaat van een XSLT-expressie die uitgerekend wordt bij het bewaren van het object. De XSLT-expressie kan putten uit de metadata van het object en objecten die direct of indirect hieraan gerelateerd zijn. Daarnaast kan een XSLT-expressie vaste teksten en opmaak bevatten.

Om het samenstellen van de XSLT-expressie te vereenvoudigen is een XSLT-editor beschikbaar waarmee op een intuïtieve en gebruikersvriendelijke manier de XSLT-expressie samengesteld kan worden.

**Soorten van CM-velden**

De CM-velden worden getypeerd naar het gebruik daarvan. Een eerste typering is of een CM-veld bedoeld is voor gebruik binnen de collectiebeheeromegving, publieksomgeving of beiden. Vervolgens zijn de volgende typeringen beschikbaar:

-  **Zoekveld**: een zoekveld wordt in het zoekformuliervoor voor het zoeken op de objecten van het metadata model opgenomen.

-  **Presentatieveld**: Een presentatieveld wordt in de presentatie van zoekresultaten als resultaatkolom opgenomen.

-  **Sorteerveld**: Een sorteerveld wordt als sorteeroptie bij de presentatie van zoekresultaten aangeboden.

-  **Filterveld**: Een filterveld wordt als filter bij de presentatie van zoekresultaten in de publieksomgeving aangeboden.

-  **Detailveld**: Een detailveld wordt als detailinformatie bij de presentatie van het object in de publieksomgeving gepresenteerd.

-  **Locatieveld**: Een locatieveld bevat de locatie van het object, als adresinformatie of als GPS-coördinaten, zodat Google Maps of een nadere map-tool dit kan gebruiken voor zoeken via een kaart en presenteren van het object of de zoekresultaten op een kaart.

-  **Relevatieveld**: Een relevantieveld kan toegevoegd worden als resultaatveld en/of sorteerveld. In het relevantieveld wordt de berekende relevantie van het zoekresultaat geplaatst t.o.v. de andere zoekresultaten. De relevantie kan gepresenteerd of op gesorteerd worden.

-  **Contextveld**: Een contextveld wordt standaard opgebouwd uit de presentatie met links van de hiërarchisch bovenliggende objecten van een object. Dit kan worden gebruikt in de presentatie van context bij een object of in een zoekresultaat.

-  **Icoonveld**: een icoonveld geeft aan dat het icoon van het object wordt gepresenteerd. Dit kan gebruikt worden als kolom in de presentatie van zoekresultaten.

-  **Globaal**: Een globaal veld wordt, als het tevens als zoekveld is aangemerkt, gebruikt als zoekveld bij het globale (eenvoudig) zoeken.

Aan een CM-veld kan een combinatie van typeringen toegekend worden. Hiermee heeft een CM-veld verschillende rollen. Bv. als een zoekveld voor zowel de beheer- als publieksomgeving van toepassing is, dan worden beide typeringen toegekend.

**Zoekmogelijkheden**

Als een CM-veld voor zoeken is aangemerkt, zijn de zoekmogelijkheden bij het "Uitgebreid zoeken" afhankelijk van de aanduiding van het type veld. De volgende typen zijn beschikbaar:

- **Datum**: Zoeken met relationele operatoren (>, <, <=, <=, =, <>), "is leeg" en "is niet leeg" zijn beschikbaar.

- **Numeriek**: Zoeken met relationele operatoren (>, <, <=, <=, =, <>), "is leeg" en "is niet leeg" zijn beschikbaar.

- **Tekst**: Zoeken met relationele operatoren (>, <, <=, <=, =, <>), "is leeg" en "is niet leeg" zijn zijn beschikbaar.

- **Volledige tekst**: Zoeken met "begint met", "begint niet met", "bevat", "bevat niet", "eindigt op", "eindigt niet op", "is leeg" en "is niet leeg" zijn beschikbaar.

Een CM-veld kan een aangemerkt worden als een combinatie van deze typen waarmee de gecombineerde zoekmogelijkheden beschikbaar komen.

Als een CM-veld is aangemerkt als een van..t/m veld, dan komen ook de mogelijkheden "ligt tussen", "ligt niet tussen" en "overlapt".

Op zoekvelden kunnen ook "selectielijsten" aangeboden worden via het selecteren van een veld uit de beschrijvingsstructuur waarbij aangegeven wordt of de waarden van dit veld oplopend of aflopend gesorteerd worden.

**Relevantie**

Als er gezocht wordt op objecten kan een relevantie worden uitgerekend via het relevantieveld. Zoeken betekent zoeken op de CM-velden die zijn aangemerkt als zoekveld. Dus de waarden van de zoekvelden worden doorzocht. Bij een zoekveld kan een gewicht worden aangegeven, waarmee treffers op het ene zoekveld als belangrijker (van hoger gewicht) worden verondersteld dan treffers op andere zoekvelden. De treffers met bijbehorende gewichten worden meegenomen om de relevantie van een zoekresultaat uit te rekenen. Gewichten zijn relatief ten opzichte van elkaar. Een hoger gewicht geeft een hogere relevantie als daar een treffer op te noteren valt.

**Zoeken op combinaties van metadata modellen**

Via de definitie van "Zoekingangen" is bepaald op welke objecten van welke metadata modellen gezocht gaat worden. Als dit een combinatie van metadata modellen betreft, dan worden de CM-velden voor zoeken,  zoekresultaten, filters en sorteren ook met elkaar gecombineerd als volgt:

- **Zoeken**: alleen de zoekvelden met een overeenkomstige naam van alle te doorzoeken metadata modellen zijn als zoekvelden beschikbaar.

- **Zoekresultaten**: alle presentatievelden van alle te doorzoeken metadata modellen zijn als presentatievelden beschikbaar.

- **Sorteren**: alle presentatievelden van alle te doorzoeken metadata modellen zijn als presentatievelden beschikbaar.

- **Filteren**: alle filters van de objecten in het zoekresultaat zijn als filters beschikbaar.

**Volgorde van CM-velden**

Aan een CM-veld wordt een volgnummer toegekend. Dit geeft de volgorde aan binnen een zoekformulier, resultaatkolommen, sorteringen of filters. Dit volgnummer is relatief en niet uniek. Bij gelijke volgnummers maakt schijnbaar de volgorde niet uit. Omdat een CM-veld meerdere typeringen kan hebben, is het zaak het volgnummer zodanig toe te kennen zodat voor iedere typering de juiste volgorde van toepassing is. Dit geldt ook als CM-velden in combinaties van metadata modellen beschikbaar zijn.