
Thesauri
~~~~~~~~

Een thesaurus is een systeem voor termen waarin termen met elkaar in relatie gebracht worden. Een thesaurus is georganiseerd volgens een hiërarchie van domeinen, conceptgroepen en concepten. Het concept is dan de daadwerkelijke term. 

In Altantis kunnen meerdere thesauri ondergebracht worden. Op velden van een metadata model kan dan een koppeling met een thesaurus aangebracht worden, met een keuze voor de plaats in de hiërarchie vanwaar de keuze voor een concept plaats gaat vinden.

In formulieren vertaald zich dit naar de mogelijkheid om op invoer van een veld suggesties uit de thesaurus te krijgen of de hiërarchie van concepten op te roepen voor selectie van een concept.

Als een koppeling van een veld met een thesaurus van toepassing is, dan wordt onderscheid gemaakt tussen een verbatim waarde en de thesaurus waarde. Dit mag van elkaar afwijken als dit zo is geconfigureerd.

Relaties tussen termen kunnen vrij gedefinieerd worden, maar zijn altijd van het type Ouder/Kind, Associatie of Transitief. Deze relatietypen kunnen daarnaast worden voorzien van relatienamen in verschillende talen.

In een thesaurus worden domeinen, conceptgroepen en concepten ingevoerd via een conceptnode. Hierin kunnen de termen in verschillende talen met aanduiding of dit voorkeurs- of standaardtermen betreffen.

Atlantis biedt ook de mogelijkheid om externe thesauri te koppelen. Daarmee wordt binnen formulieren dezelfde mogelijkheden bij de velden gegeven als wanneer het  interne thesauri zou betreffen.

In de systeeminstellingen wordt de voorkeurstaal voor de interne thesauri aangegeven.