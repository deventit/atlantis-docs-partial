
Toekennen Metadata Modellen
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Voor het toekennen van metadata modellen aan zoekingangen wordt een paneel aan de rechterzijde geopend waarin metadata modellen geselecteerd worden uit de lijst van metadata modellen. Met de **x** knop bij een metadata model wordt deze verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__