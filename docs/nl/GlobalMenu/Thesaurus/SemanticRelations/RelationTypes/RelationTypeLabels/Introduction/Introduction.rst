
Beheer Relatie Type Labels
^^^^^^^^^^^^^^^^^^^^^^^^^^

Relatie type labels definieert de labels in de verschillende talen voor de beide zijden van de relatie typen.

**Relatie type labels**

Het scherm voor de relatie type labels presenteert een paneel aan de linkerzijde. Dit geeft een overzicht van de beschikbare relatie type labels. 

**Filteren**

De lijst met relatie type labels kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. 

**Toevoegen** 

Met het **toevoegen** pictogram boven de lijst met relatie type labels wordt een nieuwe relatie type label toegevoegd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de relatie type label wordt weergegeven voor invoer.

**Wijzigen** 

Met het **potlood** pictogram naast de naam van een relatie type label wordt een relatie type label gewijzigd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de relatie type label wordt weergegeven voor mutatie.

**Verwijderen** 

Met het **prullenbak** pictogram wordt een relatie type label verwijderd. Dit kan alleen als de relatie type label niet in gebruik is.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__