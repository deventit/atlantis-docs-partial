
Talen
-----

Talen geeft aan welke talen voor de thesauri beschikbaar zijn. 

In dit paneel worden de talen onderhouden. Gepresenteerd wordt de lijst van talen.

**Filteren**

De lijst van talen wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een taal wordt toegevoegd door in het paneel met talen op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst met de volgende velden:

-  **Taal**: Naam van de taal.
-  **Code**: Code van de taal. 

**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij voor aanpassing.

**Verwijderen**

Een taal wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__