
Factuurnummer
-------------

In dit paneel worden factuurnummers gedefinieerd waarmee begonnen wordt bij facturering vanaf de aangegeven dag.

Voor het definiëren wordt een paneel aan de rechterzijde geopend met een tabel van factuurnummers en datums. Een factuurnummer wordt gedefinieerd via de volgende velden:

-  **Start datum**: De datum vanaf wanneer de facturering start met het factuurnummer.
-  **Eerste factuurnummer**: Het eerste factuurnummer dat op de start datum wordt gebruikt. Voor opvolgende facturen wordt vanaf dit nummer doorgenummerd.
-  **Laatste factuurnummer**: Het laatste factuurnummer dat is gebruikt. Dit is een alleen-lezen veld.

**Filteren**

De lijst van factuurnummers kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een factuurnummer wordt toegevoegd door in de tabel op het **+** pictogram (nieuwe toevoegen) te klikken. Hiermee komt een nieuwe regel beschikbaar waarin de gegevens van het factuurnummer ingevoerd worden. Met het **potlood** pictogram komt de regel beschikbaar waarin de gegevens van het factuurnummer gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt het factuurnummer, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__