
Openbaarheidsbeperkingen
^^^^^^^^^^^^^^^^^^^^^^^^

Openbaarheidsbeperkingen zorgen ervoor dat objecten van nadere toegangen aangemerkt worden als "niet openbaar".

In dit paneel worden de openbaarheidsbeperkingen onderhouden. Gepresenteerd wordt de lijst van openbaarheidsbeperkingen.

**Filteren**

De lijst van openbaarheidsbeperkingen wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een openbaarheidsbeperking wordt toegevoegd door in het paneel met openbaarheidsbeperkingen op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst met de volgende velden:

-  **Naam**: Naam van de openbaarheidsbeperking.
-  **Aantal jaren**: Het aantal jaren dat de openbaarheidsbeperking geldt vanaf de datum van het object. 

**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij voor aanpassing.

**Verwijderen**

Een openbaarheidsbeperking wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__

