
Transformaties
--------------

Beheer Transformaties
^^^^^^^^^^^^^^^^^^^^^

Met een transformatie wordt een zoekresultaat van objecten van een metadata model omgezet naar een zoekresultaat van objecten van een ander metadata model. 
Bijvoorbeeld bij een zoekresultaat van archiefbestanddelen kan met een transformatie deze omgezet worden naar een zoekresultaat van archieven. Dit zijn dan de archieven waarvan de archiefbestanddelen in het zoekresultaat zaten.

**Transformaties**

Het scherm voor de transformaties presenteert een paneel aan de linkerzijde. Dit geeft een overzicht van de beschikbare transformaties. 

**Filteren**

De lijst met transformaties kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. 

**Toevoegen** 

Met het **toevoegen** pictogram boven de lijst met transformaties wordt een nieuwe transformatie toegevoegd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de transformatie wordt weergegeven voor invoer.

**Wijzigen** 

Met het **potlood** pictogram naast de naam van een transformatie wordt een transformatie gewijzigd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de transformatie wordt weergegeven voor mutatie.

**Verwijderen** 

Met het **prullenbak** pictogram wordt een transformatie verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Metadata modellen <../Concepts/Concepts.html#metadata-modellen>`__, `Relaties <../Concepts/Concepts.html#relaties>`__