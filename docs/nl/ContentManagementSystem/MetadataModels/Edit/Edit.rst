
Invoer Metadata Model
---------------------

Voor het wijzigen van een metadata model zijn in dit paneel de volgende
velden beschikbaar:

-  **Naam**: Naam van het metadata model.
-  **Uniciteit**: Aanduiding van de uniciteit van objecten van dit metadata model.
-  **Contactpersoon**: Contactpersoon voor dit metadata model.
-  **Beschrijving**: Beschrijving van het metadata model.
-  **Webshop administratie**: Selecteer hier de toe te passen
   winkelwagen voor bestellingen voor reproductie, aanvragen voor inzage
   en aanvragen voor Scanning on Demand. Desbetreffende diensten dienen
   wel in de licentie beschikbaar te zijn.
-  **Icoon**: Selecteer hier het icoon voor presentatie als er verder
   geen icoon te bepalen is.
-  **Presentatie Profiel**: Selecteer hier het toe te passen Presentatie
   Profiel voor presentatie van beeldmateriaal.
-  **Segment**: Selecteer hier het standaard Segment voor objecten van
   dit metadata model.
-  **Module**: Selectie van module waar dit metadata model toe behoort.
-  **Hoofdtabel**: Hoofdtabel van dit metadata model.
-  **Navigeerbaar**: Geeft aan of objecten van dit metadata model via
   het Navigatie menu beschikbaar is.
-  **Inclusief gelinkte objecten**: Geeft aan of gerelateerde objecten
   bij navigatie ook gepresenteerd worden.
-  **Categorie Schema**: Geeft aan of objecten van dit metadata model
   als een categorie schema beschikbaar komen.
-  **Zoekboom**: Geeft aan of objecten van dit metadata model bij het
   zoeken een systematische zoekingang biedt.
-  **Bestelbaar**: Geeft aan of voor objecten van dit metadata model
   bestellingen van reproducties aangeboden worden.
-  **Aanvraagbaar**: Geeft aan of voor objecten van dit metadata
   aanvragen voor raadpleging aangeboden wordt.
-  **Scanning on Demand**: Geeft aan of objecten van dit metadata model in aanmerking komen voor Scanning on Demand.
-  **Verbergen in toevoegmenu**: Geeft aan of het toevoegen van objecten van dit metadata model niet beschikbaar is in het menu voor het toevoegen.
-  **Rangenummering**: Geeft aan of rangenummering beschikbaar is.
-  **Intern**: Als geselecteerd geeft dit aan dat dit een Atlantis metadata model betreft.
-  **Extern**: Als geselecteerd geeft dit aan dat dit een metadata model betreft van een externe bron.
-  **Auteursrechtelijk beschermd**: Selectie van een icoon dat Auteursrechtelijke bescherming aanduidt voor gekoppelde bestanden en geen ander icoon hiervoor gevonden wordt.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Metadata modellen <../Concepts/Concepts.html#metadata-modellen>`__