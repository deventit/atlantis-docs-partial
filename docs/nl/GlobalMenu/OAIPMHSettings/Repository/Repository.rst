
OAI-PMH instellingen
~~~~~~~~~~~~~~~~~~~~

Repository
----------

De repository geeft de instellingen voor de OAI-PMH client waarmee externe omgevingen data harvest uit Atlantis. Dit bestaat uit drie onderdelen:

-  **Repository**: De algemene instellingen van de client.
-  **Metadata prefix**: De beschikbare metadata prefixes.
-  **Beheerders**: De beheerders van de repository.

**Repository**

De algemene instellingen van de repository bestaat uit de volgende velden:

-  **Naam**: Naam van de repository.
-  **URL**: URL naar de OAI-PMH toegang.
-  **Protocol versie**: Versie van het OAI-PMH protocol.
-  **Formaat**: Gehanteerde datum/tijd formaat.
-  **Vroegste record**: Datum/tjd van het vroegste record.
-  **Compressie**: Soort van compressie dat wordt toegepast.
-  **Verwijderde records**: Selectie van omgang met verwijderde records.

**Metadata prefix**

Een metadata prefix geeft een beschikbare set van objecten op de OAI-PMH aan. Dit bestaat uit de volgende velden:

-  **Prefix**: Prefix van de set.
-  **Namespace**: Aanduiding van de namespace.
-  **XML Schema**: XML schema van de aangeboden objecten.

**Beheerders**

De beheerders zijn de e-mailadressen van contactpersonen omtrent de respository. Dit bestaat uit het veld:

-  **E-mailadres**: Het e-mailadres van de contactpersoon.

**Toevoegen**

Een metadata prefix of een beheerder wordt toegevoegd via het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. 

**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij voor aanpassing.

**Verwijderen**

Een metadata prefix of beheerder wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__`OAI-PMH <../Concepts/Concepts.html#oai-pmh>`__