
Verzendmethoden
---------------

Voor het definiëren van verzendmethoden wordt een paneel aan de rechterzijde geopend met een tabel van verzendmethoden. Een verzendmethode wordt gedefinieerd via de volgende velden:

-  **Beschrijving**: Beschrijving van de verzendmethode.
-  **Bedrag**: Bedrag dat voor de verzendmethode in rekening wordt gebracht.

**Filteren**

De lijst van verzendmethoden kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een verzendmethode wordt toegevoegd door in de tabel op het **+** pictogram (nieuwe toevoegen) te klikken. Hiermee komt een nieuwe regel beschikbaar waarin de gegevens van de verzendmethode ingevoerd worden. Met het **potlood** pictogram komt de regel beschikbaar waarin de gegevens van de verzendmethode gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de verzendmethoden, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__
