
Toevoegen of wijzigen
~~~~~~~~~~~~~~~~~~~~~~

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Code**: De code van de categorie. Dit mag ieder karakter bevatten. In de categorieboom worden de categorieën gepresenteerd via een codering en de **Titel**. De codering wordt standaard uit de code van deze categorie en de codes van bovenliggende categorieën, eventueel gescheiden door een **Scheidingsteken**. Het scheidingsteken tussen de codering van bovenliggende categorieën en deze categorie wordt met het veld **Scheidingsteken** aangegeven.
-  **Titel**: De titel van de categorie, wordt samen met de codering in de b=boom gepresenteerd.
-  **Scheidingsmerk**: Dit scheidingsteken wordt toegevoegd tussen de codering van de bovenliggende categorieën en deze categorie.
-  **Vroegste**: De vroegst mogelijke datering.
-  **Laatst**: De laatst mogelijke datering.
-  **Periode**: De periode of omschrijving daarvan.
-  **Telt niet in codering**: Indien dit wordt aangevinkt wordt de code van deze categorie niet meegenomen in de codering van de categorie in de boom. Het **Scheidingsteken** wordt dan ook niet toegepast.
-  **Sorteernummer**: Categorieën worden weergegeven binnen de bovenliggende categorie in volgorde van het sorteernummer. Deze wordt automatisch bepaald bij het toevoegen, verslepen en knippen/kopiëren/plakken van categorieën.
-  **Opmerking**: Ruimte voor opmerkingen.
-  **Omvang en inhoud**: Omschrijving van de omvang en inhoud van de gekoppelde objecten.
-  **Gemodereerd**: Vlag of categorie is gemodereerd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Categorieën <../Concepts/Concepts.html#categorieen>`__