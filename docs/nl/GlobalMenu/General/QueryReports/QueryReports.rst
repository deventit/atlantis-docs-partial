
Query rapporten
----------------

Query rapporten zijn tabulaire rapporten op basis van een SQL query. Dit betreffen rapporten die veel data bevatten of data bevatten die niet via de reguliere (standaard) rapportages op te vragen zijn. Een query rapport is een rapport op basis van een SQL query. Hiermee is alle data en zijn alle SQL functies beschikbaar. Een query rapport kan voor de hele database worden uitgevoerd, maar ook voor zoekresultaten. Een query rapport levert een rapport in CSV formaat.

In dit paneel worden de query rapporten onderhouden. Gepresenteerd wordt de lijst van query rapporten.

**Filteren**

De lijst van query rapporten wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een query rapport wordt toegevoegd door in het paneel met query rapporten op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst met de volgende velden:

-  **Beschrijving**: Beschrijving van het query rapport.
-  **Voor een zoekresultaat**: Vlag waarmee aangegeven wordt of het query rapport ook voor zoekresultaten beschikbaar is.
-  **Vraag**: De SQL query. Als het query rapport beschikbaar is voor zoekresultaten, dan dient in de query "\ *frm_xmlbeschrijvingen x*\ " opgenomen te worden.

**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij voor aanpassing.

**Verwijderen**

Een query rapport wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__

