
Presentatie Profielen
~~~~~~~~~~~~~~~~~~~~~

Met presentatie profielen wordt bepaald hoe digitale afbeeldingen gepresenteerd en gedownload worden. Een presentatieprofiel kan worden aangegeven bij een metadata model dat een relatie heeft met multimediale objecten en bij een auteursrechthouder. Als bij een metadata model geen presentatieprofiel is aangegeven, dan wordt het presentatieprofiel dat als "standaard" is gemarkeerd verondersteld.

**Instellingen voor presentatie**

Voor digitale afbeeldingen wordt voor Internet en Intranet afzonderlijk aangegeven wat het bestandstype, kwaliteit, maximale resolutie, maximaal aantal dpi's (dots per inch) is wanneer er gepresenteerd wordt. Daarnaast wordt aangegeven wat de resolutie van het icoon is bij het koppelen van digitale afbeeldingen aan objecten.

Deze omzetting naar deze ingstelde parameters vindt realtime plaats, en vormt het uitganspunt voor presentatie in de beeldviewer. Er wordt dan niet meer kwaliteit, resolutie en dpi's geleverd, in het aangegeven bestandsformaat, dan hier ingesteld.

De *tiles* die hiervoor worden samengesteld worden in een cache opgeslagen zodat bij herhaaldelijk opvragen van een afbeelding niet steeds de omzetting hoeft te gebeuren.

Bij het terugbrengen van de resolutie voor presentatie blijven de verhoudingen (aspect ratio) tussen breedte en hoogte behouden. Dit geldt ook voor het samen te stellen icoon.]

Als het bestandstype, de resolutie, de kwaliteit of het aantal dpi's van de originele afbeelding lager is dan wat is aangegeven in het presentatieprofiel, dan vindt geen verhoging plaats. Als een afbeelding geen informatie bevat over de kwaliteit of het aantal dpi's, dan zal dat niet in overweging genomen worden voor de presentatie.

**Watermerk**

Voor de presentatie kan een watermerk worden aangegeven via een watermerk afbeelding, plaats van het watermerk en de doorschijnendheid.

**Downloaden**

Via het presentatie profiel wordt aangegeven of een afbeelding beschikbaar is voor downloaden, in welke situaties en in welke resoluties.

Onderscheiden wordt het downloaden van:

- Het originele, onbeperkte afbeelding. Dit is de afbeelding waar via het presentatieprofiel effectief geen beperkingen gelden, bijvoorbeeld als de resolutie binnen de maximale resolutie blijft.

- Een beperkte afbeelding. Dit is een afbeelding die door het presentatieprofiel beperkt wordt t.o.v. het origineel. 

- Een selectie van een afbeelding. Dit is een download van een uitsnede van de afbeelding.

- Een download via een multimedia selectie. 

In het presentatieprofiel worden downloadresoluties geselecteerd uit de lijst van beschikbare "Downloadresoluties". Voor iedere downloadresolutie wordt dan aangegeven in welke situaties die van toepassing is.

**Uitwerking binnen de Atlantis beheeromgeving**

Bij het uploaden van digitale afbeeldingen wordt een icoon bepaald aan de hand van de instellingen van het geldende presentatieprofiel. Dit is het presentatieprofiel van het metadata model van het object waar de digitale afbeeldingen aan gekoppeld worden of het presentatieprofiel dat als standaard is aangemerkt.

Bij het presenteren van de afbeelding worden de parameters van het geldende presentatieprofiel toegepast. Voor het bepalen van het geldende presentatieprofiel wordt eerst gekeken of er een auteursrechthouder met een ingesteld presentatieprofiel geselecteerd is voor de afbeelding. Is dit niet het geval, dan is het geldende presentatieprofiel het presentatieprofiel van het metadata model van het object waar de digitale afbeeldingen aan gekoppeld worden of het presentatieprofiel dat als standaard is aangemerkt.

Als een watermerk van toepassing is, wordt deze ingewoven in de te presenteren afbeelding in de beeldviewer. 

Als download van toepassing is, zal in de voorkomende situaties een downloadknop gepresenteerd worden. Zijn er verschillende downloadresoluties van toepassing zal bij het downloaden gevraagd worden om een downloadresolutie te kiezen.




