
Invoer Verpakking
^^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Beschrijving**: De beschrijving van de soort verpakking.
-  **Hoogte**, **Breedte**, **Diepte**: De dimensies van de verpakking in cm. Getallen met een decimale punt worden ingevoerd met "."
-  **Kantelen**: Geeft aan of de verpakking bij het plaatsen in bv. een depot gekanteld mag worden.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__