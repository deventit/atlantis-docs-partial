
Invoer Opendata bron
^^^^^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Naam**: De naam van de Open data definitie.
-  **Beschrijving**: De beschrijving van de Open data definitie.
-  **Selecteer Dataset**: Hier wordt de Dataset geselecteerd waarop de Open data definitie van toepassing is.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__
