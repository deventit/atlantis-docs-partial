
Soorten Annotaties
~~~~~~~~~~~~~~~~~~

Soorten annotaties geven de soorten van annotaties aan die door de bezoeker gedaan kunnen worden.

In dit paneel worden de soorten annotaties onderhouden. Gepresenteerd wordt de lijst van soorten annotaties.

**Filteren**

De lijst van soorten annotaties wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een soort annotatie wordt toegevoegd door in het paneel met soorten annotaties op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst met de volgende velden:

-  **Beschrijving**: De naam van de soort annotatie
-  **Icoon**: Selectie van het icoon voor de soort annotatie.

**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij voor aanpassing.

**Verwijderen**

Een soort annotatie wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Annotaties <../Concepts/Concepts.html#annotaties>`__

