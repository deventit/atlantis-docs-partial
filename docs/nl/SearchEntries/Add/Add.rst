
Invoeren Zoekingang
~~~~~~~~~~~~~~~~~~~

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Naam**: Naam van de zoekingang.
-  **Naam zoekscherm**: De naam van een (maatwerk) zoekscherm. Alleen te gebruiken op instructie van DEVENTit.
-  **Specifiek zoekscherm**: De naam van een (maatwerk) specifiek zoekscherm. Alleen te gebruiken op instructie van DEVENTit.
-  **Tag**: Invoer van een tag voor de zoekingang.
-  **Publiek**: Geeft aan of de zoekingang via de publieksomgeving beschikbaar gesteld wordt.
-  **Beheer**: Geeft aan of de zoekingang via de beheeromgeving beschikbaar gesteld wordt.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__