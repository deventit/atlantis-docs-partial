
Nadere Toegangen
~~~~~~~~~~~~~~~~

Soorten nadere toegangen
------------------------

Beheer soorten nadere toegangen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In dit paneel worden soorten nadere toegangen gedefinieerd. Een soort nadere toegang bepaalt voor een toegang welke standaard openbaarheidsbeperking er geldt en worden de beschikbare vrije velden benoemd. In archieftermen worden nadere toegangen ook wel aangeduid als **Akten en Registers**

**Filteren**

De lijst van soorten nadere toegangen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een soort nadere toegang wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van de soort nadere toegang ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de soort nadere toegang gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de soort nadere toegang, na bevestiging, verwijderd. 

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__