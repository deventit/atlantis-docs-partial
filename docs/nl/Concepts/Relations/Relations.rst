
Relaties
~~~~~~~~

Atlantis is volledig vrij in het configureren van relaties tussen objecten van metadata modellen.

**Toegestane relaties**

Geconfigureerd wordt welke relaties tussen metadata modellen toegestaan zijn. Als een relatie tussen metadata modellen wordt gedefinieerd, dan kunnen objecten van het ene metadata model gerelateerd worden aan objecten van het andere metadata model.

**Bovenliggend, onderliggend en hiërarchisch**

Bij iedere relatie wordt aangegeven of deze bovenliggend of onderliggend is. Dit is soms heel duidelijk en soms subjectief. Bijvoorbeeld bij een boek met paginas is duidelijk dat het boek bovenliggend aan de paginas is. Bij een relatie tussen archief en archiefvormer is dit al minder helder. Er kan beargumenteerd worden waarom een archiefvormer bovenliggend aan een archief is, maar ook andersom. Een ander voorbeeld is een depotplaats met een relatie naar een bestanddeel, voor een depotbeheerder is het bestanddeel onderliggend aan een depotplaats, voor een archiefbeheerder andersom.

Het onderscheid is niet heel belangrijk, er moet een keuze gemaakt worden. Wat wel belangrijk is, is of de relatie hiërarchisch is. De bovenliggende hiërarchie, dus het pad van hiërarchisch bovenliggende relaties, definieert het object. Als voorbeeld een archiefpagina van een boek: op zichzelf geeft een beschrijving van een pagina alleen informatie op het niveau van het bestanddeel, bijvoorbeeld een paginanummer. Om de pagina te identificeren is informatie over het boek nodig. Hiervoor dient een hiërarchische relatie te bestaan tussen het boek en de paginas. Er wordt dan ook wel gesproken van overerving. De paginas van een boek erft de informatie van het boek over. In Atlantis wordt dit "context" genoemd. De context van een object is de identificerende informatie van de hiërarchisch bovenliggende relaties.

**Vooruit en terug**

Relaties tussen objecten worden doorlopen via navigatie. Er wordt dan gesproken van "vooruit" en "terug". Vooruit betekent dat met de richting van de relatie mee wordt gelopen, dus van bovenliggend naar onderliggend. Terug wordt de relatie in tegenovergestelde richting doorlopen, dus van onderliggend naar bovenliggend. Denk hierbij aanstromend water, met de stroom mee (vooruit) is van boven naar beneden. Tegen de stroom in (terug) is van beneden naar boven. 

**Relatienamen**

Relaties worden benoemd. De naam van een relatie is afhankelijk van vanuit welk object naar de relatie gekeken wordt. Vanuit een boek wordt de relatie gezien als paginas, vanuit de paginas als boek. Dus de relatienamen van de relatie tussen boeken en paginas zijn paginas voor de relatie vooruit en boek voor de relatie terug. Het betreft hier slechts één bidirectionele relatie tussen boeken en paginas. 

**Cardinaliteit**

Relaties hebben een cardinaliteit, dit wil zeggen: hoeveel relaties mogen er gelegd worden. Tussen boek en paginas mag er 1 relatie gelegd worden van pagina naar boek en meerdere relaties van boek naar paginas. De cardinaliteit vooruit is dan 0 (onbeperkt veel) en voor terug 1. In de Atlantis beheeromgeving wordt bij het koppelen van objecten rekening gehouden met een beperking van het aantal relaties.

**Uitwerking binnen de Atlantis beheeromgeving**

In de Atlantis beheeromgeving worden relaties met objecten gepresenteerd in navigatiebomen en bij de presentatie van de detailinformatie van een object. In een navigatieboom zal dit leiden tot een knoop met de naam van de relatie. Bij de presentatie van een detailobject via tabbladen. Bij een relatie worden de tabbladen waarin de gekoppelde objecten worden gepresenteerd aangegeven. Worden er geen tabbladen aangegeven, dan worden de gekoppelde objecten gepresenteerd in een tabblad "Relaties". 

Gekoppelde objecten van verschillende metadata modellen worden in hetzelfde tabblad gepresenteerd als op de relaties hetzelfde tabblad wordt aangegeven. Bijvoorbeeld voor enkelvoudige - en verzamelbeschrijvingen wordt op de relatie met het archief doorgaans een tabblad "Bestanddelen" aangegeven, zodat bij de presentatie van de detailinformatie van een archief een tabblad "Bestanddelen" beschikbaar is met daarin de presentatie van de gekoppelde enkelvoudige - en verzamelbeschrijvingen.

**Beheer van de relaties**

Via een relatiemodel wordt voor een metadata model grafisch de toegestane relaties met andere metadata modellen gepresenteerd. Dit model wordt interactief uitgebreid door op een metadata model te klikken. Daarnaast wordt vanuit dit model de relaties onderhouden door toevoegen, verwijderen en muteren van relaties. Met kleuren is aangegeven of de relaties (vanuit het geselecteerde metadata model) boven- of onderliggend zijn. Is de relatie een pijl, dan is de relatie hiërarchisch.