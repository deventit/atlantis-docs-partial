
Invoer Presentatieprofiel
^^^^^^^^^^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Naam**: De naam van het presentatieprofiel. Dit veld is verplicht.
-  **Standaard**: Geeft aan of dit het standaard presentatieprofiel is. Als bij een metadata model geen presentatieprofiel wordt aangegeven, dan geldt dit presentatieprofiel.

Onder het eerste kopje "Afbeelding" wordt voor het internet, intranet en het icoon afzonderlijk aangegeven:

-  **Bestandstype**: Het bestandsformaat voor de presentatie, zoals JPG, PNG, etc.
-  **Kwaliteit**: De kwaliteit van de afbeeldingen (in % voor JPG, of een nummervoor  PNG). Dit geeft de  kwaliteit/compressieverhouding of een compressiefactor.
-  **Breedte**: De maximale breedte in pixels. Het beeld wordt gepresenteerd met behoud van de verhoudingen. 
-  **Hoogte**: De maximale hoogte in pixels. Het beeld wordt gepresenteerd met behoud van de verhoudingen.
-  **DPI**: Het maximumaantal aantal punten per inch.

Onder het kopje **Watermerk** wordt het toe te passen watermerk gedefinieerd. Dit wordt ingevlochten bij presentatie van het beeld. 

-  **Afbeelding**: Selecteer hier de afbeelding van het watermerk.
-  **Selecteer plaatsing watermerk**: Hiermee wordt de positionering van het watermerk geselecteerd.
-  **Transparantie**: Met de schuifbalk wordt de transparantie van het watermerk aangegeven. Hoe lager de transparantie, hoe duidelijker het watermerk wordt gepresenteerd over de afbeelding heen.

Onder het eerste kopje "Video" wordt voor videobestanden aangegeven:

-  **Bestandstype**: Het bestandsformaat.
-  **Codec**: De toe te passen codec.
-  **Bitrate**: De bitrate.
-  **Framerate**: De framerate.
-  **Horizontale resolutie**: De horizontale resolutie.
-  **Verticale resolutie**: De verticale resolutie.

Onder het eerste kopje "Audio" wordt voor videobestanden aangegeven:

-  **Bestandstype**: Het bestandsformaat.
-  **Codec**: De toe te passen codec.
-  **Bitrate**: De bitrate. 

Onder het 2e kopje **Afbeelding** worden de beschikbare download resoluties aangegeven. Met het **+** pictogram wordt een nieuwe resolutie toegevoegd aan de tabel. Met het **potlood** pictogram wordt de komt de download resolutie beschikbaar voor wijziging. De wijzigingen worden opgeslagen met de **Opslaan** knop. Met **Afsluiten** wordt de wijzigingen in de download resolutie niet opgeslagen.

Bij het **Download resolutie** veld wordt gekozen voor de betreffende download resolutie.

Download resoluties zijn beschikbaar in diverse situaties:

-  **Internet**: bij presentatie op het Internet.
-  **Intranet**: bij presentatie op het Intranet.
-  **Beperkt**: bij een download van een uitsnede van de afbeelding.
-  **Onbeperkt**: bij download van een niet uitgesneden afbeelding.
-  **Selectie**: bij een download van een selectie van afbeeldingen.
-  **Download**: bij het gebruik van de download knop bij de presentatie van een afbeelding.

De aanvinkvelden kunnen in combinatie worden gebruikt waarmee aangegeven wordt in welke situaties welke download resoluties beschikbaar zijn.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Presentatie profielen <../Concepts/Concepts.html#presentatie-profielen>`__