
Publicatiekosten
^^^^^^^^^^^^^^^^

In dit paneel worden de publicatiekosten gedefinieerd. 

**Filteren**

De lijst van publicatiekosten kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een publicatiekost wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de tabel waarin de gegevens van de publicatiekost ingevoerd worden. Met het **potlood** pictogram komt de regel beschikbaar waarmee de gegevens van de publicatiekost gewijzigd worden.

Voor het toevoegen of wijzigen zijn de volgende velden beschikbaar:

-  **Bedrag**: Het bedrag dat wordt doorberekend.
-  **Minimale hoeveelheid**: De minimale hoeveelheid (kan in aantallen of seconden zijn) dat kan worden afgenomen.
-  **Van**: de hoeveelheid waar vanaf de kosten worden doorberekend.
-  **Tot**: de hoeveelheid tot waarvoor de kosten worden doorberekend.
-  **Blokgrootte**: De eenheid waarin afgeleverd wordt. Bv. in seconden voor bewegend beeld of audio.
-  **Per eenheid**: Geeft aan of het bedrag per eenheid of voor de gehele staffel wordt doorberekend.

**Verwijderen**

Met het **prullenbak** pictogram wordt de publicatiekost, na bevestiging, verwijderd. 

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__