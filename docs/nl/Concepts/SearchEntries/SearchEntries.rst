
Zoekingangen
~~~~~~~~~~~~

In Atlantis kan een organisatie de zoekingangen helemaal naar eigen smaak inrichten.

Zoekingangen worden gedefinieerd om daarmee het zoeken op objecten van metadata modellen mogelijk te maken. Zoekingangen gelden voor de beheer- of publieksomgeving of voor beide. 

**Metadata modellen**

Een zoekingang kan ondergebracht zijn onder een andere zoekingang. Dit vertaalt zich naar een boomstructuur. In de beheer- en publieksomgevingen komen de zoekingangen dan ook hiërarchisch beschikbaar.

Aan zoekingangen worden metadata modellen toegewezen. Hiermee wordt aangegeven dat de zoekingang de objecten van die metadata modellen doorzoekt. De toegewezen metadata modellen zijn ook in de boomstructuur opgenomen.

Iedere zoekingang staat op zich in de toewijzing van de metadata modellen. Dus als een zoekingang subzoekingangen heeft, dan zal de zoekingang niet automatisch zoeken op de metadata modellen van de subzoekingang. Als dit gewenst is zullen de metadata modellen expliciet toegewezen dienen te worden aan de zoekingang.

**Beschikbaarheid zoekingangen**

Of zoekingangen ook daadwerkelijk beschikbaar komen is afhankelijk van de rechten van de gebruiker op de te doorzoeken objecten. Daarbij wordt een algoritme uitgevoerd die kijkt naar de segmenttoewijzing van objecten in combinatie met de rechten van de gebruiker op die segmenten, via de segmenttoewijzing van gebruikers binnen gebruikersgroepen of het standaard segment van de gebruiker.

**Zoekformulieren**

Aan de hand van de combinatie van metadata modellen die doorzocht worden door een zoekingang wordt een standaard zoekformulier gegenereerd. Dit is het zoekformulier waarin de zoekvelden met overeenkomstige benaming in de volgorde van de volgnummers worden gepresenteerd met 1 zoekveld per regel.

Op een zoekingang kunnen ook alternatieve zoekformulieren gedefinieerd en aan gebruikersgroepen toegewezen worden. Een gebruiker kan dan kiezen uit eventuele alternatieve zoekformulieren om de zoekvraag op te stellen.

**Uitwerking binnen Atlantis**

De zoekingangen komen in de beheeromgeving beschikbaar via het Zoeken menu. Dit levert de zoekingangen in de vorm van zoekingangen, subzoekingangen, subsubzoekingangen, etc. In de publieksomgeving staan de zoekingangen en subzoekingangen onder het kopje "Zoeken in".

