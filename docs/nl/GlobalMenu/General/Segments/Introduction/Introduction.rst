
Segmenten
---------

Beheer Segmenten
^^^^^^^^^^^^^^^^

In dit paneel worden de Segmenten gedefinieerd. Deze worden automatisch en/of handmatig toegewezen aan objecten van metadata modellen. Segmenten worden gebruikt voor de autorisatie van gebruikers door deze in gebruikersgroepen te plaatsen voor toegewezen segmenten.

**Filteren**

De lijst van Segmenten kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een segment wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van het segment ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van het segment gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt het segment, na bevestiging, verwijderd. Dit kan alleen als deze niet meer wordt gebruikt bij metadata modellen, objecten van metadata modellen en bij de autorisatie.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Autorisatie <../Concepts/Concepts.html#autorisatie>`__