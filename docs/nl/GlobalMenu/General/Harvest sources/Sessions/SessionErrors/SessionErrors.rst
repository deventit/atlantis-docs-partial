
Fouten
^^^^^^
In dit paneel worden de opgetreden fouten van de harvest sessie gepresenteerd naar datum/tijd en de opgetreden fout.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `OAI-PMH <../Concepts/Concepts.html#oai-pmh>`__