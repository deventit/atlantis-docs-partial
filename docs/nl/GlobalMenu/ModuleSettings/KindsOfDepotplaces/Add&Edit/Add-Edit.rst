
Invoer Soorten Depotplaatsen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voor het wijzigen van een soort depotplaats zijn in dit paneel de volgende velden beschikbaar:

-  **Naam**: Naam van de soort depotplaats.
-  **Metadata model**: Selectie van het metadata model van de soort depotplaats.
-  **Bevat**: Selectie van het metadata model van de soort depotplaatsen die het bevat.
-  **Aantal items bevat**: Aantal depotplaatsen van "Bevat" metadata model.
-  **Laagste niveau**: Vlag die aangeeft of op deze soort van depotplaats rechtstreeks geplaatst kan worden.
-  **Hoogte**: Hoogte van soort depotplaats.
-  **Breedte**: Breedte van soort depotplaats.
-  **Diepte**: Diepte van soort depotplaats.

Bij het aanmaken van een object van deze soort depotplaats worden automatisch ook een "Aantal items bevat" objecten van de soort "Bevat" metadata model aangemaakt en gekoppeld met deze depotplaats.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__