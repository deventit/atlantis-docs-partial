
Standaard toeslagen
-------------------

Standaard toeslagen zijn toeslagen die aan een item in een bestelling of voor een hele bestelling toegevoegd worden.

Voor het definiëren van standaard toeslagen wordt een paneel aan de rechterzijde geopend met een tabel van standaard toeslagen. Een standaard toeslag wordt gedefinieerd via de volgende velden:

-  **Beschrijving**: Beschrijving van de toeslag.
-  **Bedrag**: Bedrag dat voor de toeslag dat in rekening wordt gebracht.
-  **IsDigitaal**: Geeft aan of het van toepassing is op een digitaal artikel in de bestelling (bv. om een CD-ROM in rekening te brengen).
-  **Per artikel**: Geeft aan dat de toeslag per artikel berekend wordt. Anders geldt de toeslag voor de winkelwagen
-  **Is vaste toeslag**: Geeft aan dat de toeslag voor iedere bestelling berekend wordt.

**Filteren**

De lijst van standaard toeslagen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een standaard toeslag wordt toegevoegd door in de tabel op het **+** pictogram (nieuwe toevoegen) te klikken. Hiermee komt een nieuwe regel beschikbaar waarin de gegevens van de standaard toeslag ingevoerd worden. Met het **potlood** pictogram komt de regel beschikbaar waarin de gegevens van de standaard toeslag gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de standaard toeslag, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__
