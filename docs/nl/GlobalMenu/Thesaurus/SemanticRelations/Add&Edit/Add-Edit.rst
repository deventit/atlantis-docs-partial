
Invoer Semantische Relaties
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voor het wijzigen van een semantische relatie zijn in dit paneel de volgende velden beschikbaar:

-  **Naam**: Naam van de relatie.
-  **Beschrijving**: Omschrijving van de relatie.
-  **Ouder/kind**: Vlag die aangeeft of het een ouder/kind (hiërarchische) relatie betreft.
-  **Equivalent**: Vlag die aangeeft of het een equivalentie (gelijkwaardige) relatie betreft.
-  **Transitief**: Vlag die aangeeft of het een transitieve relatie betreft.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__