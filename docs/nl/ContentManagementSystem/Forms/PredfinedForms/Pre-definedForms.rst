
Invoer voorgedefinieerd formulier
---------------------------------

Met dit paneel worden vooraf gedefinieerde formulieren toegevoegd of gewijzigd. 

Formulieren kunnen toegewezen worden aan gebruikersgroepen. Gebruikers krijgen alleen de beschikking over formulieren voor invoer en mutatie als deze:

- zijn toegewezen aan de gebruikersgroepen waar de gebruiker deel van uitmaakt 
- niet toegewezen zijn aan gebruikersgroepen

Daarnaast kan een formulier als "Standaard" worden aangemerkt. Hiermee is dit dan het formulier dat standaard gepresenteerd wordt aan de gebruiker bij invoer of mutatie van het object. De gebruiker heeft daarnaast de mogelijkheid om voor andere, toegewezen, formulieren te kiezen.

Een vooraf gedefinieerd formulier heeft een aantal instellingen die ingevoerd dienen te worden:

-  **Naam**: Dit is de naam van het formulier zoals die aan gebruikers gepresenteerd zal worden
-  **URL**: De link naar het formulier. Deze heeft de opbouw: *DynamischeSchermen/Xmlbeschrijving?template=NAAM VAN FORMULIERTEMPLATE* 
-  **Groepen**: Selecteer hier de gebruikersgroepen waar het formulier aan toegekend wordt
-  **Standaard**: Geef hiermee aan of dit formulier het standaard formulier is voor de geselecteerde gebruikersgroepen.

Met de **Opslaan** knop wordt de definitie opgeslagen. Met **Reset** worden de instellingen teruggezet naar de waarden bij openen van de definitie of de laatst bewaarde instellingen.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, , `Formulieren <../Concepts/Concepts.html#formulieren>`__