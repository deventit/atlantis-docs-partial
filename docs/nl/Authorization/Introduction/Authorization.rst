
Autorisatie
***********

Via Autorisatie uit het Dashboard worden gebruikers geautoriseerd voor toegang tot de functies van de beheer- en publieksomgeving. Dit gebeurt via:

-  Het definiëren van groepen
-  Het registreren van gebruikers
-  Toekennen van rechten aan groepen
-  Toekennen van gebruikers aan groepen, eventueel voor toegewezen segmenten

**Panelen**

Het Autorisatiescherm bestaat uit 2 panelen. Het linkerpaneel geeft een overzicht van de groepen. Het rechterpaneel een overzicht van de gebruikers in de geselecteerde groepen. Om alle gebruiker te presenteren in het rechterpaneel, kan op het “\ **oog**\ ” pictogram geklikt worden.

**Filteren**

Zowel de lijst van groepen als van gebruikers kan gefilterd worden door in het “**Zoek hier**” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen van een nieuwe gebruikersgroep**

Een gebruikersgroep wordt toegevoegd door in het paneel met de gebruikersgroepen op het “**+**” pictogram (nieuwe toevoegen) te klikken. Dit geeft een pop-up scherm waarin de gegevens van de groep ingevoerd wordt.

**Toevoegen van een nieuwe gebruiker**

Een gebruiker wordt toegevoegd door in het paneel met de gebruikers op het “**+**” pictogram (nieuwe toevoegen) te klikken. Dit geeft een pop-up scherm waarin de gegevens van de gebruiker ingevoerd wordt.

**Muteren**

Met het “**potlood**” pictogram wordt een pop-up venster opgeroepen waarmee de gegevens van de gebruiker of gebruikersgroep gemuteerd wordt.

**Verwijderen**

Groepen en gebruikers worden verwijderd met het “**prullenmand**” pictogram in het desbetreffende paneel. Een groep kan alleen verwijderd worden als deze geen gebruikers meer bevat. Een gebruiker kan alleen verwijderd worden als deze niet meer in (andere) groepen zit. In het mutatiescherm voor gebruikers kunnen gebruikers losgekoppeld worden van groepen. Nadat de gebruiker niet meer gekoppeld is aan enige groep kan deze verwijderd worden. Als alle gebruikers van een groep losgekoppeld zijn, kan de groep verwijderd worden.

**Toekennen rechten aan groepen**

Met het “**R**” pictogram in het paneel van de groepen worden rechten toegekend aan groepen.sdfsdfsdfsdffsdfsdf


**Presenteren alle gebruikers**

Alle gebruikers worden opgeroepen via het “**oog**” pictogram bovenaan het paneel van de gebruikersgroepen. Het paneel met de gebruikers presenteert dan alle gebruikers.

**Zie ook**: `Algemene Bedieningselementen <../General/General.html#MainGeneral>`__, `Rechten <../Authorization/Authorization.html#toekennen-rechten-aan-gebruikersgroepen>`__, `Autorisatie <../Concepts/Concepts.html#autorisatie>`__
