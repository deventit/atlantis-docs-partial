
Download Resoluties
-------------------

Beheer Download Resoluties
^^^^^^^^^^^^^^^^^^^^^^^^^^

In dit paneel worden de Download Resoluties gedefinieerd. Deze worden gebruikt bij de Presentatie Profielen om voor een Presentatie Profiel voor diverse situaties de gebruiker of bezoeker de keuze te geven uit geschikte download resoluties.

**Filteren**

De lijst van Download Resoluties kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een download resolutie wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van de download resolutie ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de download resolutie gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de download resolutie, na bevestiging, verwijderd. Dit kan alleen als deze niet meer wordt gebruikt bij een Presentatie Profiel.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Presentatie profielen <../Concepts/Concepts.html#presentatie-profielen>`__