
Organisaties
~~~~~~~~~~~~

In het Globale menu onder het kopje "\ **Autorisatie**\" staat optie "\ **Organisaties**\". Organisaties kunnen toegekend worden aan gebruikers en gebruikersdiensten. In dit paneel worden de organisaties onderhouden. Gepresenteerd wordt de lijst van organisaties.

**Filteren**

De lijst van organisaties wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een organisatie wordt toegevoegd door in het paneel met de organisaties op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst waarin de naam van de organisatie ingevoerd wordt.

**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij aanpassing van de naam van de organisatie.

**Verwijderen**

Een organisatie wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel. Een organisatie kan alleen verwijderd
worden als deze niet meer gebruikt wordt.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Gebruikers <../Authorization/Authorization.html#gebruikers>`__, `Autorisatie <../Concepts/Concepts.html#autorisatie>`__