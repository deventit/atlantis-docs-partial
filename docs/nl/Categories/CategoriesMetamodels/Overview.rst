
Hiërarchische indeling
~~~~~~~~~~~~~~~~~~~~~~

Categorieën worden hiërarchisch gestructureerd volgens een categorie schema.

De categorie schema wordt gepresenteerd via een boomstructuur. De boom bevat knopen en subknopen. Iedere knoop of subknoop is een categorie. Met de **+** of **-** voor een knoop wordt deze uit- of ingeklapt. De **groene vink** of **zwarte** vink voor een knoop geeft aan of de categorie gemodereerd of gedemodereerd is.

**Filteren**

De boom wordt gefilterd door in het "Zoek hier" veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen op het hoogste niveau**

Categorieën worden op het hoogste niveau toegevoegd door op het **+** pictogram te klikken. Dit opent een extra paneel aan de rechterkant voor de invoer van de gegevens. Het linkerpaneel klapt automatisch in.

**Toevoegen op een onderliggend niveau**

Categorieën worden op een onderliggend niveau toegevoegd door met de rechtermuisknop op een categorie te klikken en in het context menu dat dan verschijnt te kiezen voor **Nieuwe knoop**. Dit opent een extra paneel aan de rechterkant voor de invoer van de gegevens. Het linkerpaneel klapt automatisch in.

**Bewerken**

Een categorie wordt bewerkt door met de rechtermuisknop op een categorie te klikken en in het context menu dat dan verschijnt te kiezen voor **Bewerken**. Dit opent een extra paneel aan de rechterkant voor de invoer van de gegevens. Het linkerpaneel klapt automatisch in.

**Verwijderen**

Categorieën worden verwijderd door deze te selecteren. Het selecteren van categorieën gebeurt door deze aan te klikken. Door de <SHIFT> of <CRTL> toets ingedrukt te houden bij het selecteren worden meerdere categorieën geselecteerd (werkt niet op alle browsers/besturingssystemen). Vervolgens wordt er verwijderd op één van de onderstaande manieren:

- Door op het **prullenmand** pictogram te klikken worden de geselecteerde categorieën, na bevestiging, verwijderd.
- door met de rechtermuisknop op een categorie te klikken en in het context menu dat dan verschijnt te kiezen voor **Verwijderen**, **Verwijderen inclusief subknopen** of **Verwijderen subknopen**, wordt na bevestiging en voor de categorie waarvoor het context menu is opgeroepen, de gekozen verwijdering uitgevoerd.

**Verplaatsen**

Categorieën worden verplaatst door deze te verslepen of te knippen en te plakken.

-  **Verslepen**: Met de muis wordt een categorie versleept **voor**, **op** of **na** de doelcategorie. Als versleept wordt **op** de doelcategorie wordt de geselecteerde categorie onderliggend aan de doelcategorie geplaatst.
-  **Knippen/plakken**: Met de rechtermuistoets wordt voor de categorie het context menu opgeroepen en gekozen voor **Knippen**. Vervolgens wordt op de doelcategorie met de rechtermuistoets het context menu opgeroepen en gekozen voor **Voor**, **Onderliggend**, of **Na**.

Bij het verplaatsen worden de onderliggende categorieën mee verplaatst.

**Kopiëren**

Categorieën worden gekopieerd met de rechtermuistoets voor de categorie het context menu op te roepen en te kiezen voor **Kopieer lijst**. Vervolgens wordt op de doelcategorie met de rechtermuistoets het context menu opgeroepen en gekozen voor **Voor**, **Onderliggend**, of **Na**.

Bij het kopiëren worden de onderliggende categorieën mee verplaatst.

**Modereren**

Categorieën worden gemodereerd of gedemodereerd door deze te selecteren en te klikken op de moderatie pictogrammen bovenaan het paneel:

- Met het **groene vink** pictogram worden de geselecteerde categorieën gemodereerd.
- Met het **zwarte vink** pictogram worden de geselecteerde categorieën gedemodereerd.

Bij het modereren of demodereren wordt gevraagd of ook de onderliggende categorieën gemodereerd/gedemodereerd moeten worden. Door hierop bevestigend te antwoorden wordt dit uitgevoerd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Categorieën <../Concepts/Concepts.html#categorieen>`__