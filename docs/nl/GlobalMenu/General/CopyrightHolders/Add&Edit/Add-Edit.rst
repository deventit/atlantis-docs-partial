
Invoer Auteursrechthouder
^^^^^^^^^^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen van een auteursrechthouder zijn in dit paneel de volgende velden beschikbaar:

-  **Naam**
-  **Adres**
-  **Postcode**
-  **Provincie**
-  **Woonplaats**
-  **Land**
-  **Telefoonnummer**
-  **Fax**
-  **Website**
-  **Email**
-  **Jaar van overlijden**

Naast de bovenstaande contactgegevens worden de afspraken over het gebruik van hun werken geregistreerd via de volgende velden:

**Presentatie profiel**

Hier wordt een presentatie profiel geselecteerd. Deze wordt toegepast op gekoppelde bestanden als daar een auteursrechthouder aan gekoppeld is.

**Toestemming voor privégebruik nodig**

Als aangevinkt, moet de besteller van een reproductie voor privégebruik toestemming vragen aan de auteursrechthouder. Contactgegevens van de auteursrechthouder wordt opgenomen in de bestelling.

**Toestemming voor publicatie nodig**

Als aangevinkt, moet de besteller van een reproductie voor publicatie toestemming vragen aan de auteursrechthouder. Contactgegevens van de auteursrechthouder wordt opgenomen in de bestelling.

**Publicatie op Internet toegestaan**

Als aangevinkt, mag de organisatie de werken op internet publiceren. Met deze optie komen ook de Creative Commons rechten beschikbaar die aan de publicatie meegegeven moeten worden. Hierbij worden de volgende afkortingen in combinatie gebruikt:

    -  **BY**: het kopiëren, distribueren, vertonen en uitvoeren van het werk en afgeleide werken is toegestaan op voorwaarde van het vermelden van de oorspronkelijke auteur.
    -  **SA**: het distribueren van afgeleide werken is alleen toegestaan onder een identieke licentie.
    -  **NC**: het kopiëren, distribueren, vertonen en uitvoeren van het werk en afgeleide werken mag voor niet-commerciële doeleinden.
    -  **ND**: het kopiëren, distribueren, vertonen en uitvoeren van het werk is toegestaan, maar niet het veranderen van het werk.

De gangbare combinatie van deze licenties zijn aan te klikken. Er kan één combinatie gekozen worden. Het nogmaals aanklikken zet de licentie uit.

**Toestemming voor publicatie op sites van derden**

Als aangevinkt, mag de organisatie de werken op sites van derden publiceren. Dit betekent dat het doorgegeven mag worden via koppelingen en exports voor gebruik op sites van andere organisaties en personen.

**Reproductie door derden toegestaan**

Als aangevinkt, mag de organisatie de werken aanbieden voor reproductie door de organisatie zelf. Is deze niet aangevinkt kunnen de werken niet via de site van de organisatie besteld worden voor reproductie.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Auteursrechthouders <../Concepts/Concepts.html#auteursrechthouders>`__