
Autorisatie
~~~~~~~~~~~

Het autorisatiesysteem van Atlantis zorgt ervoor dat gebruikers alleen die mogelijkheden krijgen die horen bij hun verantwoordelijkheden en bevoegdheden. Dit gebeurt
via:

-  het definiëren van groepen
-  het registreren van gebruikers
-  toekennen van rechten aan groepen
-  toekennen van gebruikers aan groepen, eventueel voor toegewezen
   segmenten.

**Groepen en rechten**

Groepen vertegenwoordigen de rollen. Zo kunnen er groepen zijn voor
beheerders, medewerkers, vrijwilligers, etc. Wat een groep aan
toestemmingen geeft wordt bepaald door de toegekende rechten aan de
groep. Gebruikers kunnen deel uitmaken van meerdere groepen. De
toestemmingen die een gebruiker daarmee krijgt zijn de opgetelde
toestemmingen vanuit de groepen waar de gebruiker aan toegekend is.

**Segmenten**

Als een gebruiker het recht krijgt om objecten te lezen, dan zou dat
betekenen dat de gebruiker toegang voor lezen zou hebben op alle
objecten van alle metadata modellen. Om dit te beperken kunnen segmenten
gebruikt worden. Als een object wordt aangemaakt, dan wordt het
toegewezen aan een standaard segment. Welk segment dit is, wordt bepaald
door de segment toewijzing op het metadata model van dat object. Een
gebruiker kan toegewezen worden aan een groep onder vermelding van de
segmenten waarvoor deze toewijzing geldt. Zo kan een gebruiker de rol
beheerder hebben voor de objecten uit het ene segment en de rol
raadpleger of geheel geen toegang hebben op objecten uit een ander
segment. Een segment is te beschouwen als een soort van directory. Door
de plaatsing van objecten in een segment wordt het in deze directory
geplaatst. Als een gebruiker aan een groep wordt toegewezen voor
bepaalde segmenten worden dan de rechten voor die gebruiker op de
directory gezet.

Ook aan een gebruiker kan een standaard segment worden toegekend. Dit
segment overschrijft het segment van het metadata model. Als een
gebruiker een standaard segment toegewezen heeft gekregen, betekent dit
dat ieder object dat door de gebruiker aangemaakt wordt, automatisch dit
segment toegewezen krijgt in plaats van het standaard segment van het
metadata model van het object.

Op een object kan, indien de gebruiker daar rechten voor heeft,
handmatig de segment toewijzing aanpassen. Een object kan daarbij ook
meerdere segmenten toegewezen krijgen.

Door de combinatie van groepen, rechten en segmenten kunnen de
mogelijkheden van gebruikers op objecten tot op het niveau van het
individuele object geregeld worden.

**Veiligheidsniveaus**

Veiligheidsniveaus worden gebruikt om gebruikers toegang te geven tot objecten van het eigen veiligheidsniveau of lager. Daarvoor wordt bij een gebruikersgroep het veiligheidsniveau aangegeven, waarmee op de gebruikers die deel uitmaken van deze groep het desbetreffende veiligheidsniveau van toepassing is. Deze wordt ook toegekend aan afzonderlijk objectbeschrijvingen en de daaraan gekoppelde bestanden.


**Dynamische segmenttoewijzing**

Atlantis kent een methodiek voor dynamische segmenttoewijzing. Dit wordt geregeld via "Rechten Statussen". Dit wordt ingericht door voor metadata modellen rechten statussen toe te kennen. Dit zijn definities van statussen waarvoor aangegeven wordt welke segmenten toegevoegd worden aan het object als de status van toepassing is en verwijderd worden als de status verlaten wordt. Door de wijzigingen in segmenten krijgen gebruikers vanaf dat moment andere rechten via de lidmaatschap van groepen waar ze deel van uitmaken voor de betreffende segmenten.

**Anonieme gebruikers**

Anonieme gebruikers zijn gebruikers die zonder in te loggen toegang hebben tot de publieks- of collectiebeheeromgeving. 

Voor anonieme gebruikers dient een gebruikersaccount beschikbaar te zijn waarin de vlag "Niet geregistreerd" aangevinkt is. Door deze gebruiker aan een gebruikersgroep toe te wijzen krijgt deze daarmee de rechten die op deze gebruikersgroep van toepassing zijn. 

**Geregistreerde gebruikers**

Anonieme gebruikers kunnen zich registreren via de publieks- of collectiebeheeromgeving. Deze gebruikers worden automatisch toegewezen aan de gebruikersgroep waarin de vlag "Nieuwe gebruikers" aangevinkt is. De geregistreerde gebruiker krijg daarmee de rechten die op deze gebruikersgroep van toepassing zijn. 

Indien een geregistreerde gebruiker meer of andere rechten nodig heeft dan die van de standaard geregistreerde gebruikers dient de gebruikers aan de passende gebruikersgroepen gekoppeld te worden.

**Autorisatie op veldniveau**

Het autorisatiesysteem zoals hierboven beschreven regelt de toegang tot softwarefuncties en objecten. Hiermee wordt niet de toegang op veldniveau van de metadata van de objecten geregeld. Dit wordt gedaan via de definitie en beschikbaarstelling van formulieren. Formulieren worden, indien de Schermbouwer dienst deel uitmaakt van de licentie, naar behoefte samengesteld en kunnen gekoppeld worden aan gebruikersgroepen. Via de formulieren wordt dan geregeld tot welke velden de gebruiker toegang heeft op veldniveau, voor raadplegen en muteren.

**Uitwerking binnen Atlantis**

De ingestelde autorisaties werken door de gehele Atlantis omgeving door bij de mogelijkheden voor:

- het menu voor het toevoegen van nieuwe objecten
- het menu voor het zoeken op objecten
- de objecten in zoekresultaten
- het muteren van objecten
- de beschikbaarheid van programmafuncties
- etc. 
