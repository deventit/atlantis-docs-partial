
Thesaurus
~~~~~~~~~

Dit scherm presenteert de verkenner voor de binnen de applicatie opgebouwde thesauri via een hiërarchische indeling. De hiërarchische indeling wordt gepresenteerd via een boomstructuur. De boom bevat knopen en subknopen. Iedere knoop of subknoop is een onderdeel van een thesaurus. Met de “\ **+**\ ” of “\ **-**\ ”  voor een knoop wordt deze uit- of ingeklapt.

Een thesaurus bestaat uit 3 soorten knopen. Bovenaan de Verkenner staan de kleuren aangegeven waarmee deze knopen worden weergegeven:

-  **Domein**: Dit is het startpunt voor een thesaurus.
-  **Concept groep**: Dit is een knoop die concept groepen en concepten kan bevatten.
-  **Concept**: Dit is de definitie van een term uit de thesaurus.

Rechtsboven in het scherm kan de “\ **Standaard Taal**\ ” worden aangegeven. Deze wordt standaard gevuld met de keuze hiervoor in de **Systeeminstellingen**.

**Filteren**

De thesauri worden gefilterd door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. Dit geeft alle knopen op het hoogste niveau die knopen bevatten waarvan de waarde voldoet aan het ingevoerde filter.

**Toevoegen op het hoogste niveau**

Een knoop op het hoogste niveau, dit zal doorgaans een **Domein** zijn, wordt toegevoegd met het “\ **+**\ ” pictogram bovenaan het scher. Dit opent de invoer van de eigenschappen van deze knoop aan de rechterkant.

**Toevoegen op een onderliggend niveau**

Knopen worden op een onderliggend niveau toegevoegd door met de rechter muisknop op een knoop te klikken en in het context menu dat dan verschijnt te kiezen voor “\ **Nieuwe knoop**\ ”. Dit opent de invoer van de eigenschappen van deze knoop aan de rechterkant.

**Bewerken**

Een knoop wordt bewerkt door met de rechtermuisknop op een knoop te klikken en in het context menu dat dan verschijnt te kiezen voor “\ **Bewerken**\ ”. Dit opent de invoer van de eigenschappen van deze knoop aan de rechterkant.

**Verwijderen**

Knopen worden verwijderd door deze te selecteren. Het selecteren van knopen gebeurt door deze aan te klikken. Door de <SHIFT> of <CRTL> toets ingedrukt te houden bij het selecteren worden meerdere knopen
geselecteerd. Vervolgens wordt er verwijderd op één van de onderstaande manieren:

-  Door op het “\ **prullenmand**\ ” pictogram te klikken worden de geselecteerde knopen, na bevestiging, verwijderd.
-  Door met de rechtermuisknop op een knoop te klikken en in het context menu dat dan verschijnt te kiezen voor “\ **Verwijderen**\ ”, “\ **Verwijderen inclusief subknopen**\ ” of “\ **Verwijderen subknopen**\ ”, wordt na bevestiging en voor de knoop waarvoor het context menu is opgeroepen, de gekozen verwijdering uitgevoerd.

**Verplaatsen**

Knopen worden verplaatst door deze te verslepen of te knippen en te plakken.

-  **Verslepen**: Met de muis wordt een knoop versleept **voor**, **op** of **na** de doelknoop. Als versleept wordt **op** de doelknoop wordt de geselecteerde knoop onderliggend aan de doelknoop geplaatst.
-  **Knippen/plakken**: Met de rechtermuistoets wordt voor de knoop het context menu opgeroepen en gekozen voor “\ **Knippen**\ ”. Vervolgens wordt op de doelknoop met de rechtermuistoets het context menu opgeroepen en gekozen voor “\ **Voor**\ ”, “\ **Onderliggend**\ ”, of “\ **Na**\ ”.

Bij het verplaatsen worden de onderliggende knopen mee verplaatst.

**Kopiëren**

Knopen worden gekopieerd met de rechtermuistoets voor de knoop het context menu op te roepen en te kiezen voor “\ **Kopieer lijst**\ ”. Vervolgens wordt op de doelknoop met de rechtermuistoets het context menu opgeroepen en gekozen voor “\ **Voor**\ ”, “\ **Onderliggend**\ ”, of “\ **Na**\ ”.

Bij het kopiëren worden de onderliggende knopen mee verplaatst.

**Verplaats knoop naar het hoogste niveau**

Een knoop worden verplaatst naar het hoogste niveau door met de rechtermuistoets voor de knoop het context menu op te roepen en te kiezen voor “\ **Verplaats naar bovenste niveau**\ ”.

Bij het verplaatsen worden de onderliggende knopen mee verplaatst.

**Knopen splitsen**

Een knoop wordt gesplitst door met de rechtermuistoets voor de knoop het context menu op te roepen en te kiezen voor “\ **Splitsten**\ ”. Dit opent de invoer van de eigenschappen van de nieuwe knoop aan de
rechterkant. De gesplitste knoop krijgt een equivalentie relatie met de knoop waar vanaf gesplitst is. De equivalentie relatie dient als **semantische relatie** derhalve beschikbaar te zijn binnen de thesaurus.

**Alfabetisch ordenen**

De knopen van een bovenliggende knoop worden alfabetisch geordend door met de rechtermuistoets voor de voor de bovenliggende knoop het context menu op te roepen en te kiezen voor “\ **Alfabetisch ordenen**\ ”.

**Samenvoegen concepten**

Met het “\ **boom**\ ” pictogram bovenaan het scherm wordt een kopie van de boom aan de rechterkant weergegeven voor het samenvoegen van knopen. Selecteer hierbij een knoop in beide bomen om deze samen te voegen.

Tussen de bomen wordt de samenvoeging nader gespecificeerd in termen van relaties die met de samenvoeging meegenomen moeten worden.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__
