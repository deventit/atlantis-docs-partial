
Categorieën
***********

Het paneel link presenteert de categorie schema's. Met categorie schema's worden categorie schemas gemaakt voor de objecten. Als een metadata model is aangemerkt als "Categorie Schema" dan komt de naam van dit metadata model in de lijst in het linkerpaneel te staan. Hiervoor kan dan een hiërarchisch indeling opgezet worden, bv. een de indeling van het archievenoverzicht, een geografisch indeling met werelddelen, landen, etc. of een andere indeling.

**Filteren**

De lijst van categorie schemas kan gefilterd worden door in het "Zoek hier" veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Opbouwen van een categorie schemas**

Door op de naam van een categorie schema te klikken komt de indeling in een extra paneel aan de rechterkant beschikbaar. Het linkerpaneel klapt dan automatisch in.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Categorieën <../Concepts/Concepts.html#categorieen>`__