
Categorieën
~~~~~~~~~~~

Met categorieën worden categorie schemas gemaakt. Een categoriesysteem bestaat uit een metadata model dat is aangemerkt als "Categorie Schema". Niet ieder metadata model komt daarvoor in aanmerking. Een categorie schema dient een specifieke beschrijvingsstructuur te hebben die wordt geleverd door de toptabel RUB_RUBRIEKEN.

Door een relatie tussen een metadata model van een categorieschema en een metadata model van objecten kunnen deze objecten via een koppeling met categorieën hiërarchisch ingedeeld worden. Bij het koppelen vanuit een object met een categorie wordt een boomstructuur van het categorie schema gepresenteerd. Uit de categorieën wordt dan een categorie geselecteerd en gekoppeld.

**Navigatie**

Een categorieschema kan gebruikt worden om hiërarchisch (systematisch) door de gekoppelde objecten te navigeren. Hiertoe dient het categorie schema aangemerkt te worden als "Navigeerbaar" en "Inclusief gelinkte documenten".

**Systematisch Zoeken**

Een categorieschema kan gebruikt worden om hiërarchisch (systematisch) door de gekoppelde objecten te zoeken. Hiertoe dient het categorie schema aangemerkt te worden als "Zoekboom". Dit systematisch zoeken komt beschikbaar bij het zoeken op objecten van metadata modellen die een relatie hebben met het categorie schema.

**Codering en volgorde**

Categorieschema's kenmerken zich doorgaans door het toepassen van een codering van de categorieën. De code van een categorie wordt dan opgebouwd uit de codes van de bovenliggende categorieën en de code van de categorie, per niveau gescheiden door scheidingstekens, bv. 1.A4-12.1, wat voor de verschillende niveaus (1, A4, 12, 1) de codes van categoriën representeert. Voor een categorie wordt dan ook de code op dat niveau aangegeven (bv. 12) met het scheidingsteken waarmee het van de codering van de bovenliggende niveaus wordt gescheiden (bv. -).

Bij invoer van categorieën wordt niet gelet op de code voor wat betreft de volgorde. Een volgorde wordt bepaald door categoriën binnen hun niveau in volgorde te slepen. Categorieën kunnen ook naar andere niveaus gesleept worden.

**Uitwerking binnen de Atlantis beheer- en publieksomgeving**

Categorieschema's worden aangeboden om daarmee hiërarchisch te navigeren door objecten. Dit gebeurt via het Navigatie menu en via het Systematisch zoeken op object.
