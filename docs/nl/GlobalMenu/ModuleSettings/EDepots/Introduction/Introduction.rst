
E-Depots
--------

Beheer E-Depots
^^^^^^^^^^^^^^^

E-Depots definieert de E-Depots. Er kunnen vrij E-Depots gedefinieerd worden. 

**E-Depots**

Het scherm voor de E-Depots presenteert een paneel aan de linkerzijde. Dit geeft een overzicht van de beschikbare E-Depots. 

**Filteren**

De lijst met E-Depots kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. 

**Toevoegen** 

Met het **toevoegen** pictogram boven de lijst met E-Depots wordt een nieuwe E-Depot toegevoegd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de E-Depot wordt weergegeven voor invoer.

**Wijzigen** 

Met het **potlood** pictogram naast de naam van een E-Depot wordt een E-Depot gewijzigd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de E-Depot wordt weergegeven voor mutatie.

**Verwijderen** 

Met het **prullenbak** pictogram wordt een E-Depot verwijderd. Dit kan alleen als de E-Depot niet in gebruik is.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__