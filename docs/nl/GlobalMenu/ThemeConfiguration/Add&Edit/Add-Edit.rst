
Invoer Thema Configuratie
-------------------------

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Naam**: De naam van het Thema. 
-  **Logo**: Selecteer hier een logo in de vorm van een beeldbestand. 

Onder het kopje **Kleur** worden de diverse kleurstellingen aangegeven:

-  **Primaire kleur**: De kleur van koppen en knoppen.
-  **Secundaire kleur**: De kleur van de kruimelpaden.
-  **Tertiaire kleur**: De kleur van kolommen.
-  **Icoon kleur**: De kleur van pictogrammen.
-  **Fontkleur**: De kleur van teksten.

Onder het kopje **Typografie** worden de diverse typografieën aangegeven:

-  **Primair lettertype**: Lettertype van koppen en knoppen.
-  **Primaire lettergrootte**: Lettergrootte van koppen en knoppen.
-  **Secundaire lettergrootte**: Lettergrootte van de kruimelpaden.
-  **Tertiaire lettergrootte**: Lettergrootte van kolommen.

**Voorvertoning**

De instellingen voor het Thema wordt met de knop  **Voorvertoning** geraadpleegd.

**Activeren Thema**

Met deze knop wordt het Thema geactiveerd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__
