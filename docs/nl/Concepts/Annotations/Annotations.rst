
Annotaties
~~~~~~~~~~

Annotaties zijn op- en aanmerkingen die een bezoeker via de publieksomgeving kan doorgeven op een object. De dienst "Annotaties" dient daarvoor deel uit te maken van de licentie.

**Annotatietypes**

Met annotatietypes wordt gedefinieerd wat voor soort annotaties op objecten doorgegeven kunnen worden. Bv. een opmerking, aanvullingen, correctie, etc. Een annotatietype kan voorzien worden van een icoon waarmee visueel een onderscheid tussen de annotatietypes gemaakt wordt.

Als een bezoeker een annotatie wil doorgeven, dan zal het eerst het annotatietype selecteren om daarmee aan te geven wat voor soort annotatie het betreft.

**Annotatievelden**

Voor metadata modellen kan aangegeven worden welke annotatievelden beschikbaar zijn. Bv. naam, datering, onderwerp of beschrijving. Na het selecteren van het annotatietype moet de bezoeker een annotatieveld selecteren om daarmee de annotatie verder te classificeren.

**Bestanden**

Een bezoeker kan bij een annotatie een bestand toevoegen. Dit kan een afbeelding, audio/video bestand of anderszins zijn.

**Beheer van annotaties**

Annotaties worden in de collectiebeheeromgeving in een verzamelscherm samengebracht. Vanuit dit scherm worden de annotaties beheerd. De collectiebeheerder kan annotaties modereren, waarmee deze goedgekeurd worden, of de annotaties verwerken in de objectbeschrijvingen. Annotaties worden ook in een eigen tabblad bij de obejcten gepresenteerd. Ook vandaaruit kunnen deze beheerd worden.

**Publicatie van annotaties**

Als in de systeeminstellingen "Alleen gemodereerde annotaties publiceren" aangevinkt is, worden alleen de annotaties die gemodereerd zijn gepubliceerd bij de presentatie van de detailinformatie van het object. Is deze optie niet aangevinkt, dan wordt een annotatie direct gepubliceerd.





Met annotatievelden wordt gedefinieerd op welke velden een bezoeker annotaties kan toevoegen. Als er geen annotatievelden zijn gedefinieerd wordt er via de publieksomgeving ook geen mogelijkheden gegeven voor het toevoegen van annotaties. De dienst "Annotaties" dient daarnaast deel uit te maken van de licentie.