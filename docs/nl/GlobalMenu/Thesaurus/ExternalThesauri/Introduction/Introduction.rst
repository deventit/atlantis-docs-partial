
Externe Thesauri
----------------

Beheer Externe Thesauri
^^^^^^^^^^^^^^^^^^^^^^^

Externe thesauri definieert de beschikbare externe thesauri. Er kunnen vrij externe thesauri gedefinieerd worden. Hiervoor dienen geschikte koppelingen beschikbaar te zijn.

**Externe thesauri**

Het scherm voor de externe thesauri presenteert een paneel aan de linkerzijde. Dit geeft een overzicht van de beschikbare externe thesauri. 

**Filteren**

De lijst met externe thesauri kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. 

**Toevoegen** 

Met het **toevoegen** pictogram boven de lijst met externe thesauri wordt een nieuwe externe thesaurus toegevoegd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de externe thesaurus wordt weergegeven voor invoer.

**Wijzigen** 

Met het **potlood** pictogram naast de naam van een externe thesaurus wordt een externe thesaurus gewijzigd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de externe thesaurus wordt weergegeven voor mutatie.

**Verwijderen** 

Met het **prullenbak** pictogram wordt een externe thesaurus verwijderd. Dit kan alleen als de externe thesaurus niet in gebruik is.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__