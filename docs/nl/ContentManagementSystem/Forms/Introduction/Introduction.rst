
Formulieren
~~~~~~~~~~~

Beheer Formulieren
------------------

Aan metadata modellen worden formulieren gekoppeld voor invoer en mutatie. Welke formulieren voor een metadata model beschikbaar zijn wordt in een extra paneel aan de rechterzijde gepresenteerd. Het linker paneel klapt daarbij automatisch in. Deze lijst kan  gefilterd worden door in het "Zoek hier" veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

Er is een onderscheid tussen formulieren die gemaakt zijn met de Schermbouwer en formulieren die daar niet mee gemaakt zijn. Alleen formulieren die met de Schermbouwer zijn gemaakt kunnen ook aangepast worden. Nieuwe formulieren zijn altijd met de Schermbouwer samengesteld. Om formulieren met de Schermbouwer samen te stellen of te wijzigen dient de Schermbouwer onderdeel uit te maken van de licentie.

**Filteren**

De lijst van formulieren kan gefilterd worden door in het "Zoek hier" veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen van een formulier**

Een formulier wordt toegevoegd door in het paneel met de formulieren op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een extra paneel aan de rechterkant waarin de gegevens van het formulier ingevoerd wordt. 

**Linken van formulieren**

Met het **link** pictogram worden formulieren die bij een ander metadata model gedefinieerd zijn gelinkt aan dit metadata model. Dit kan alleen voor formulieren van metadata modellen die dezelfde hoofdtabel hebben.

**Formulieren loskoppelen**

Formulieren die gelinkt zijn, presenteren een **formulier ontkoppelen** pictogram. Hiermee wordt een gelinkt formulier ontkoppeld van dit metadata model.

**Muteren**

Met het **potlood** pictogram wordt een extra venster aan de rechterkant geopend met daarin de gegevens van het formulier. Als dit een Schermbouwer formulier betreft wordt dan ook de lay-out van het formulier getoond met de mogelijkheden om deze aan te passen. Voor een nieuw formulier dient deze eerst als een nieuw formulier aangemaakt te worden en vervolgens kan deze gemuteerd worden om bij de Schermbouwer te komen.

**Verwijderen**

Formulieren worden verwijderd met het **prullenmand** pictogram bij het formulier. Na bevestiging wordt het formulier verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Formulieren <../Concepts/Concepts.html#formulieren>`__