
Invoer Transformaties
^^^^^^^^^^^^^^^^^^^^^

Voor het wijzigen van een transformatie zijn in dit paneel de volgende velden beschikbaar:

-  **Naam**: Naam van de transformatie.
-  **Pad**: Het pad van relaties die doorlopen moet worden om de transformatie uit te voeren. Dit is een SQL query die de objecten van het "Van" metadata model transformeert naar objecten van het "Naar" metadata model.
-  **Omgekeerd pad**: Het pad van relaties die doorlopen moet worden om de transformatie uit te voeren. Dit is een SQL query die de objecten van het "Naar" metadata model transformeert naar objecten van het "Van" metadata model.
-  **Van**: Selectie van het metadata model waarvan de objecten getransformeerd moeten worden.
-  **Naar**: Selectie van het metadata model waar de objecten naartoe getransformeerd moeten worden.
-  **Standaard**: Vlag die aangeeft dat deze transformatie standaard uitgevoerd moet worden bij een zoekvraag op objecten van het "Van" metadata model.
-  **Via externe index**: Vlag die aangeeft dat een externe index wordt toegepast om de SQL query voor de transformatie uit te voeren.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Metadata modellen <../Concepts/Concepts.html#metadata-modellen>`__, `Relaties <../Concepts/Concepts.html#relaties>`__