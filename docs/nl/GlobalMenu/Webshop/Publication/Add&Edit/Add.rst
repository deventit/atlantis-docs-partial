
Invoer Publicatiesoorten
^^^^^^^^^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Omschrijving**: Omschrijving van de publicatiesoort.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__