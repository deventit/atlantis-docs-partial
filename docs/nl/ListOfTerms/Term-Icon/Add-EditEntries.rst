
Invoeren keuze-opties
~~~~~~~~~~~~~~~~~~~~~

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Keuze toevoegen en bevestigen**: Hier wordt de keuze-optie ingevoerd en bevestigd met <Enter>. Als een keuze-optie wordt aangeklikt uit de onderstaande lijst van termen, dan wordt de term in dit veld gepresenteerd voor wijziging. 
-  **Standaard**: Geef aan of deze keuze-optie standaard wordt geselecteerd voor het veld.
-  **Termen**: Dit is de termenlijst. Met de **x** knop in de term wordt deze verwijderd uit de keuzelijst. Door op de term te klikken wordt de term in het veld **Keuze toevoegen en bevestigen** geladen voor wijziging.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__