
Harvest bronnen
---------------

Beheer Harvest bronnen
^^^^^^^^^^^^^^^^^^^^^^

Harvest bronnen definieert de harversting van externe bronnen via OAI-PMH. Daarnaast wordt van deze bronnen inzicht gegeven in de harvesting sessie die hebben plaatsgevonden met de fouten die daarbij opgetreden zijn.

**Harvest bronnen**

Het scherm voor de harvest bronnen presenteert een paneel aan de linkerzijde. Dit geeft een overzicht van de beschikbare harvest bronnen. 

**Filteren**

De lijst met harvest bronnen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. 

**Toevoegen** 

Met het **toevoegen** pictogram boven de lijst met harvest bronnen wordt een nieuwe harvest bron toegevoegd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de harvest bron wordt weergegeven voor invoer.

**Actief indicator**

Hardvest bronnen die actief zijn worden aangegeven met een vinkje.

**Sessies** 

Met het **bekijk sessie** pictogram naast de naam van een harvest bron worden de sessies gepresenteerd met daarbij opgetreden fouten. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de sessies wordt weergegeven voor raadpleging.

**Wijzigen** 

Met het **potlood** pictogram naast de naam van een harvest bron wordt een harvest bron gewijzigd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de harvest bron wordt weergegeven voor mutatie.

**Verwijderen** 

Met het **prullenbak** pictogram wordt een harvest bron verwijderd. Dit kan alleen als de harvest bron niet in gebruik is of is geweest.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `OAI-PMH <../Concepts/Concepts.html#oai-pmh>`__