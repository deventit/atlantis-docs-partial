
Dagdelen
--------

Dagdelen geven de keuzen voor de bezoeker in de tijdseenheden waarin aangevraagd kan worden voor inzage op de studiezaal. 

Voor het definiëren wordt een paneel aan de rechterzijde geopend met een tabel van dagdelen. Een dagdeel wordt gedefinieerd via de volgende velden:

-  **Naam**: Naam van het dagdeel
-  **Start tijd**: Start tijd in hh:mm
-  **Eind tijd**: End tijd in hh:mm
-  **Gehele dag**: Geeft aan of het dagdeel de gehele dag beslaat.

**Filteren**

De lijst van dagdelen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een dagdeel wordt toegevoegd door in de tabel op het **+** pictogram (nieuwe toevoegen) te klikken. Hiermee komt een nieuwe regel beschikbaar waarin de gegevens van het dagdeel ingevoerd worden. Met het **potlood** pictogram komt de regel beschikbaar waarin de gegevens van het dagdeel gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt het dagdeel, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__
