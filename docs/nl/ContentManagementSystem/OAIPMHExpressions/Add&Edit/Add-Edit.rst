
Invoer OAI-PMH expressies
--------------------------

Voor het wijzigen van een expressie voor OAI-PMH zijn in dit paneel de volgende velden beschikbaar:

-  **Prefix**: Selectie van de prefix voor de OAI-PMH expressie.
-  **Records uitsluiten zonder multimedia**: Vlag die aangeeft of records zonder gekoppelde bestanden geharvest worden.
-  **Neem niet-gemodereerde records op**: Vlag die aangeeft of niet gemodereerde objecten geharvest worden.

Als de muis over een expressie wordt bewogen, dan geeft dit een drietal menu-opties: ListIdentifiers, ListRecords en GdtRecord. Het aanklikken van een optie geeft de mogelijkheid een XSLT expressie in te voeren.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `OAI-PMH <../Concepts/Concepts.html#oai-pmh>`__