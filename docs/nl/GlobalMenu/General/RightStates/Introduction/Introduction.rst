
Rechten statussen
------------------

Beheer rechten statussen
^^^^^^^^^^^^^^^^^^^^^^^^

Rechten statussen worden gebruikt om segmenten toe te voegen bij een bepaalde status van een object van een metadata model. Met deze menu-optie wordt een lijst van rechten statussen onderhouden.
Met rechten statussen is het mogelijk om bij een status van een object, door het toevoegen van segmenten, de rechten op dat object aan te passen voor zolang de status geldt. 

Welke rechten statussen van toepassing zijn voor welk metadata model wordt ingesteld bij de metadata modellen.

**Rechten statussen**

Het scherm voor de rechten statussen presenteert een paneel aan de linkerzijde. Dit geeft een overzicht van de beschikbare rechten statussen. 

**Filteren**

De lijst met rechten statussen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. 

**Toevoegen** 

Met het **toevoegen** pictogram boven de lijst met rechten statussen wordt een nieuwe rechten status toegevoegd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de rechten status wordt weergegeven voor invoer.

**Wijzigen** 

Met het **potlood** pictogram naast de naam van een rechten status wordt een rechten status gewijzigd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de rechten status wordt weergegeven voor mutatie.

**Verwijderen** 

Met het **prullenbak** pictogram wordt een rechten status verwijderd. Dit kan alleen als de rechten status niet in gebruik is.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Autorisatie <../Concepts/Concepts.html#autorisatie>`__