
Volumes
-------

In dit paneel worden de Volumes gedefinieerd. 

Volumes zijn de opslaglocaties voor (multimediale) bestanden die gekoppeld worden met de objecten. Op de relatie met digitale bestanden kan een volume worden aangegeven. Is dit niet aangegeven, dan wordt het standaard volume gebruikt.

Voor annotaties en voor het encoderen van streaming audio en video worden ook volumes aangegeven.

Een volume bestaat uit twee paden: één voor het lezen en één voor het schrijven. Atlantis ondersteunt de volgende soorten volumes:

- een lokale map op de server
- een netwerkmap
- een ftp-adres
- een http-adres (alleen voor lezen)

**Filteren**

De lijst van Volumes kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een Volume wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel waarin de gegevens van het volume ingevoerd worden. Met het **potlood** pictogram komt de regel beschikbaar waarin de gegevens van het volume gewijzigd worden.

De volgende velden zijn beschikbaar:

-  **Naam**: Naam van het volume.
-  **Locatie om te lezen**: De bestandslocatie voor het lezen van bestanden.
-  **Locatie om te schrijven**: De bestandslocatie voor het schrijven van bestanden.
-  **Standaard**: Vlag die aangeeft of het een standaard volume betreft.

**Verwijderen**

Met het **prullenbak** pictogram wordt het volume, na bevestiging, verwijderd. Dit kan alleen als deze niet meer wordt gebruikt.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Invoeren relaties <../ContentManagementSystem/ContentManagementSystem.html#invoer-relatie>`__
