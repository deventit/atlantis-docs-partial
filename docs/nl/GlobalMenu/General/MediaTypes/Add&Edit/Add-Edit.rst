
Invoer Mediatype
^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Naam**: De bestandsextensie.
-  **Expressie voor streaming**: De expressie die de streaming configureert.
-  **Icoon**: Het icoon voor de bestandsextensie
-  **Standaard**: Markeert het icoon als het standaard icoon indien geen ander icoon bepaald kan worden voor de bestandsextensie.
-  **Encoderen**: Vlag of bestandstype geëncodeerd dient te worden.
-  **Video bestand**: Vlag of bestandtype een video bestand betreft.
-  **Audio bestand**: Vlag of bestandstype een audio bestand betreft.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Mediatypes <../Concepts/Concepts.html#mediatypes>`__