
Mediatypes
~~~~~~~~~~

De enige plaats waar het met objecten gekoppelde bestanden worden gepresenteerd is bij de de presentatie van de detailinformatie van een object. Op alle andere plaatsen worden iconen gepresenteerd. Een icoon is dan een representatie van het object of van een, aan het object, gekoppeld bestand. Dde representatie van een object met gekoppelde bestanden is het icoon van het eerste gekoppelde bestand.

Mediatypes definiëren welk icoon wordt gepresenteerd voor een digitaal bestand dat geen afbeelding is op basis van de extensie van het bestand. Voor afbeeldingen wordt een icoon van de afbeelding gepresenteerd. 

Het icoon voor een bestand wordt bepaald volgens de onderstaande regels:

- Is het een afbeelding, dan een icoon van de afbeelding
- Is het geen afbeelding:
    - is het een PDF bestand en maakt de dienst ""digitale informatiebronnen" onderdeel uit van de licentie, dan een icoon van de eerste pagina van het PDF bestand.
    - is er een icoon gedefinieerd voor de bestandsextensie bij de Mediatypes, dan dit icoon gebruiken
    - is er geen icoon gedefinieerd voor de bestandsextensie bij de Mediatypes, dan het icoon gebruiken dat aangemerkt staat als "standaard" bij de Mediatypes.

Als objecten worden gepresenteerd in bomen, dan wordt het met het metadata model gekoppelde icoon weergegeven. Als dit niet beschikbaar is, dan wordt het standaard icoon gebruikt dat aangemerkt staat als "standaard" bij de mediatypes.