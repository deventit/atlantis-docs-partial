
Algemeen
~~~~~~~~

Beheer Annotatietypes
---------------------

**Filteren**

De lijst van Annotatie Types wordt gefilterd door in het "Zoek hier" veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen en wijzigen**

Een Annotatie Type  wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van het Annotatie Type ingevoerd worden. Met het potlood pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van het Annotatie Type gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt een Annotatie Type, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Annotaties <../Concepts/Concepts.html#annotaties>`__
