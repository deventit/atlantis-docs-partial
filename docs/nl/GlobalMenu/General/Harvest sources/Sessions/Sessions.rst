
Sessies
^^^^^^^

In dit paneel worden de harvest sessies gepresenteerd naar start- en eind datum en tijd. Met het **sessiefouten bekijken** pictogram wordt een paneel geopend met daarin de opgetreden fouten voor de desbetreffende sessie.


**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `OAI-PMH <../Concepts/Concepts.html#oai-pmh>`__