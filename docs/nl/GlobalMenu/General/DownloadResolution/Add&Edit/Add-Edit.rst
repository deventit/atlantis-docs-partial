
Invoer Download Resolutie
^^^^^^^^^^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen van een download resolutie zijn in dit paneel de volgende velden beschikbaar:

-  **Beschrijving**: De beschrijving van de download resolutie.
-  **Bestandstype**: Het bestandstype waarin de download wordt afgeleverd. 
-  **DPI**: Het aantal DPI's van de afbeelding bij download.
-  **Kwaliteit**: De kwaliteit van de afbeelding bij download. Dit wordt aangegeven in % voor JPG downloads of een getal van 1-5 voor PNG downloads.
-  **Horizontale resolutie**: Het maximum aantal pixels horizontaal van de download.
-  **Verticale resolutie**: Het maximum aantal pixels verticaal van de download.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Presentatie profielen <../Concepts/Concepts.html#presentatie-profielen>`__