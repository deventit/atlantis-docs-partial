
Systeeminstellingen
*******************

Via de systeeminstellingen worden een aantal instellingen gedaan die voor de gehele applicatie en voor alle gebruikers gelden.

**IP adresranges voor intranet**

Hiermee worden de IP-adressen opgegeven voor het intranet. Als de applicatie via het intranet wordt benaderd, dan heeft dat gevolgen voor bv. auteursrechten. Auteursrechtelijke bescherming geldt dan niet omdat het geen publicatie betreft. Ook kunnen afwijkende instellingen gelden voor de kwaliteit en resoluties waarmee afbeeldingen worden gepresenteerd via de publieksomgeving. Dit wordt ingesteld via Presentatieprofielen voor intranet en internet afzonderlijk.

Met het “**+**” pictogram boven het overzicht wordt een nieuwe adresrange toegevoegd. Met het “**potlood**” pictogram wordt een adresrange geopend voor wijziging. In beide gevallen kan direct in de nieuwe of te wijzigen regel ingevoerd worden. IP-adressen worden ingevoerd als: 4 cijferreeksen, gescheiden door een "\.\" bv. 5.123.12.173. Met het “**prullenbak**” pictogram wordt een adresrange, na bevestiging, verwijderd. De range wordt aangegeven door de **IP begin** en **IP eind** velden. Het aanvinkveld “\ **Uitsluiten**\ ” betekent dat de ingevoerde range uitgesloten is van het intranet.

**Segmenten per IP**

Voor de geselecteerde IP-adresrange wordt de toegang tot de hier aangegeven segmenten beperkt.

**Gebruikersgroepen per IP**

Voor de geselecteerde IP-adresrange wordt de toegang tot de hier aangegeven gebruikersgroepen beperkt.

**Thesaurus instellingen**

Hier wordt de standaardtaal voor de thesauri geselecteerd. Dit betreffen dan de thesauri die in de Atlantis ondergebracht zijn.

**Systeem Instellingen**

Hiermee worden een aantal standaard instellingen gedaan:

-  **Taal:** Selecteer de standaard taal.
-  **Metadata model voor multimedia op annotaties**: Selectie van het metadata model voor multimedia op annotaties.
-  **Volume voor multimedia op annotaties**: Selectie van het volume voor opslag van multimedia op annotaties.
-  **Volume voor encoderen**: Selectie van het volume om op te encoderen.
-  **Maximaal aantal zoekresultaten**: Het maximaal aantal zoekresultaten op een zoekvraag binnen de beheeromgeving.
-  **Maximaal aantal zoekresultaten (publiek)**: Het maximaal aantal zoekresultaten op een zoekvraag binnen de publieksomgeving.
-  **Maximaal aantal resultaten voor automatisch aanvullen**: Het maximaal aantal suggesties voor automatisch aanvullen.
-  **Standaardprijs per scan voor Scanning on Demand**: Prijs (in eurocenten) voor een scan ten behoeve van Scanning on Demand.
-  **Aantal maanden van inactiviteit voor verwijdering gebruiker x1 maand voor het verstrijken wordt een e-mail gestuurd**: Instelling voor aantal maanden voor verwijdering of anonimisering van gebruikers waarop een notificatie aan de gebruiker via e-mail wordt gestuurd.
-  **Aantal maanden om bezoekers/bezoeken te anonimiseren**: Aantal maanden van inactiviteit totdat bezoekers en bezoeken worden geanonimiseerd.
-  **Aantal maanden om bestellingen te anonimiseren**: Aantal maanden van inactiviteit totdat bestellingen worden geanonimiseerd.
-  **Publiceer alleen gemodereerde objecten:** Hiermee worden alleen objecten via de publieksomgeving gepubliceerd als deze gemodereerd zijn.
-  **Publiceer alleen gemodereerde annotaties**: Hiermee worden alleen annotaties via de publieksomgeving gepubliceerd als deze gemodereerd zijn.
-  **Zet op Niet-Gemodereerd na wijziging**: Als een beschrijving wordt bewaard, dan wordt, als dit is aangevinkt, deze automatisch naar niet-gemodereerd teruggezet.
-  **Scanning on Demand toestaan**: Vlag dat aangeeft of Scanning on Demand geactiveerd is. Dit is een alleen-lezen vlag. Aanpassing hiervan in overleg met support van DEVENTit.
-  **Moderatie op stamgegevens**: Hiermee wordt moderatie op stamgegevens mogelijk. Stamgegevens zijn selectie-tabellen zoals trefwoorden, thema’s, etc. Door deze optie aan te vinken komt moderatie hierop beschikbaar. Dit geeft de mogelijkheid om toevoegingen en aanpassingen aan deze stamgegevens te visualiseren, te controleren en, eventueel na wijziging, te modereren.
-  **Converteerunieke identificaties automatisch naar hoofdletters**: Unieke identificaties van beschrijvingen die letters kunnen bevatten worden hiermee automatisch naar hoofdletters omgezet bij bewaren.

**Systeeminstellingen parameters**

In deze sectie worden extra parameters geconfigureerd via de veldcombinaties naam en waarde.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__
