
Invoer expressies ListIdentifiers, ListRecords, GetRecord
---------------------------------------------------------

In dit paneel worden de XSLT expressies gedefinieerd voor de ListIdentifiers, ListRecords of GetRecord aanroepen voor de OAI-PMH toegang van de desbetreffende metadata prefixen.


**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `OAI-PMH <../Concepts/Concepts.html#oai-pmh>`__