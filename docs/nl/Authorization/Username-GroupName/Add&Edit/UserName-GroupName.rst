
Gebruikers
~~~~~~~~~~

Met dit pop-up scherm worden de gegevens van een nieuwe gebruiker
ingevoerd of van een bestaande gebruiker gewijzigd. Dit scherm bevat de
volgende velden:

-  **Gebruikersnaam***: De gebruikersnaam waarmee de gebruiker zich
   aanmeldt binnen de beheer- of publieksomgeving. Dit veld is verplicht
   en uniek.
-  **Titel**: De aanhef van de gebruiker, zoals de heer, mevrouw, etc.
-  **Standaard segment**: Selectie van het standaard segment van de
   gebruiker. Dit segment wordt toegepast op alle objecten dat die
   door de gebruiker worden aangemaakt.
-  **Naam**: De achternaam
-  **Voornaam**: De voornaam
-  **Wachtwoord\***: Het wachtwoord waarmee de gebruiker zich aanmeldt
   binnen de beheer- of publieksomgeving. Bij het invullen van dit veld
   wordt het wachtwoord gewijzigd. Voor nieuwe gebruikers is dit veld
   verplicht.
-  **Wachtwoord bevestigen**: Hier wordt het wachtwoord nogmaals
   ingevoerd ter controle.
-  **Adres**: De straat + huisnummer
-  **Postcode**: De postcode van het adres
-  **Woonplaats**: De woonplaats
-  **Telefoonnummer**: Het telefoonnummer
-  **Land**: Selectie van het land
-  **Systeemaccount**: Extern systeemaccount.
-  **Registratienummer**: Extern registratienummer.
-  **Geslacht**: Geslacht van de gebruiker.
-  **Opmerkingen**: Aanvullende opmerkingen.
-  **Afdeling**: Selectie van de afdeling van de gebruiker.
-  **Organisatie**: Selectie van de organisatie van de afdeling.
-  **2-factor authenicatie methode**: De methode voor 2-factor authenticatie.
-  **Geboortedatum**: Geboortedatum van de gebruiker.

Bij de gebruiker zijn een aantal opties aan te geven:

-  **Actief**: De gebruiker is actief of niet. Een niet-actieve
   gebruiker kan niet meer inloggen.
-  **Op rekening**: De gebruiker kan op rekening bestellingen plaatsen.
-  **Niet geregistreerd**: Als dit veld is aangevinkt, dan is dit het
   gebruikersaccount voor de anonieme gebruiker. Een anonieme gebruiker
   maakt gebruik van de publieksomgeving zonder in te loggen. Waar deze
   gebruiker dan toegang toe heeft wordt bepaald door de
   gebruikersgroepen waar deze gebruiker aan toegekend is.
-  **Stuur login gegevens**: Bij bewaren worden de login gegevens aan de
   betreffende gebruiker gemaild op het e-mailadres zoals die in dit
   scherm is ingevoerd.

Het laatste onderdeel betreft de segment en groepstoewijzing van de
gebruiker. Hiermee wordt uitgedrukt dat een gebruiker onderdeel uitmaakt
van één of meerdere groepen, eventueel voor de aangegeven segmenten per
groep. Als geen segmenten worden aangegeven bij een groep, dan maakt een
gebruiker deel uit van de groep voor alle segmenten. Dit werkt als
volgt:

-  Met de “\ **+**\ ” knop wordt een nieuwe regel aan deze tabel
   toegevoegd.
-  Met het “\ **potlood**\ ” komt de desbetreffende regel beschikbaar
   voor wijziging.
-  Bij **Segment** worden de segmenten geselecteerd uit de lijst van
   beschikbare segmenten. Er kunnen hier meerdere segmenten worden
   geselecteerd.
-  Bij **Groep** wordt een groep geselecteerd.
-  Met “\ **Opslaan**\ ” wordt de invoer bewaard. Met **Afsluiten**
   wordt de invoer niet bewaard.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Autorisatie <../Concepts/Concepts.html#autorisatie>`__
