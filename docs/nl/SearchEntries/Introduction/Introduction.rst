
Zoekingangen
************

Zoekingangen worden gedefinieerd om daarmee het zoeken op objecten van metadata modellen mogelijk te maken. Zoekingangen gelden voor de beheer- of publieksomgeving of voor beide. Aan welke omgeving een zoekingang  beschikbaar wordt gesteld, wordt weergeven via de kleur van de zoekingang:

-  **Oranje**: Zoekingang is voor de beheeromgeving.
-  **Groen**: Zoekingang is voor de publieksomgeving.
-  **Blauw**: Zoekingang is voor beide omgevingen.

Zoekingangen zijn hiërarchisch van opbouw, een zoekingang kan ondergebracht zijn onder een andere zoekingang. Dit vertaalt zich naar een boomstructuur. In de beheer- en publieksomgevingen worden de zoekingangen dan ook hiërarchisch beschikbaar.

Aan zoekingangen worden metadata modellen toegewezen. Hiermee wordt aangegeven dat de zoekingang de objecten van die metadata modellen doorzoekt. De toegewezen metadata modellen zijn ook in de boomstructuur opgenomen en worden met de kleur **Zwart** weergegeven.

**Hiërarchische indeling**

De hiërarchische indeling van de zoekingangen wordt gepresenteerd via een boomstructuur van het categorie schema. De boom bevat knopen en subknopen. Iedere knoop of subknoop is een zoekingang of metadata model. Met de **+** of **-** voor een knoop wordt deze uit- of ingeklapt. 

**Filteren**

De boom wordt gefilterd door in het "Zoek hier" veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen op het hoogste niveau**

Zoekingangen worden op het hoogste niveau toegevoegd door op het **+** pictogram te klikken bovenaan het scherm. Dit opent een extra paneel aan de rechterkant voor de invoer van de gegevens.

**Toevoegen zoekingangen op een onderliggend niveau**

Zoekingangen worden op een onderliggend niveau toegevoegd door met de rechtermuisknop op een zoekingang te klikken en in het context menu dat dan verschijnt te kiezen voor **Nieuwe zoekingang**. Dit opent een extra paneel aan de rechterkant voor de invoer van de gegevens. 

**Toewijzen van metadata modellen aan een zoekingang**

Metadata modellen worden aan een zoekingang toegewezen door met de rechtermuisknop op een zoekingang te klikken en in het context menu dat dan verschijnt te kiezen voor **Metadata model**. Dit opent een extra paneel aan de rechterkant voor de selectie van de metadata modellen. Hier kunnen metadata modellen ook gewijzigd en verwijderd worden.

**Bewerken zoekingang**

Een zoekingang wordt bewerkt door met de rechtermuisknop op een zoekingang te klikken en in het context menu dat dan verschijnt te kiezen voor **Bewerken**. Dit opent een extra paneel aan de rechterkant voor de invoer van de gegevens.

**Verwijderen zoekingang**

Zoekingangen  worden verwijderd door deze te selecteren. Het selecteren van zoekingangen gebeurt door deze aan te klikken. Door de <SHIFT> of <CRTL> toets ingedrukt te houden bij het selecteren worden meerdere zoekingangen geselecteerd (werkt niet op alle browsers/besturingssystemen). Vervolgens wordt er verwijderd op één van de onderstaande manieren:

- Door op het **prullenmand** pictogram te klikken worden de geselecteerde zoekingangen, na bevestiging, verwijderd.
- Door met de rechtermuisknop op een zoekingang te klikken en in het context menu dat dan verschijnt te kiezen voor **Verwijderen**, wordt na bevestiging en voor de zoekingang waarvoor het context menu is opgeroepen verwijderd.

**Verplaatsen**

Zoekingangen worden verplaatst door deze te verslepen of te knippen en te plakken.

-  **Verslepen**: Met de muis wordt een zoekingang versleept **voor**, **op** of **na** de doel-zoekingang. Als versleept wordt **op** de doel-zoekingang wordt de geselecteerde zoekingang onderliggend aan de doel-zoekingang geplaatst.
-  **Knippen/plakken**: Met de rechtermuistoets wordt voor de zoekingang het context menu opgeroepen en gekozen voor **Knippen**. Vervolgens wordt op de doel-zoekingang met de rechtermuistoets het context menu opgeroepen en gekozen voor **Plakken**. De geplakte zoekingang wordt dan onderliggend aan de doel-zoekingang geplaatst.

Bij het verplaatsen worden de onderliggende Zoekingangen mee verplaatst.

**Kopiëren**

Zoekingangen worden gekopieerd met de rechtermuistoets voor de zoekingang het context menu op te roepen en te kiezen voor **Kopieer lijst**. Vervolgens wordt op de doel-zoekingang met de rechtermuistoets het context menu opgeroepen en gekozen voor "Plakken". De gekopieerde zoekingang wordt dan onderliggend aan de doel-zoekingang geplaatst.

Bij het kopiëren worden de onderliggende zoekingangen mee verplaatst.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`
