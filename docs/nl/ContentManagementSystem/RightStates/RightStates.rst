
Rechten statussen
~~~~~~~~~~~~~~~~~

Op een metadata model worden rechten statussen toegekend. Hiermee worden segment mutaties doorgevoerd op metadata model objecten wanneer deze in de desbetreffende status komen if deze status verlaten.

**Overzicht van rechten statussen**

In dit paneel wordt een overzicht gepresenteerd van de toegekende rechten statussen voor het metadata model. 

**Filteren**

De lijst kan gefilterd worden via het **Zoek hier** veld boven het overzicht. Een ingevoerd filter wordt direct toegepast. 

**Toevoegen en wijzigen**

Met het **+** pictogram boven het overzicht wordt een nieuwe rechten status toegevoegd. Met het **potlood** pictogram wordt een rechten status geopend voor wijziging. In beide gevallen wordt de invoer of mutatie direct in de regel ingevoerd.

**Verwijderen**

Met het **prullenbak** pictogram wordt een rechten status, na bevestiging, verwijderd.


**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Rechten statussen <../GlobalMenu/GlobalMenu.html#rechten-statussen>`__


