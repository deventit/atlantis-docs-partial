
Linken formulieren
------------------

Met dit paneel worden formulieren van andere metdata modellen (met dezelfde hoofdtabel) gelinkt aan dit metadata model.

Gepresenteerd wordt een paneel met de mogelijk te linken formulieren.

**Filteren**

De lijst van formulieren kan gefilterd worden door in het "Zoek hier" veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Linken van formulieren**

Met het **link** pictogram bij een formulier wordt het formulier gelinkt aan het metadata model.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Formulieren <../Concepts/Concepts.html#formulieren>`__


