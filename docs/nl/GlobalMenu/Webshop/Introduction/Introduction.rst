
Webwinkel
~~~~~~~~~

De webshop biedt een winkelwagen aan de bezoekers voor het plaatsen van bestellingen voor reproductie, het aanvragen/reserveren van stukken voor inzage op de studiezaal en aanvragen voor Scanning on Demand.

De licentiesamenstelling bepaalt of de webshop beschikbaar is.

Voor de webshop wordt geconfigureerd:

-  **Administraties**: Via administraties worden verschillende winkelwagens gedefinieerd, met hun eigen instellingen. Voor een administratie kan gedefinieerd worden:

    - Afzenders, onderwerpen en ontvangers van bestellingen en aanvragen.
    - Maximale aantallen voor de diverse bestellingen en aanvragen.
    - Vakanties: De dagen dat de studiezaal gesloten is.
    - Open dagen: De dagen van de week dat de studiezaal doorgaans geopend is.
    - Dagdelen: De dagdelen waaruit bij een aanvraag voor inzage geselecteerd kan worden.
    - Standaard supplementen: Standaard toeslagen per besteld artikel of per bestelling.
    - Verzendmethoden: De beschikbare verzendmethoden.
    - Factuurnummer: Start factuurnummer vanaf een opgegeven datum.

    Een administratie wordt doorgaans ingezet per organisatie zodat de bovenstaande instellingen per organisatie gedefinieerd worden. De onderstaande configuraties worden op metadata model niveau gedefinieerd, waarmee eveneens een splitsing in organisaties plaatsvindt. Een organisatie binnen de applicatie heeft immers doorgaans haar eigen metadata modellen.

-  **Kortingen**: Kortingen op bestellingen via staffels in prijzen of percentages.
-  **Reproductiesoorten**: De soorten van reproducties de aangeboden worden voor bestellingen.
-  **Publicatiesoorten**: De soorten van publicaties die aangeboden worden voor bestellingen.

De webshop kan uitgebreid worden met de functies voor **Elektronisch betalen**  en direct **Direct downloaden**.

**Elektronisch betalen**

Met Elektronisch betalen kan een bestelling direct afgerekend worden. Hiervoor dient de organisatie een abonnement met Ingenico (https://ingenico.nl/epayments) af te sluiten en de gegevens daarvoor aan DEVENTit door te geven.

**Direct downloaden**

Met direct downloaden krijgt de bezoeker in de e-mail van de bestelling tevens de download links meegestuurd om het bestelde direct te downloaden.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__
