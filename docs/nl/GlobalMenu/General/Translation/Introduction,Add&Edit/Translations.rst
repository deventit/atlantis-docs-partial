
Vertalingen
------------

In dit paneel worden vertalingen gedefinieerd. Voor ieder schermelement wordt hier de te presenteren tekst in diverse talen ingevoerd.

**Filteren**

De lijst van vertalingen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Paginering**

De tabel van vertalingen is gepagineerd. Onderaan de tabel worden de **paginanummers** weergegeven. Hierop kan door te klikken direct deze pagina opgeroepen worden. Met de **>** en **<** pictogrammen wordt naar de volgende of vorige pagina gegaan. Met de **|<**en **>|** pictogrammen wordt naar de eerste en laatste pagina gegaan.

**Toevoegen en wijzigen**

Een vertaling wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de tabel waar de gegevens van de vertaling ingevoerd worden. Met het **potlood** pictogram komt de regel voor wijziging beschikbaar. 

De volgende velden zijn beschikbaar:

-  **Sleutel**: De sleutel van het schermelement, waarmee deze uniek geïdentificeerd is.
-  **Taal**: Per taal is een kolom beschikbaar in de vertaaltabel. Hier wordt de tekst van het schermelement voor de desbetreffende taal ingevoerd.

**Verwijderen**

Met het **prullenbak** pictogram wordt de vertaling, na bevestiging, verwijderd. Dit kan alleen als deze niet meer wordt gebruikt bij een **Relatie**.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__