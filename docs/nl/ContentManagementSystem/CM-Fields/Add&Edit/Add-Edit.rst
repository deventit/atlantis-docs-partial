
Invoer Content Management Veld 
------------------------------

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Naam**: De naam van het veld. Dit tekstveld is verplicht.
-  **Label**: De tekst waarmee het veld wordt weergegeven.
-  **Volgnummer**: het volgnummer van het veld in het zoekformulier, presentatie zoekresultaten en filters.
-  **Gewicht**: Het gewicht van dit veld in het zoekresultaat ten behoeve van de relevantie.
-  **Parameters**: Extra parameters.
-  **Groepsnaam**: Groepsnaam voor groepering van filtervelden.
-  **Functie**: Selectie van een functie voor sortering.
-  **Bovenliggend filter**: Selectie van een bovenliggend filter.
-  **Tabel.kolom**: Selectie van een tabel en kolom.
-  **Glossariumbsd**: Selectie van een metadata model voor het glossarium.
-  **Directe velden**: Wordt alleen gebruikt door DEVENTit. Wordt verder niet besproken. Dit alleen aanpassen in overleg met support van DEVENTit.
-  **Sorteerveld**: Markeert veld als te gebruiken voor sorteringen.
-  **Zoekveld**: Markeert veld als te gebruiken voor zoeken.
-  **Globaal**: Markeert veld als te gebruiken voor globaal (eenvoudig) zoeken.
-  **Publiek**: Markeert veld als te gebruiken voor de publieksomgeving.
-  **Beheer**: Markeert veld als te gebruiken voor de beheeromgeving.
-  **Aanvullende metagegevens indexeren**: Markeert veld als aanvullende metagegevens te indexeren.
-  **Resultaatveld**: Markeert veld als te gebruiken als resultaatkolom.
-  **Icoonveld**: Markeert veld als veld waarmee een icoon gepresenteerd wordt.
-  **Locatieveld**: Markeert veld als waarde voor locatiebepaling.
-  **Contextveld**: Markeert veld als presentatie van de context van het object.
-  **Sorteer filter op naam**: Geeft aan dat dit filterveld de waarden dient te sorteren op naam in plaats van het aantal zoekresultaten.
-  **Datum**:  Markeert veld als datumveld.
-  **Nummer**:  Markeert veld als numeriek  veld.
-  **Tekst**:  Markeert veld als tekstveld.
-  **Volledige tekst**:  Markeert veld als volledig tekstveld
-  **Relevantieveld**:  Markeert veld als veld waar de relevantie in weergegeven wordt. 
-  **Korte beschrijving**: Veld bevat de korte beschrijving van een object. Deze wordt gepresenteerd in boomweergaven.
-  **Van...t/m**: Het veld is een bereik veld. In een zoekformulier wordt dan dit veld (als het gemarkeerd is als Zoekveld) met een van en t/m zoekveld opgenomen.
-  **Standaard alfabetisch**: Bij sortering op dit veld gebeurt dit standaard in alfabetische volgorde. Dit veld dient dan wel gemarkeerd te zijn als Resultaatveld.
-  **Schakel treffermarkering uit**: Geeft aan of in een zoekresultaat op dit veld de treffermarkering uitgeschakeld dient te worden. Dit veld dient dan wel gemarkeerd te zijn als Resultaatveld.
-  **Filterveld**: Markeert veld als te gebruiken als filterveld.
-  **Selectie query**: Selectie van een tabel en kolom voor het presenteren van een selectie op de waarden van deze kolom.
-  **Aflopen**: Vlag die aangeeft of de waarden van de selectie query aflopend gesorteerd dienen te worden. Als deze niet is aangevinkt dan wordt oplopend gesorteerd.
-  **Help tekst**: Een helptekst voor dit content management veld.


**Zoekoperatoren**

De beheeromgeving kent "Uitgebreid zoeken". Hiermee kunnen zoekvelden en zoekoperatoren geselecteerd worden. De zoekoperatoren kunnen onderverdeel worden in 2 groepen:
-  **Relationeel**: dit betreffen operatoren als >, <, =, etc.
-  **Volledig tekst**: dit betreffen operatoren als bevat, bevat niet, begint met, eindigt op, etc.

Welke operatoren er beschikbaar zijn is afhankelijk van of het veld als datum, numeriek, tekst of volledige tekst is gemarkeerd. Bij volledige tekst zijn de volledige tekst operatoren beschikbaar. Bij de overige markeringen de relationele operatoren. Om beide groepen van operatoren beschikbaar te krijgen moet het veld zowel als volledige tekst als minimaal één van de andere markeringen krijgen.

**Combinaties van metadata modellen**

Bij het zoeken kan een combinatie van metadata modellen gemaakt worden. Daarmee worden voor de Zoek-, Resultaat-, Sorteer- en Filtervelden ook combinaties gemaakt als volgt:
-  **Zoekvelden**: De Zoekvelden met dezelfde naam bij alle geselecteerde metadata modellen.
-  **Resultaatvelden**: De gecombineerde Resultaatvelden van alle geselecteerde metadata modellen.
-  **Sorteervelden**: De gecombineerde Sorteervelden van alle geselecteerde metadata modellen.
-  **Filtervelden**: De gecombineerde Filtervelden van alle geselecteerde metadata modellen.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Content Management Velden <../Concepts/Concepts.html#content-management-velden>`__


