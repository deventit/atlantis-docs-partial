
Opendata
--------

Beheer Opendata bronnen
^^^^^^^^^^^^^^^^^^^^^^^

In dit paneel worden de Open data definities weergegeven.

**Filteren**

De lijst van Open data kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een Open data definitie wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van de Open data definitie ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de Open data definitie gewijzigd worden.

**definitie van uitvoer formaten**

Een Open data definitie kan de objecten in bepaalde uitvoerformaten aanbieden. Met het **S** pictogram bij een definitie wordt een nieuw paneel geopend aan de rechterzijde waarin de uitvoerformaten gedefinieerd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de Open data definitie, na bevestiging, verwijderd. 

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__