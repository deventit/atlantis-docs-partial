
Gebruikersgroepen
~~~~~~~~~~~~~~~~~

Met dit pop-up scherm worden de gegevens van een nieuwe groep ingevoerd of van een bestaande groep gewijzigd. Dit scherm bevat:

-  **Naam**: De naam van de groep
-  **Omschrijving**: De omschrijving van de groep
-  **Nieuwe gebruikers**: Vlag of nieuw geregistreerde gebruikers in deze groep worden geplaatst.
-  **Kosten voor aanvragen mogelijk**: Vlag of kosten van toepassing kunenn zijn bij aanvragen voor inzage of reserveringen.
-  **Veiligheidsniveau**: Het veiligheidsniveau voor leden van deze groep.

De nieuw geregistreerde gebruikers zijn gebruikers die zichzelf via de beheer- of publieksomgeving geregistreerd hebben. Deze worden dan altijd in de groep geplaatst die het veld **Nieuwe gebruikers** aangevinkt heeft staan. Het is aan de beheerder om deze gebruikers in de juiste groepen te plaatsen. De toewijzing van groepen aan gebruikers gebeurt via de functie voor het toevoegen of wijzigen van gebruikers.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Autorisatie <../Authorization/Authorization.html#Authorization>`__
