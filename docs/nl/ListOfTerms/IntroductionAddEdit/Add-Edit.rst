
Keuzelijsten
************

Keuzelijsten worden samengesteld op velden van metadata modellen. Als
een Keuzelijst aan een veld van een metadata model gekoppeld is, dan
kont deze automatisch in formulieren voor invoer en mutatie van objecten
van de metadata modellen beschikbaar.

**Filteren**

De lijst wordt gefilterd door in het “Zoek hier” veld de filterwaarde in
te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen of wijzigen**

Voor het toevoegen of wijzigen is een tabel beschikbaar met de volgende
velden:

-  **Tabelnaam. Kolomnaam**: Het veld van het metadata model. Selecteer
   de kolom uit de lijst van tabellen. De lijst van tabellen wordt
   gefilterd door een filterwaarde in te voeren boven de tabel. Er wordt
   steeds een groep van tabellen ingeladen en gepresenteerd met de knop
   **Laad meer gegevens**. Met de “\ **+**\ ” knop voor een tabel wordt
   deze uitgeklapt voor presentatie van de kolommen. Hier kan een kolom
   geselecteerd worden door de ze aan te klikken.
-  **Vrije invoer toegestaan**: Als dit veld is aangevinkt dan mag er in
   het veld een waarde worden ingevoerd die niet in de keuzelijst
   voorkomt.
-  **Uitbreidbaar**: Als dit veld is aangevinkt dan mogen er nieuwe
   termen aan de keuzelijst toegevoegd worden. Dit zal ervoor zorgen dat
   wanneer een niet-bestaande term in het veld wordt ingevoerd, deze aan
   de keuzelijst wordt toegevoegd.
-  **Metadata model**: Selecteer hier het metadata model waarvoor deze
   keuzelijst wordt opgeroepen in het formulier.
-  **Termen**: Met het “\ **T**\ ” pictogram wordt een paneel aan de
   rechterzijde geopend voor het definiëren van de termen van de
   keuzelijst.

**Filteren**

De lijst wordt gefilterd door in het “Zoek hier” veld de filterwaarde in
te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen of wijzigen**

Voor het toevoegen of wijzigen is een tabel beschikbaar met de volgende
velden:

-  **Tabelnaam. Kolomnaam**: Het veld van het metadata model. Selecteer
   de kolom uit de lijst van tabellen. De lijst van tabellen wordt
   gefilterd door een filterwaarde in te voeren boven de tabel. Er wordt
   steeds een groep van tabellen ingeladen en gepresenteerd met de knop
   **Laad meer gegevens**. Met de “\ **+**\ ” knop voor een tabel wordt
   deze uitgeklapt voor presentatie van de kolommen. Hier kan een kolom
   geselecteerd worden door de ze aan te klikken.
-  **Vrije invoer toegestaan**: Als dit veld is aangevinkt dan mag er in
   het veld een waarde worden ingevoerd die niet in de keuzelijst
   voorkomt.
-  **Uitbreidbaar**: Als dit veld is aangevinkt dan mogen er nieuwe
   termen aan de keuzelijst toegevoegd worden. Dit zal ervoor zorgen dat
   wanneer een niet-bestaande term in het veld wordt ingevoerd, deze aan
   de keuzelijst wordt toegevoegd.
-  **Metadata model**: Selecteer hier het metadata model waarvoor deze
   keuzelijst wordt opgeroepen in het formulier.
-  **Termen**: Met het “\ **T**\ ” pictogram wordt een paneel aan de
   rechterzijde geopend voor het definiëren van de termen van de
   keuzelijst.

**Zie ook:** `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__
