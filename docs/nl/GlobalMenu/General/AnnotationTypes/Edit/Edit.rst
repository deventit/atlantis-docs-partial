
Invoer Annotatietype
^^^^^^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen van een Annotatie Type zijn in dit paneel de volgende velden beschikbaar:

-  **Beschrijving**: De beschrijving van het Annotatie Type
-  **Icoon**: Icoon waarmee het Annotatie Type wordt gepresenteerd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Annotaties <../Concepts/Concepts.html#annotaties>`__
