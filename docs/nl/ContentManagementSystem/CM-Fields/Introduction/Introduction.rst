
Content Management Velden
~~~~~~~~~~~~~~~~~~~~~~~~~

Beheer Content Management Velden
--------------------------------

Op een metadata model worden Content Management Velden geconfigureerd. Dit zijn velden waarvan de waarden berekend worden bij het bewaren van een object of bij het her-indexeren. 

**Overzicht van Content Management Velden**

In het rechterpaneel wordt een overzicht gepresenteerd van de gedefinieerde Content Management Velden voor het geselecteerde metadata model. In deze tabel wordt met vinkjes aangegeven voor welke doelen de Content Management Velden gedefinieerd zijn. In dit overzicht zijn de belangrijkste doelen opgenomen. Deze kolommen zijn te sorteren in aflopende of oplopende volgorde via de knopjes achter de naam van de kolom.

Het volgnummer kolom is daarbij van belang, omdat deze de volgorde van de velden aangeeft. Voor bv. zoekvelden geeft dit de volgorde van de velden in het zoekformulier weer. Dit geldt ook voor Resultaat-, Filter- en Sorteervelden. Volgnummers mogen meerdere keren voorkomen. De volgorde van gelijk genummerde velden is dan onbepaald. 

**Exporteren**

Boven het overzicht van de content management velden is een knopje voor exporteren beschikbaar. Hiermee wordt de definitie van de content management velden geëxporteerd naar een XML bestand. Dit betand kan worden aangepast in een tekstverwerker kan dan weer worden geïmporteerd naar een andere Atlantis omgeving, of onder aanpassing van de naam van het metadata model, geïmporteerd worden in dezelfde Atlantis omgeving.

**Importeren**

Boven het overzicht van de content management velden is een knopje voor importeren beschikbaar. Hiermee wordt de definitie van de content management velden voor een nieuw metadata model geïmporteerd. Het importbestand wordt verkregen via de functie voor het exporteren. 

**Pas wijzigingen toe**

Boven het overzicht van de content management velden is een knopje voor het toepassen van wijzigingen beschikbaar. Hiermee worden alle gemaakte wijzigingen doorgevoerd.

**Voeg globaal zoekveld toe**

Boven het overzicht van de content management velden is een knopje voor het toevoegen van een standaard globaal zoekveld. Dit zoekveld verzameld alle metadata velden van een object voor globaal zoeken.

**Voeg standaard icoonveld toe**

Boven het overzicht van de content management velden is een knopje voor het toevoegen van een standaard icoonveld als resultaatveld voor de beheer- en publieksomgeving.

**Voeg standaard context voor beheer toe**

Boven het overzicht van de content management velden is een knopje voor het toevoegen van een standaard context voor de beheeromgeving als resultaatveld. Hiermee worden de hiërarchisch bovengelegen objecten van een object als context van het object gepresenteerd. Dit presenteert zich als een icoon dat, wanneer daarop geklikt wordt, de context zichtbaar wordt gemaakt waarbij op ieder object in de context geklikt kan worden om daarnaar toe te navigeren.

**Voeg standaard context voor publiek toe**

Boven het overzicht van de content management velden is een knopje voor het toevoegen van een standaard context voor de publieksomgeving als resultaatveld. Hiermee worden de hiërarchisch bovengelegen objecten van een object als context van het object gepresenteerd. Dit presenteert zich als een overzicht waarbij op ieder object in de context geklikt kan worden om daarnaar toe te navigeren.

**Kopieer meerdere CM-Velden**

Boven het overzicht van de content management velden is een knopje voor het kopiëren van meerdere CM-velden tegelijkertijd naar een ander metadata model met dezelfde toplevel tabel.  

**Filteren**

De lijst kan gefilterd worden via de **filter** knop boven het overzicht. Doelen kunnen hiermee aan- of uitgevinkt worden waarmee de lijst hierop gefilterd wordt. 

**Toevoegen en wijzigen**

Met het **+** pictogram boven het overzicht wordt een nieuw CM-veld toegevoegd. Met het **potlood** pictogram wordt een CM-veld geopend voor wijziging. In beide gevallen wordt een paneel aan de rechterzijde geopend en klapt dit paneel automatisch dicht.

**Kopiëren**

In de Kopiëren kolom staat een knopje waarmee een content management veld gekopieerd wordt. Dat kan naar dit metadata model of een ander metadata model met dezelfde toptabel zijn.

**Verwijderen**

Met het **prullenbak** pictogram wordt een veld, na bevestiging, verwijderd.

**Sorteren**

De lijst van content management velden wordt gesorteerd door op de knopjes voor oplopend en aflopend sorteren te klikken.

**XSLT**
In de XSLT kolom staan knopjes waarmee de XSLT definitie van een content management veld opgeroepen wordt voor invoer en mutatie.

**Her-indexering**

Het is goed te beseffen dat wijzigingen aan en toevoegingen van Content Management Velden leiden tot her-indexering. De waarden van de velden worden immers berekend bij het opslaan van een object. Voor de reeds opgeslagen objecten moeten deze dan herberekend worden. Dit gebeurt door her-indexering. Afhankelijk van de verdere configuratie (bv. op de relaties) kan dit ook her-indexering van gerelateerde objecten tot gevolgd hebben. Her-indexering kan daarmee omvangrijk, tijdrovend en belastend zijn voor de omgeving. Het doorvoeren van wijzigingen en toevoegingen dienen derhalve met de nodige terughoudendheid en planmatig te worden gedaan.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Content Management Velden <../Concepts/Concepts.html#content-management-velden>`__
