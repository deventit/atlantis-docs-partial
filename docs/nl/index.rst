####################
Configuratie Cockpit
####################

.. toctree::
   :maxdepth: 10
   :caption: Configuratie Cockpit

   Preface/Preface
   Concepts/Concepts     
   General/General
   Setting/Setting
   Dashboard/Dashboard
   ContentManagementSystem/ContentManagementSystem   
   Authorization/Authorization
   Categories/Categories
   ListOfTerms/ListOfTerms
   Reports/Reports
   SearchEntries/SearchEntries
   GlobalMenu/GlobalMenu
 