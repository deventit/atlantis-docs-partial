
Invoer Formulier
----------------

Met dit paneel worden formulieren toegevoegd of gewijzigd.

Een formulier gaat over één tabel uit het metadata model. Kies daarvoor
de tabel waarvan de velden op dit formulier geplaatst dienen te worden.
Als er velden van een gerelateerde tabel opgenomen dienen te worden, dan
wordt daar een eigen formuliervoor  gemaakt en als subformulier
(aggregatie) opgenomen in dit formulier. Naast velden en aggregaties kan
het formulier worden opgebouwd uit gekoppelde objecten van gerelateerde
metadata modellen, tabbladen en veldgroepen.

Formulieren kunnen toegewezen worden aan gebruikersgroepen. Gebruikers
krijgen alleen de beschikking over formulieren voor invoer en mutatie
als deze: zijn toegewezen aan de gebruikersgroepen waar de gebruiker
deel van uitmaakt of niet toegewezen zijn aan gebruikersgroepen

Daarnaast kan een formulier als “Standaard” worden aangemerkt. Hiermee
is dit dan het formulier dat standaard gepresenteerd wordt aan de
gebruiker bij invoer of mutatie van het object. De gebruiker heeft
daarnaast de mogelijkheid om voor andere, toegewezen, formulieren te
kiezen.

**Hoofdinstellingen**

Een formulier heeft een aantal instellingen die ingevoerd dienen te
worden:

-  **Naam**: Dit is de naam van het formulier zoals die aan gebruikers gepresenteerd zal worden
-  **Sjabloon Naam**: De naam van het sjabloon met de opmaak van het
   formulier
-  **Tabel selectie**: Selecteer hier de tabel uit het metadata model
   waarvoor een formulier wordt opgesteld
-  **Groepen**: Selecteer hier de gebruikersgroepen waar het formulier
   aan toegekend wordt
-  **Standaard**: Geef hiermee aan of dit formulier het standaard
   formulier is voor de geselecteerde gebruikersgroepen.
-  **Thesaurus Link**: Geef hiermee aan dat thesaurus links op het formulier worden weergegeven.
-  **Editgrid**:  Geef hiermee aan dat het een formulier betreft voor EditGrid invoer.


Als het een nieuw formulier betreft, dan is de “\ **Opslaan**\ ” knop
beschikbaar. Hiermee wordt de definitie opgeslagen en komt de
Schermbouwer beschikbaar. Met “\ **Reset**\ ” worden de instellingen
teruggezet naar de waarden bij openen van de definitie of de laatst
bewaarde instellingen.

De Schermbouwer presenteert een knoppenbalk met daaronder een vlak met rijen en kolommen. Als het een nieuw formulier betreft heeft het formulier de dimensies van 1 rij en 1 kolom.

**Definiëren dimensies formulier**

Als eerste dienen de dimensies van het formulier bepaald te worden. Door op de knop **Fòrmulier** te klikken komen de eigenschappen van het formulier beschikbaar aan de rechterzijde van het formulier. Dit bevat de volgende onderdelen:

-  **Rijen**: Geef hier het aantal rijen van het formulier in.
-  **Kolommen**: Geef hier het aantal kolommen van het formulier in.

**Veld**

Velden worden op het formulier geplaatst door deze vanuit de knoppenbalk
te slepen naar de gewenste cel van het formulier. Door op de naam
linksboven te klikken komen de eigenschappen van het veld beschikbaar
aan de rechterzijde van het formulier. Dit bevat de volgende onderdelen:

-  **Veld Naam**: Selecteer hier het veld uit de lijst van velden van de
   tabel waarvoor het formulier wordt opgebouwd.
-  **Veld Type**: Als dit formulier een aggregatie betreft die
   herhaalbaar opgenomen wordt in een bovenliggend formulier, dan worden
   de geaggregeerde records via een tabelvorm gepresenteerd in het
   bovenliggende formulier. Een regel in de tabel is dan een record van
   de aggregatie. Voor de presentatie in tabelvorm kunnen velden uit het
   formulier geselecteerd worden. Met deze instelling wordt dit
   aangegeven. Met deze instelling wordt bepaald of dit veld zichtbaar
   is in alleen het formulier (alleen invoer), alleen bij presentatie in
   tabelvorm (alleen lezen) of op beide plaatsen (beide).
-  **Rij**: De huidige rij van het veld.
-  **Van kolom**: De beginkolom van het veld.
-  **Naar kolom**: De eindkolom van het veld.
-  **Volgorde in kop**: Als het veld bij de presentatie in tabel vorm
   wordt gebruikt, kan hier aangegeven worden op welke plaats (kolom)
   van die tabel dit veld gepresenteerd wordt.
-  **Tekst voor label**: De naam waarmee het veld wordt weergegeven in
   het formulier.
-  **Verplicht**: Vlag of invoer verplicht is.
-  **Alleen-Lezen**: Vlag of veld alleen gelezen kan worden.
-  **Wis in Kopie**: Vlag of de veldwaarde bij het maken van een kopie
   van het object gewist dient te worden voor het kopie object.
-  **Tekstblok**: Een veld waarin meerdere regels in opgenomen kunnen
   worden.
-  **Hoogte Tekstblok**: Hoogte van het veld in pixels
-  **Breedte**: Breedte van het veld in procenten
-  **Toon Thesaurus detail pop-up**: Indien een thesaurus gekoppeld is
   wordt een link naar de details van het thesaurus concept getoond.
-  **Verberg in ouder aggregatie kolommen**: Geeft aan of bij
   aggregaties binnen aggregaties dit veld meegenomen moet worden voor
   presentatie in tabelvorm van de bovenliggende aggregatie.
-  **Afhankelijk van**: Als dit veld voor de invoer afhankelijk is van
   een ander veld, kan dit veld hier geselecteerd worden.
-  **Datum Type**: Om te controleren of bij de invoer van datum ranges
   de startwaarde niet na de eindwaarde ligt, kan hier aangegeven worden
   of dit veld de vroegste of laatste datum is
-  **Groepering**: De velden in een datumrange worden bekend gemaakt
   door daar dezelfde groepsnaam aan te geven.

**Aggregatie**

Aggregaties (subformulieren) worden op het formulier geplaatst door deze
vanuit de knoppenbalk te slepen naar de gewenste cel van het formulier.
Door op de naam linksboven te klikken komen de eigenschappen van de
aggregatie beschikbaar aan de rechterzijde van het formulier. Dit bevat
de volgende onderdelen:

-  **Naam**: Geef de naam waarmee de aggregatie op het formulier wordt
   gepresenteerd.
-  **Rij**: De huidige rij van de aggregatie.
-  **Van kolom**: De beginkolom van de aggregatie.
-  **Naar kolom**: De eindkolom van de aggregatie.
-  **Sjabloon** Selecteer hier het formulier van de aggregatie.
-  **Enkelvoudige presentatie**: Bij een enkelvoudige presentatie wordt
   het formuliervoor  de aggregatie in dit formulier geplaatst. Is dit
   niet aangevinkt, dan is de aggregatie herhaalbaar en wordt een
   tabelvorm voor de presentatie van de geaggregeerde records gebruikt.
   Het formulier komt dan beschikbaar als op een tabelrij wordt geklikt.
-  **Tabel**: Selecteer hier de tabel van de aggregatie.
-  **Verplichte toevoegen**: Geeft aan of minimaal 1 record van de de aggregatie verplicht ingevoerd dient te worden.

Merk op dat óf een sjabloon óf een tabel geselecteerd wordt voor de aggregatie.

**Metadata model**

Objecten van gekoppelde metadata modellen worden in het formulier
weergegeven door de knop “\ **Metadata model**\ ” vanuit de
knoppenbalk te slepen naar de gewenste cel van het formulier. Door op de
naam linksboven te klikken komen de eigenschappen van dit veld
beschikbaar aan de rechterzijde van het formulier. Dit bevat de volgende
onderdelen:

-  **Relatie type**: Selecteer hier de relatienaam van de relatie
   waarmee de objecten gekoppeld zijn.
-  **Rij**: De huidige rij van het veld.
-  **Van kolom**: De beginkolom van het veld.
-  **Naar kolom**: De eindkolom van het veld.
-  **Tekst voor label**: De naam waarmee het metadata model object wordt
   weergegeven in het formulier.
-  **Verplicht**: Vlag of invoer verplicht is.
-  **Alleen-Lezen**: Vlag of veld alleen gelezen kan worden.
-  **Wis in Kopie**: Vlag of de veldwaarde bij het maken van een kopie
   van het object gewist dient te worden voor het kopie object.
-  **Tekstblok**: Een veld waarin meerdere regels in opgenomen kunnen
   worden.
-  **Hoogte Tekstblok**: Hoogte van het veld in pixels
-  **Breedte**: Breedte van het veld in procenten
-  **Meerdere relaties**: Geeft aan of er één metadata model object of
   meerdere gepresenteerd worden. In het laatste geval zal een tabelvorm
   worden gebruikt.
-  **CM Veld**: Selecteer hier het Content Management Veld waarmee het
   gekoppelde object op het formulier wordt gepresenteerd.

**Tabblad**

Aan het formulier kunnen tabbladen worden toegevoegd door de knop
“\ **Tab**\ ” vanuit de knoppenbalk te slepen naar de beginrij van het
tabblad. Door op de naam linksboven te klikken komen de eigenschappen
van het tabblad beschikbaar aan de rechterzijde van het formulier. Dit
bevat de volgende onderdelen:

-  **Naam**: De naam van het tabblad.
-  **Van Rij**: De eerste rij die in het tabblad opgenomen moet worden.
-  **Naar Rij**: De laatste rij die op het tabblad opgenomen moet
   worden.
-  **Volgorde**: Het rangnummer van het tabblad, Tabbladen worden
   gepresenteerd in volgorde van dit rangnummer.

**Veldgroep**

Rijen op het formulier kunnen omkaderd worden door de knop
“\ **Veldgroep**\ ” vanuit de knoppenbalk te slepen naar de beginrij van
de veldgroep. Door op de naam linksboven te klikken komen de
eigenschappen van de veldgroep beschikbaar aan de rechterzijde van het
formulier. Dit bevat de volgende onderdelen:

-  **Naam**: De naam van de veldgroep. Deze wordt in de omkadering
   weergegeven.
-  **Van Rij**: De eerste rij in de veldgroep.
-  **Naar Rij**: De laatste rij in de veldgroep.

**Component**

Naast informatie uit metadata modellen is aan een object ook systeeminformatie gekoppeld. Denk hierbij aan de moderatie vlag, audit informatie, etc. Dit soort systeeminformatie wordt geleverd via componenten. Met een component kan een bepaald soort sytseeminformatie geselecteerd worden voor presentatie op een formulier. Dit bevat de volgende onderdelen:

-  **Naam**: De naam van het component. Selecteer hier uit de beschikbare componenten.
-  **Rij**: De huidige rij van het component.
-  **Van kolom**: De beginkolom van het component.
-  **Naar kolom**: De eindkolom van het component.

**Aanpassen van breedte en verplaatsen**

Elementen op het formulier kunnen aangepast worden naar het aantal
kolommen dat in beslag wordt genomen. Als op de naam van het element
geklikt wordt, dan verschijnt rechtsonder een “\ **driehoek**\ ”.
Hiermee kan de breedte van het veld door slepen aangepast worden.

Elementen op het formulier kunnen verplaatst worden. Als op de naam van
het element geklikt wordt, dan verschijnt een “\ **pijlenknop**\ ”.
Hiermee kan het element door slepen verplaatst worden.

**Verwijderen**

Door op de naam van een element te klikken wordt met het
“\ **prullenbak**\ ” pictogram boven de eigenschappen aan de
rechterzijde dit element, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Formulieren <../Concepts/Concepts.html#formulieren>`__






