
Toekennen rechten aan Gebruikersgroepen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Met dit scherm worden rechten toegekend aan de desbetreffende groep.

**Filteren**

Zowel de lijst van groepen als van gebruikers kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Selecteren van rechten**

Via de aanvinkvelden worden rechten toegekend of onttrokken aan de groep.

**Beschikbare Rechten**


**Annnotaties**

De annotatie rechten hebben invloed op de mogelijkheden die een gebruiker met annotaties heeft. Deze rechten hebben alleen invloed als de annotatiemodule geactiveerd is.

Bekijken

Met dit recht komt er een nieuwe men-optie beschikbaar onder het medewerker menu: 'Annotaties'. Hierin kunnen alle toegevoegde annotaties bekeken en eventueel bewerkt worden.

Op de publieksomgeving heeft dit recht ook invloed. Zonder dit recht kan een gebruiker de toegevoegde annotaties bij een object niet bekijken.

Modereren

Op de publieksomgeving kunnen annotaties alleen bekeken worden als ze gemodereerd zijn (mits ingesteld). Met dit recht kan een gebruiker annotaties (de)modereren in de beheeromgeving.

Dit betekent dat een gebruiker met dit recht annotaties (on)zichtbaar kan maken voor de publieksomgeving.

Multimedia_toevoegen

Als dit recht is toegekend heeft een gebruiker de mogelijkheid multimediabestanden toe te voegen bij een annotatie. Deze mogelijkheid is beschikbaar op de beheer- en publieksomgeving.

Muteren

Een gebruiker met dit recht kan de tekst van bestaande annotaties aanpassen.

Toevoegen

Dit recht is heeft invloed op de publieksomgeving en zorgt ervoor  dat gebruikers nieuwe annotaties mogen plaatsen. Op de beheeromgeving bestaat deze mogelijkheid niet.

Verwijderen

Dit recht is alleen van toepassing op de beheeromgeving. Gebruikers met dit recht mogen bestaande annotaties verwijderen.

**Atlantis7**

De Atlantis7 rechten hebben invloed toegang tot Atlantis7 en de Configuratie Cockpit.

Beheer

Geeft toegang tot de Configuratie Cockpit.

Customer_Help_Aanmaken

Staat toe om eigen helpbestanden in de Configuratie Cockpit aan te maken.

Customer_Help_Muteren

Staat toe om eigen helpbestanden in de Configuratie Cockpit aan te passen.

**Batchacties**

Batchacties zijn taken die Atlantis geutomatiseerd kan uitvoeren. Deze zullen specifiek naar wens zijn gemaakt en alleen beschikbaar zijn en sommige omgevingen.

Bekijken

Toont de beschikbare batchacties bij medewerker > batch acties.

Uitvoeren

Geeft de mogelijkheid om de batchacties uit te voeren.

**Beheerwebsite**

Zonder dit recht is het niet mogelijk om op de beheeromgeving in te loggen.

**Beheer_aanvragenregistratie_bekijken**

Dit recht is nodig om het aanvragenregistratie scherm te mogen bekijken.

**Beheer_Applicatie**

Dit recht is nodig om diverse systeeminstellingen te kunnen wijzigen. Denk hierbij bijvoorbeeld aan de intranetrange in het systeeminstellingen scherm.

**Beheer_Audit**

Deze rechten hebben te maken met de Full Audit module en bepalen wat een gebruiker mag uitvoeren.

Header_Terugdraaien

Bepaald of de gebruiker alle gedane mutaties van een moment mag terugdraaien. Het is dan alleen mogelijk de hele mutatie aan te passen.

Prullenbak_bekijken

Bepaald of de gebruiker in het medewerker menu de optie 'Verwijderde documenten' te zien krijgt.

Regel_Terugdraaien

Bepaald of de gebruiker een gedeelte van de mutaties van een moment mag terugdraaien. Het is hiermee mogelijk een gedeelte van de mutatie aan te passen.

Verwijdering_Doorvoeren

Bepaald of de gebruiker in 'Verwijderde documenten' menu de beschrijvingen permanent mag verwijderen.

Verwijdering_Terugdraaien

Bepaald of de gebruiker in 'Verwijderde documenten' menu de beschrijvingen terug mag zetten op niet verwijderd.

**Beheer Auteursrecht**

De volgende rechten hebben betrekking tot het aanmaken van auteursrechthouders in het beheerder menu.

Bekijken

Met dit recht kunnen de auteursrechthouders bekeken worden.

Muteren

Met dit recht kunnen de auteursrechthouders gemuteerd worden.

Toevoegen

Met dit recht kunnen de auteursrechthouders toegevoegd worden.

Verwijderen

Met dit recht kunnen de auteursrechthouders verwijderd worden.

**Beheer Autorisatie**

Dit zijn diverse rechten die toegekend kunnen worden aan gebruikers welke beslissingen mogen nemen over de rechten van andere gebruikers (denk hierbij ook aan segmenten).

Deze rechten hebben alleen invloed op de beheeromgeving.

Bekijken

Met dit recht kunnen de diverse autorisatieinstellingen bekeken worden. Zonder dit recht is het ook niet mogelijk wijzigingen aan te brengen in de huidige autorisatie.

Muteren

Met dit recht mogen wijzigingen aangebracht worden in de huidige autorisatie. Denk hierbij aan het toekennen en ontnemen van rechten van gebruikersgroepen.

Toevoegen

Dit recht maakt het mogelijk nieuwe gebruikers, gebruikersgroepen en segmenten aan te maken.

Verwijderen

Dit recht geeft de mogelijkheid bestaande gebruikers, gebruikersgroepen en segmenten te verwijderen.

**Beheer_Bestellingenregistratie_bekijken**

Dit recht is nodig om het bestellingenoverzicht te mogen bekijken.

**Beheer_bezoekenregistratie_bekijken**

Dit recht is nodig om het bezoekenregistratie scherm te mogen bekijken.

**Beheer Diversen**

De rechten onder Beheer_Diversen zijn alleen van invloed op de beheeromgeving. Deze rechten hebben invloed op metavelden, metapaths, downloadresoluties, annotatiesoorten, auteursrechten, mediatypes, presentatieprofielen, tabbladen en volumes

Bekijken

Met dit recht worden de hiervoor genoemde items zichtbaar in het beheermenu.

Muteren

Met dit recht kunnen wijzigingen worden aangebracht in de hiervoor genoemde items.

Toevoegen

Met dit recht kunnen toevoegingen worden gedaan in de hiervoor genoemde items.

Verwijderen

Met dit recht kunnen in de hiervoor genoemde items verwijderingen worden uitgevoerd.

**Beheer Documenten**

Deze rechten hebben invloed de mogelijkheden die gebruikers hebben om met objecten op de beheeromgeving om te gaan.

Bekijken

Als een gebruiker de mogelijkheid moet hebben naar de detailpagina te kunnen navigeren bij een bestaand object, dan moet dit recht worden toegekend.

Toevoegen

Dit recht betekend dat een gebruiker nieuwe objecten toe mag voegen.

Muteren

Met dit recht mag een gebruiker bestaande objecten wijzigen.

Verwijderen

Dit recht zorgt ervoor  dat een gebruiker bestaande objecten mag verwijderen.

Samenvoegen

Dit recht zorgt ervoor  dat een gebruiker bestaande objecten mag samenvoegen.

Transformeren

Dit recht zorgt ervoor  dat een gebruiker bestaande objecten mag transformeren.

**Beheer_Formulieren**

Deze rechten hebben invloed de mogelijkheden die gebruikers hebben om aanpassingen te doen in het beheerder > document configuratie > formulieren scherm.

Bekijken

Het formulieren scherm en de formulieren mogen worden bekeken.

Muteren

Formulieren mogen worden gemuteerd. (Dit betreft de naam en locatie van het formulier, niet de velden in het formulier.)

Toevoegen

Formulieren mogen worden toegevoegd. (Dit betreft de naam en locatie van het formulier, niet de velden in het formulier.)

Verwijderen

Formulieren mogen worden verwijderd. (Dit betreft de naam en locatie van het formulier, niet de velden in het formulier.)

**Beheer_Keuzenlijsten**

Deze rechten hebben invloed de mogelijkheden die gebruikers hebben om aanpassingen te doen in het beheerder > document configuratie > keuzenlijsten scherm.

Bekijken

Het keuzenlijsten scherm en de keuzenlijsten mogen worden bekeken.

Muteren

Keuzenlijsten en opties mogen worden gemuteerd.

Toevoegen

Keuzenlijsten en opties mogen worden toegevoegd.

Verwijderen

Keuzenlijsten en opties mogen worden verwijderd.

Beheer_Metadatamodel_Verwijderen

Staat het toe om metadatamodellen uit de configuratiecockpit te verwijderen.

**Beheer OAI**

Dit zijn verschillende rechten welke betrekking hebben op OAI. Met deze rechten kunnen OAI-koppelingen worden ingesteld en beheerd.
Via OAI kunnen bronnen van buiten Atlantis in Atlantis worden ingeladen.

Deze rechten worden meestal alleen aan DEVENTit medewerkers toegekend.

Bekijken

Hiermee kunnen de verschillende OAI koppelingen worden bekeken. Zonder dit recht is het menu-item niet te zien.

Muteren

Met dit recht kunnen bestaande OAI koppelingen worden gewijzigd.

Toevoegen

Met dit recht kunnen nieuwe OAI koppelingen worden toegevoegd.

Verwijderen

Met dit recht kunnen bestaande OAI koppelingen worden verwijderd.

**Beheer_Rapporten**

Deze rechten hebben invloed de mogelijkheden die gebruikers hebben om aanpassingen te doen in het beheerder > document configuratie > rapportage scherm.

Bekijken

Het rapportage scherm en de rapporten mogen worden bekeken EN gemuteerd.

Verwijderen

Rapporten mogen worden verwijderd.

**Beheer Processen**

Deze rechten hebben invloed op het procesbeheer in atlantis.

Procesbeheer is momenteel nog niet beschikbaar.

**Beheer Seriegewijs**

De volgende rechten hebben betrekking tot de verschillende functionaliteiten bij het seriegewijs beschrijven. Als de gebruiker het recht heeft komt de optie bij het seriegewijs beschrijven beschikbaar.

Met_veldcontrole

Met Controle op veld niveau wordt alleen gekeken of de te wijzigen velden in alle geselecteerde objecten dezelfde inhoud hebben of leeg zijn.

Overschrijven

Bestaande waarde mag overschreven worden met de nieuwe waarde.

Toevoegen

Er mag een nieuwe waarde worden toegevoegd als er nog geen waarde aanwezig is.

Verwijderen

Bestaande waardes mogen verwijderd worden.

Zonder_veldcontrole

Seriegewijs beschrijven zonder veldcontroles overschrijft altijd bestaande waardes.

**Beheer_sodaanvragenregistratie_bekijken**

Dit recht is nodig om het Scanning on Demand aanvragenregistratie scherm te mogen bekijken.

**Beheer_Verplichte_Velden_Negeren**

Met dit recht mogen sommige verplichte velden toch leeg blijven tijdens het invoeren van een object.

Als de velden in de database verplicht zijn, moeten ze oneacht dit recht wel of niet is toegekend worden ingevuld.

Dynamische schermen houden op het moment van schrijven ook geen rekening met dit recht.

**Beheer_Vertrouwelijke_Velden_Inzien_Nat**

Deze groep rechten is alleen van toepassing op Naturalis. Door deze rechten te ontnemen kunnen bepaalde velden uit het formulier verwijderd worden.

Agent_Info

Heeft invloed op de Agent informatievelden.

Audit_Info

Met dit recht kunnen de auditvelden beschikbaar worden gesteld.

Locality_Info

Met dit recht kunnen verschillende location velden beschikbaar worden gesteld.

Storage_Info

Met dit recht kunnen verschillende storage velden beschikbaar worden gesteld.

**Beheer_Webwinkel**

Deze groep velden zijn van invloed op de webwinkelinstellingen.

Deze rechten hebben dus alleen invloed op de beheeromgeving.

Ook hebben deze rechten alleen nut als de webwinkel module geactiveerd is.

Bekijken

Dit recht is nodig om het webwinkel menu in de beheeromgeving te kunnen zien.

Toevoegen

Dit recht is nodig om administraties, feestdagen, kortingen, publicatiesoorten, reproductiesoorten, standaardtoeslagen en verzendmethodes toe te voegen

Muteren

Dit recht is nodig om administraties, feestdagen, kortingen, publicatiesoorten, reproductiesoorten, standaardtoeslagen en verzendmethodes aan te passen

Verwijderen

Dit recht is nodig om administraties, feestdagen, kortingen, publicatiesoorten, reproductiesoorten, standaardtoeslagen en verzendmethodes te verwijderen.

**Containerrelaties**

Deze rechten hebben invloed op de containerrelaties (relaties tussen brontypen) tussen verschillende objecten. Zonder dit recht kan een gebruiker bijvoorbeeld geen persoon aan een akte koppelen.

Toevoegen

Dit recht zorgt ervoor  dat een gebruiker nieuwe relaties tussen objecten kan leggen (koppelen).

Verwijderen

Dit recht zorgt ervoor  dat een gebruiker relaties tussen objecten kan verwijderen (ontkoppelen).

**Document_Alleeneigen_Muteren**

Dit recht bepaald of een gebruiker alleen in een beschrijving mag muteren wanneer deze beschrijving door dezelfde gebruiker is aangemaakt. Wordt overschreven door het document_muteren recht.

**Crowdsource_Documentbeschrijven**

Geeft het recht om in de crowdsourcing (publiek) een beschrijving te mogen beschrijven.

**DGP_Beschermde_Informatie**

Speciaal recht voor (U)GIAS. Geeft het recht beschermde informatie in te zien.

**DGP_Document_Mag_Bestemming_Aanpassen**

Speciaal recht voor (U)GIAS. Geeft het recht om de bestemming aan te passen.

**Document_Alleeneigen_bekijken**

Alleen records die de gebruiker zelf heeft aangemaakt kunnen door die gebruiker gevonden worden.

Document_Alleeneigen_muteren

Alleen records die de gebruiker zelf heeft aangemaakt kunnen door die gebruiker gemuteerd worden.

**Document_Alleeneigen_Muteren**

Dit recht bepaald of een gebruiker alleen in een beschrijving mag muteren wanneer deze beschrijving door dezelfde gebruiker is aangemaakt. Wordt overschreven door het document_muteren recht.

**Document**

Bekijken

Als dit recht niet is toegekend dan kunnen gebruikers niet op de detailschermen van de objecten komen.

Gemodereerd_muteren

Als dit recht niet is toegekend dan kunnen gebruikers gemodereerde beschrijvingen niet muteren.

Hernummeren

Met dit recht krijgen gebruikers het recht om in een ordening de bestanddelen te hernummeren.

Importeren

Als dit recht is toegekend heeft de gebruiker in het medewerker menu een extra optie "importeren". Met dit recht kunnen verschillende imports gedaan worden.

Modereren

Een gemodereerd object is zichtbaar op de publieksomgeving. Een gedemodereerd object is dit niet. Gebruikers met dit recht kunnen de moderatiestatus op een object veranderen om het zo (on)zichtbaar voor het publiek te maken.

Gebruikers met dit recht kunnen ongemodereerde objecten op de publieksomgeving zien en kunnen objecten direct vanuit de publieksomgeving (de)modereren

Muteren

Gebruikers met dit recht kunnen direct vanuit de publieksomgeving naar het detaildocument op de beheeromgeving navigeren.

Toevoegen

Gebruikers met dit recht mogen objecten op de beheeromgeving toevoegen

Verwijderen

Gebruikers met dit recht kunnen objecten verwijderen. Dit recht heeft invloed op de beheeromgeving en de publieksomgeving

**Document_toevoegen_onbeantwoordafgesloten**

Speciaal recht voor (U)GIAS. Staat toe aan een afgesloten vraag nog toevoegingen te doen.

**Dossiervorming**

Gebruikers met dit recht hebben op de publieksomgeving de mogelijk zelf dossiers aan te leggen. Hiervoor is wel de dossiermodule nodig.

**Dynamische_velden**

Gebruikers met dit recht kunnen in het beheer scherm van de dynamische veldconfiguratie.

**Exportsets_Bekijken**

Geeft in het medewerker menu het recht om de exportsets te bekijken.

**Externe_thesaurus**

Gebruikers met dit recht kunnen in het beheer scherm van de Externe Thesaurus.

**Importsets_bekijken**

Zonder dit recht is het niet mogelijk importsets te bekijken. Het is dus ook niet mogelijk jouw eigen importset te bekijken als dit recht niet is toegekend.

**Indexeren**

Als dit recht is toegekend kunnen gebruikers via de beheeromgeving een indexering van objecten starten. Zelfs als dit recht is toegekend, is er een admin wachtwoord nodig om tussen 9.00 en 17.00 uur te kunnen indexeren.

**Koppelen_Via_Barcode**

Staat toe om via Barcodes te koppelen (mits functionaliteit aanwezig en geconfigureerd).

**Movement**

Als dit recht is toegekend dan kan de gebruiker in het 'verplaatsen' scherm in het medewerker menu.

**Multimedia**

De volgende rechten hebben invloed op de bestanden welke worden toegevoegd als zijnde multimedia (dit kunnen echter allerlei soorten bestanden zijn, denk hierbij aan PDF of TXT).

Bekijken

Zonder dit recht kunnen gebruikers niet op de imageviewer van DEVENTit komen. Ook worden in de beheeromgeving de thumbnails op een detailbeschrijving verborgen. Het is dus niet mogelijk hier wijzigingen in aan te brengen, zelfs al is dat recht toegekend.

Modereren

Alleen gemodereerde multimedia is zichtbaar op de publieksomgeving. Gebruikers met dit recht kunnen multimediabestanden modereren.

Muteren

Gebruikers met dit recht kunnen bestaande multimediabestanden aanpassen. Het gaat hier om de metadata bij het multimediabestand, maar ook het multimediabestand zelf kan vervangen worden als dit recht is toegekend.

Toevoegen

Gebruikers met dit recht kunnen nieuwe multimediabestanden toevoegen aan bestaande objecten. Als het recht ontbreekt zal ook het multimedia toevoegen icoon verborgen worden.

**Niet_openbaar_bekijken**

Gebruikers met dit recht mogen auteursrechterlijk beschermde afbeeldingen bekijken.

Toevoegen
Gebruikers met dit recht kunnen nieuwe multimediabestanden toevoegen aan bestaande documenten. Als het recht ontbreekt zal ook het multimedia toevoegen icoon verborgen worden.

**URLSBekijken**

Gebruikers met dit recht kunnen de directe URLs van een multimediabestand zien als ze het multimediatabblad openen.

Verwijderen

Gebruikers met dit recht kunnen bestaande multimediabestanden verwijderen. Als een multimediabestand aan één object gekoppeld zit, wordt alleen de koppeling verbroken. Zodra de laatste koppeling wordt verbroken zal het bestand ook van schijf verwijderd worden.

**Navigeren**

Gebruikers met dit recht krijgen toegang tot het navigeren scherm in het medewerker menu.

**NietGemodereerdZoeken**

Gebruikers met dit recht mogen objecten vinden welke niet gemodereerd zijn. Normaal worden niet gemodereerde objecten verborgen. Dit recht heeft invloed op publiek en beheeromgeving.

**NietOpenbaarZoeken**

Gebruikers met dit recht mogen objecten vinden welke nog niet als 'OPENBAAR' zijn aangemerkt. Normaal worden niet openbare objecten verborgen. Dit recht heeft invloed op publiek en beheeromgeving.

**Openbare_Zoekvragen_Benaderen**

Gebruikers met dit recht krijgen onder het medewerker menu de optie "Zoekvragen". Hiermee kunnen bewaarde zoekvragen benaderd worden.

**Open_Data**

Toont de Open Data en bijbehorende sets in het beheerder menu. Staat ook toevoeging, mutatie en verwijdering toe.

**ProcesBeheer**

De volgende rechten hebben invloed op procesbeheer.

Bekijken

Nog niet geïmplementeerd

Muteren

Nog niet geïmplementeerd

Toevoegen

Nog niet geïmplementeerd

Verwijderen

Nog niet geïmplementeerd

**Rapportages**

Gebruikers met dit recht mogen rapportages maken. Rapportages geven gedetailleerde informatie in een vooraf bepaalde layout. Een rapport kan worden gemaakt over een detaildocument of over een selectie van objecten. Zonder dit recht wordt het knopje om rapporten mee te genereren verborgen.

**Rubrieken**

Medewerkers met dit recht krijgen bij een detailbeschrijving toegang tot het tabblad rubriekenschema.

**Scanningondemand_aanvragen**

Gebruikers met dit recht mogen een nieuwe SOD aanvraag toevoegen.

**Scanningondemand_muteren**

Gebruikers met dit recht mogen SOD aanvragen muteren.

**Schermbouwer**

De volgende rechten hebben invloed op de schermbouwer

Bekijken

Medewerkers met dit recht kunnen de schermbouwer templates en velden in het beheerder menu bekijken.

Muteren

Medewerkers met dit recht kunnen de schermbouwer templates en velden in het beheerder menu muteren.

Toevoegen

Medewerkers met dit recht kunnen nieuwe schermbouwer templates en velden aanmaken.

Verwijderen

Medewerkers met dit recht kunnen schermbouwer templates en velden verwijderen.

**Segmenten_Wijzigen**

Gebruikers met dit recht mogen segmenten toevoegen en verwijderen bij records.
Let op: Een gebruiker met een segmentbeperking kan met dit recht een record een segment geven die hij zelf niet mag zien, waardoor het record na opslaan voor deze gebruiker niet meer zichtbaar zal zijn.

**Seriegewijs_Beschrijven**

Gebruikers met dit recht hebben de mogelijkheid objecten seriegewijs te beschrijven. Seriegewijs beschrijven houdt in dat één wijziging op meerdere objecten tegelijk kan worden doorgevoerd. Zonder dit recht wordt het knopje voor seriegewijs beschrijven niet getoond.

**Sodbatch**

De volgende rechten hebben betrekking tot het digitaliseerbatches scherm in het scanning on demand scherm.

Bekijken

Medewerkers met dit recht kunnen de digitaliseerbatches bekijken.

Muteren

Medewerkers met dit recht kunnen de digitaliseerbatches aanpassen. Dit recht is ook nodig om de digitaliseerbatches te verwerken.

Toevoegen

Medewerkers met dit recht kunnen digitaliseerbatches aanmaken.

Verwijderen

Medewerkers met dit recht kunnen digitaliseerbatches verwijderen.

**Stambestand**

Stambestanden bevatten informatie welke voor meerdere objecten relevant kunnen zijn. Denk hierbij bijvoorbeeld aan Auteurs. Bibliotheekstukken kunnen meerdere auteurs hebben, maar een auteur kan ook aan meerdere bibliotheekstukken hebben gewerkt. Om niet telkens een nieuw bestand te hoeven aanmaken voor dezelfde auteur wordt zijn informatie in een stambestand opgenomen. Dit bestand kan dan bij meerdere objecten geselecteerd worden.

Modereren

Medewerkers met dit recht hebben de mogelijkheid stambestanden te modereren. Niet gemodereerde stambestanden kunnen niet gevonden worden door medewerkers welke het recht "Niet_gemodereerd_zoeken" niet hebben.

Muteren

Medewerkers met dit recht kunnen bestaande stambestanden aanpassen. Deze aanpassing kan dus van invloed zijn voor alle objecten welke aan dit stambestand gekoppeld zijn.

Toevoegen

Medewerkers met dit recht kunnen nieuwe stambestanden aanmaken.

Verwijderen

Medewerkers met dit recht kunnen bestaande stambestanden verwijderen. Als dit stambestand aan meerdere objecten gekoppeld is, zal dit stambestand van al deze objecten ontkoppeld worden.

**Statistieken_bekijken**

Gebruikers met dit recht kunnen de statistieken welke in Atlantis worden bijgehouden bekijken. In de statistieken worden onder andere veelgebruikte zoektermen bewaard. Statistieken worden alleen bewaard als de statistieken module actief is.

**Thesaurus**

Deze rechten hebben invloed op het thesaurusgedeelte in atlantis.

Bekijken

Gebruikers met dit recht kunnen de thesaurusitems in Atlantis bekijken. Zonder dit recht wordt onder het beheerder menu het kopje "Thesaurus browser" niet getoond.

Muteren

Gebruikers met dit recht mogen bestaande thesaurus objecten aanpassen.

Toevoegen

Gebruikers met dit recht mogen nieuwe thesaurus objecten toevoegen.

Verwijderen

Gebruikers met dit recht mogen bestaande thesaurusdocumenten verwijderen.

**Webwinkel_Plaatsen_Regel**

Staat een applicatie toe om via extern in onze webwinkel een bestelling te plaatsen. (Is niet nodig voor normale gebruikers)

**Werkset**

De volgende rechten gaan over de werksets in atlantis.

Bekijken

Gebruikers met dit recht kunnen alle openbare en al hun eigen werksets bekijken. Zonder dit recht wordt het kopje "Werksets" onder het medewerker menu niet getoond.

Muteren

Gebruikers met dit recht mogen documente aan bestaande werksets toevoegen. Ook kunnen deze gebruikers objecten uit bestaande werksets verwijderen. Verder geeft dit de mogelijkheid werksets te hernoemen.

Toevoegen

Gebruikers met dit recht mogen nieuwe werksets aanmaken.

Verwijderen

Gebruikers met dit recht mogen bestaande werksets verwijderen.

**Zoekvraag_Opslaan**

Gebruikers met dit recht mogen zoekvragen opslaan.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Autorisatie <../Concepts/Concepts.html#autorisatie>`__
