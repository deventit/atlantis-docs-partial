
Relatiemodel
------------

Raadplegen Relatiemodel
~~~~~~~~~~~~~~~~~~~~~~~

Dit paneel verschijnt rechts van het paneel met metadata modellen. Het paneel van de metadata modellen klapt automatisch in.

Van het metadata model waarvoor het relatiemodel is opgeroepen wordt het relatiemodel gepresenteerd. Dit bevat het metadata model en alle daaraan gerelateerde metadata modellen. De metadata modellen worden
gepresenteerd als cirkels en de relaties als lijnen tussen de cirkels. Het geselecteerde metadata model wordt groter en oranje weergegeven. Onder een metadata model wordt de naam weergegeven. Op de relaties
worden de relatienamen gepresenteerd, bezien vanuit het geselecteerde metadata model.

De relaties zijn of bovenliggend of onderliggend van het geselecteerde metadata model. Als een relatie bovenliggend is, dan betekent dit dat het metadata model waar de relatie naartoe loopt vanuit het geselecteerde metadata model een bovenliggend metadata model is. Bv. een relatie van een pagina naar een boek zal bovenliggend zijn. Dezelfde relatie vanuit het boek naar de pagina’s zal onderliggend zijn.

Bovenliggende relaties worden weergegeven in een blauwe kleur. Onderliggende relaties in een groene kleur.

Een relatie kan ook een pijl zijn. Dit betekent dat dit een hiërarchische relatie is. Dan loopt de pijl van bovenliggend naar onderliggend. Bv. een relatie tussen een boek en de pagina’s van het boek zal een hiërarchische relatie zijn. Hoewel relaties die niet worden aangegeven met een pijl ook als onderliggend of bovenliggend worden aangegeven, is hier meer sprake van een netwerkrelatie. Of iets boven of onderliggend is, is dan veelal een kwestie van perspectief. Bv. een maker en een object van de maker hebben een relatie met elkaar. Of dit onderliggend of bovenliggend is, is afhankelijk van hoe hiernaar gekeken wordt. Het kan beide onderliggend of bovenliggend zijn. Of een relatie hiërarchisch is of niet heeft gevolgen voor een aantal zaken:

-  In een boomweergave in de beheeromgeving kan vaak een filter gelegd worden op het presenteren van alle relaties of alleen de hiërarchische relaties.
-  Bij het opbouwen van de context van een object worden standaard de hiërarchisch bovenliggende relaties meegenomen. Hiermee wordt een object ondubbelzinnig geïdentificeerd.

Een relatie kan ook een dikkere lijn hebben. Dit betreft dan een eigenaarschap van de onderliggende metadata modellen van de bovenliggende metadata modellen. Een eigenaarschap betekent dat de onderliggende objecten van dat metadata model niet kunnen bestaan zonder een object van het bovenliggende metadata model.

**Navigatie**

Door op een cirkel te klikken wordt dit metadata model geselecteerd. Het relatiemodel wordt dan aangevuld met de metadata modellen waarmee deze een relatie heeft. Dit kan voor ieder metadata model worden gedaan waarmee het relatiemodel steeds completer en omvangrijker wordt. Het model kan verplaatst worden door:

-  met de linker muistoets ingedrukt te houden en de muis te bewegen
-  met de “\ **pijltjes**\ ” knoppen linksonder in het paneel

Met het scrollwiel van de muis kan het model in- en uitgezoomd worden. Dit kan ook met de “\ **zoom-in**\ ” en “\ **zoom-uit**\ ” knoppen rechtsonder in het paneel. Daar zit ook een “\ **standaard weergave**\ ” knop om het model naar het midden van het paneel en volledig zichtbaar
weer te geven.

**Wijzigen van metadata model**

De gegevens van een metadata model kan gewijzigd worden door op ditmetadata model te dubbelklikken (als dit metadata model nog niet geselecteerd is) of enkel te klikken (als het metadata model geselecteerd is), en te klikken op het “\ **potlood**\ ” pictogram. Dit opent een extra paneel aan de rechterzijde met de gegevens van het metadata model voor wijziging. Het paneel met het relatiemodel zal automatisch inklappen.

**Toevoegen van relaties**

Relaties worden toegevoegd door het metadata model te selecteren waar vanuit de relatie wordt aangemaakt, deze extra aan te klikken. Met het klikken komen de pictogrammen voor het toevoegen van een “\ **bovenliggende**\ ” of “\ **onderliggende**\ ” relatie. Hiermee komt de informatie voor het definiëren van een relatie in een extra rechterpaneel beschikbaar. Het paneel met het relatiemodel klapt dan automatisch in.

**Wijzigen of verwijderen van een relatie**

Een relatie kan gewijzigd of verwijderd worden door te klikken op derelatie. Hiermee komen pictogrammen beschikbaar voor het wijzigen (“**potlood**\ ”) of verwijderen (“**prullenbak**). Als gekozen wordt
voor wijzigen, dan komt de informatie voor het wijzigen van een relatie in een extra rechterpaneel beschikbaar. Het paneel met het relatiemodel klapt dan automatisch in. Als gekozen wordt voor verwijderen wordt na bevestiging de relatie verwijderd.

**Reset van het relatiemodel**

Als na het aanklikken van metadata modellen het model onoverzichtelijk geworden is, kan met de **Reset knop** rechtsboven het model naar de uitgangssituatie teruggebracht worden.

**Zie ook**: `Algemene Bedieningselementen <../General/General.html#MainGeneral>`__, `Relaties <../Concepts/Concepts.html#relaties>`__
