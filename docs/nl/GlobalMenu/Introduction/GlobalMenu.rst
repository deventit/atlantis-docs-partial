
Globaal menu
************

Het globale menu wordt opgeroepen door de knop **vier vierkantjes** rechtsboven in het scherm. Dit levert de configuratie van diverse onderdelen van de applicatie aan. Deze zijn onderverdeeld in de volgende functies:

1. **Algemeen**

   -  **Download Resoluties**: Dit configureert de downloadresoluties die beschikbaar worden gesteld bij het downloaden van afbeeldingen of uitsneden daarvan.
   -  **Auteursrechthouders**: Hiermee wordt een auteursrechthouders bestand opgebouwd met de NAW gegevens en toestemmingen die gegeven zijn ten aanzien van de (auteurs)rechten.
   -  **Presentatieprofielen**: Via een presentatieprofiel wordt geconfigureerd hoe met beeldmateriaal omgegaan wordt in termen van kwaliteit en resolutie met betrekking tot opslag, presentatie en download.
   -  **Annotatie Types**: Bij een annotatiesoort van een metadata model kan bij de invoer door een bezoeker één van deze types gekozen worden.  
   -  **Mediatypes**: Hiermee worden de iconen geregistreerd voor niet-afbeeldingen. Ook het standaard icoon als er nog geen multimediaal bestand aan een beschrijving is gekoppeld wordt hier ingesteld.
   -  **Verpakkingstypes**: Hiermee worden soorten verpakkingen zoals dozen, etc. geconfigureerd. 
   -  **Tabbladen**: De tabbladen die voor het weergeven van gerelateerde objecten beschikbaar zijn. 
   -  **Segmenten**: De beschikbare segmenten die toegepast kunnen worden.
   -  **Open Data**: De configuratie van de Open data bronnen die beschikbaar gesteld worden.
 
2. **Thema configuratie**

   -  **Themas**: De configuratie van beschikbare thema's die toegepast worden op de applicatie.
   
3. **Webshop**

   -  **Administraties**: De configuratie van verschillende webshops.
   -  **Kortingen**: Toe te passen kortingen op bestellingen van reproducties
   -  **Reproductiesoorten**: De soorten van reproducties die besteld kunnen worden.
   -  **Publicatiesoorten**: De soorten van publicaties voor de bestelde reproducties.
   
4. **Thesaurus**

   -  **Configuratie**: De configuratie van de thesauri die in de applicatie ondergebracht worden.
   -  **Browser**: Presenteert de thesauri die in de applicatie ondergebracht zijn via een browser.
   
5. **Indexen**

   -  **Indextypes**: Configuratie van de samenstelling van verschillende soorten van indexen (Nadere Toegangen) naar openbaarheid en beschikbare velden.
   -  **Indexen**: Configuratie van Indexbestanden, gekoppeld met een Indextype.
   
**In het globale menu worden alleen die opties aangeboden die vanuit de licentiesamenstelling toegankelijk zijn.**

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__