
Afdelingen
~~~~~~~~~~

In het Globale menu onder het kopje "**Autorisatie**" staat optie "**Afdelingen**". Afdelingen maken onderdeel uit van organisaties en kunnen toegekend worden aan gebruikers. 

In dit paneel worden de afdelingen onderhouden. Gepresenteerd wordt de lijst van afdelingen met de daarbij behorende organisaties.

**Filteren**

De lijst van afdelingen wordt gefilterd door in het “**Zoek hier**” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een afdeling wordt toegevoegd door in het paneel met de veiligheidsniveaus op het “**+**” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst met de volgende velden:

-  **Naam**: De naam van de afdeling.
-  **Organisatie**: Selectie van de bijbehorende organisatie.
-  **Mag originelen ontlenen**: Vlag of gebruikers van deze afdeling originele objecten mogen aanvragen voor inzage.

**Muteren**

Met het “**potlood**” pictogram komt de regel vrij voor aanpassing.

**Verwijderen**

Een afdeling wordt verwijderd met het “**prullenmand**” pictogram in de desbetreffende regel. Een afdeling kan alleen verwijderd worden als deze niet meer gebruikt wordt.

**Zie ook**: `Algemene Bedieningselementen <../General/General.html#MainGeneral>`__, `Organisaties <../GlobalMenu/GlobalMenu.html#organisaties>`__, `Gebruikers <../Authorization/Authorization.html#gebruikers>`__, `Autorisatie <../Concepts/Concepts.html#autorisatie>`__