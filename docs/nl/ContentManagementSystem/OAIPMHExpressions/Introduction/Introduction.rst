
OAI-PMH expressies
~~~~~~~~~~~~~~~~~~

Beheer OAI-PMH expressies
--------------------------

In dit paneel worden de expressies voor OAI-PMH gedefinieerd.

**Filteren**

De lijst van expressies voor OAI-PMH kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een expressie voor OAI-PMH wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van de expressie voor OAI-PMH ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de expressie voor OAI-PMH gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de expressie voor OAI-PMH, na bevestiging, verwijderd. Dit kan alleen als deze niet meer wordt gebruikt.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, , `OAI-PMH <../Concepts/Concepts.html#oai-pmh>`__