
Invoer Externe Thesauri
^^^^^^^^^^^^^^^^^^^^^^^

Voor het wijzigen van een externe thesaurus zijn in dit paneel de volgende velden beschikbaar:

-  **Naam**: Naam van de externe thesaurus.
-  **URL**: URL naar de externe thesaurus.
-  **Parameters**: Parameters voor het sturen van een zoekvraag.
-  **Limiet**: Aanduiding van het maximaal aantal resultaten per opvraging.
-  **Resultaat**: Aanduiding voor het resultaat.
-  **Gebruikersnaam**: Gebruikersnaam voor benadere van de externe thesaurus.
-  **Taal code**: Aanduiding van de taal.
-  **Land code**: Aanduiding van het land.
-  **Filter**: Filters voor de zoekvraag.
-  **Detail URL**: Detail URL voor een concept uit de externe thesaurus.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__