
Invoer E-Depots
^^^^^^^^^^^^^^^

Voor het wijzigen van een E-Depot zijn in dit paneel de volgende velden beschikbaar:

-  **Naam**: Selectie van een gekoppeld E-Depot.
-  **URL**: URL naar het E-Depot.
-  **Gebruikersnaam**: Gebruikersnaam voor inloggen op het E-Depot.
-  **Wachtwoord**: Wachtwoord voor inloggen op het E-Depot.
-  **Actief**: Vlag die aangeeft of het E-Depot actief is.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__