
Verpakkingen
------------

Beheer Verpakkingen
^^^^^^^^^^^^^^^^^^^

In dit paneel worden de verpakkingtypes gedefinieerd. Fysieke objecten kunnen in verpakkingen worden opgenomen voor plaatsing in bv. het depot. Verpakkingtypes definiëren o.a. de dimensies van de verpakking.

**Filteren**

De lijst van verpakkingstypes kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een verpakkingstype wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van de soort verpakking ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de soort verpakking gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de verpakkingstype, na bevestiging, verwijderd. 

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__