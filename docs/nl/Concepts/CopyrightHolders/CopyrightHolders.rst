
Auteursrechthouders
~~~~~~~~~~~~~~~~~~~

Een auteursrechthouder is een persoon of een bedrijf die de exclusieve rechten van een werk bezit. Een organisatie maakt afspraken met de auteursrechthouder over het niveau van de toestemmingen die gegeven worden ten aanzien van het gebruik van hun werk. In dit paneel worden naast de contactgegevens dit niveau van toegestaan gebruik vastgelegd.

**Afspraken over het gebruik**

De afspraken over het gebruik betreffen:

- Of de bezoeker toestemming van de auteursrechthouder moet vragen voor privégebruik van een werk of reproductie daarvan.
- Of de bezoeker toestemming van de auteursrechthouder moet vragen voor publicatie van een werk of reproductie daarvan.
- Of de Atlantis gebruikersorganisatie het werk mag publiceren via Internet.
- Of de Atlantis gebruikersorganisatie reproducties mag maken van het werk.
- Of de Atlantis gebruikersorganisatie het werk mag doorgeven aan sites/portalen van derden.

Daarnaast kan een auteursrechthouder eisen stellen aan de kwaliteit van publicatie via de gebruikersorganisatie op Internet of intranet, of er een watermerk mee gepubilceerd dient te worden en in welke resoluties het werk gedownload mag worden. Hiervoor wordt een toegesneden presentatieprofiel gekoppeld.

Atlantis houdt in alle functies die hiermee te maken hebben rekening met de afspraken die daarvoor gemaakt zijn. Dit betreft zowel publicatie via een publieksomgeving, bij het gebruik van de Atlantis API door derden en bij het beschikbaar stellen van data via (Linked) Open data, OAI-PMH protocol, SRU/OpenSearch bevraging en aanvragen voor reproductie.

**Creative Commons licentie**

Als publicatie via Internet is toegestaan, dan kan de bijbehorende Creative Commons recht aangegeven worden.

**Intranet of Internet**

Via de IP ranges met eventuele gekoppelde segmenten en/of gebruikersgroepen bepaalt Atlantis wat geldt als intranet of het Internet. Publicatie via het internet wordt niet beschouwd als publicatie en maakt daarmee geen inbreuk op auteursrecht.

**Presentatie van auteursrechterlijke werken**

Als een werk auteursrechterlijk beschermd is, dan wordt bij publicatie een "auteursrechterlijk beschermd" icoon weergegeven. Dit icoon kan ingesteld worden bij de definitie van een metadata model en geldt dan voor alle gekoppelde bestanden aan objecten van dit metadata model. Als er geen icoon gekoppeld is wordt het standaard icoon van van de Mediatypes hiervoor gebruikt.

