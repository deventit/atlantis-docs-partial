
Veiligheidsniveaus
~~~~~~~~~~~~~~~~~~

Veiligheidsniveaus worden gebruikt om gebruikers toegang te geven tot objecten van het eigen veiligheidsniveau of lager. Daarvoor wordt bij een gebruikersgroep het veiligheidsniveau aangegeven. Deze wordt ook toegekend aan afzonderlijk documentbeschrijvingen en de daaraan gekoppelde bestanden.

In dit paneel worden de veiligheidsniveaus onderhouden. Gepresenteerd wordt de lijst van veiligheidsniveaus bestaande uit een naam en een niveau via een getal. Het getal geeft het veiligheidsniveau aan.

**Filteren**

De lijst van veiligheidsniveaus wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een veiligheidsniveau wordt toegevoegd door in het paneel met de veiligheidsniveaus op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst waarin de naam en het niveau van de organisatie ingevoerd wordt.

**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij voor aanpassing van de naam en het niveau.

**Verwijderen**

Een veiligheidsniveau wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel. Een veiligheidsniveau kan alleen verwijderd
worden als deze niet meer gebruikt wordt.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Groepen <../Authorization/Authorization.html#gebruikersgroepen>`__, `Autorisatie <../Concepts/Concepts.html#autorisatie>`__