
Metadata modellen
~~~~~~~~~~~~~~~~~

Een Atlantis omgeving werkt met verschillende soorten objecten. Denk aan beeldbank-, archief- en museale objecten. In Atlantis is het concept object volledig doorgetrokken naar alle onderdelen waar velden voor beschrijving op van toepassing zijn. Dus ook rubrieken, categorieën, depotplaatsen, bestellingen, aanvragen, annotaties, multimedia, etc.

Een soort object wordt in Atlantis een metadata model genoemd. Een metadata model staat voor een beschrijvingsstructuur van (relationele) tabellen en velden. Veelal zijn dit beschrijvingsstructuren die geënt zijn op standaarden. Denk hierbij aan ISAD(G) voor archieven, reeksen, series en bestanddelen, ISAAR(CPF) voor archiefvormers, GABOS, Dublin Core of anderszins voor beeldobjecten, SPECTRUM, Basis Registratie Kaart of Historische Voorwerpen Kaart voor museale objecten, etc. Atlantis is volledig vrij in de toe te passen metadata modellen. Deze worden als het ware "ingeplugd" in het generieke multimediale informatiesysteem.

**Generieke eigenschappen metadata model**

Op de metadata modellen worden allerlei generieke eigenschappen gedefinieerd zodat het multimediale informatiesysteem "weet" hoe het hiermee om moet gaan, zoals:

- De relaties met andere objecten.
- Het al dan niet kunnen koppelen van bestanden.
- De formulieren voor invoer en mutatie.
- Het zoeken, presenteren zoekresultaten, sorteren, filteren en presenteren detailinformatie.
- De Koppeling met een E-Depot.
- Beschikbaarheid voor aanvragen, het bestellen van reproducties en scanning on demand.
- Etc.

**Uitbreiding van beschrijvingsstructuren**

Via de dienst **Dynamische Velden** kunnen de beschrijvingsstructuren van metadata modellen uitgebreid worden met extra tabellen en velden. Hiervoor dient deze dienst deel uit te maken van de licentie.

**Modulen en diensten**

De metadata modellen komen beschikbaar via modulen en diensten. Een module of dienst is gericht op een  soort collectie, toepassingsgebied of aanvullende diensten en leveren één of meer metadata modellen. Modulen en diensten maken onderdeel uit van de licentie van een gebruikesorganisatie. Het al dan niet beschikken over bepaalde metadata modellen is daarom afhankelijk van de aangeschafte modulen en diensten.

**Uitwerking binnen de Atlantis beheeromgeving**

Via de modulen in een licentie wordt in de Atlantis beheeromgeving het "Toevoegen" menu opgebouwd. Hiermee worden nieuwe objecten van metadata modellen toegevoegd. Dit menu geeft eerst een lijst van modulen en dan per module een lijst van metadata modellen.