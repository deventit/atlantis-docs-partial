
Editgrid templates
~~~~~~~~~~~~~~~~~~

Editgrid templates invoertemplates waarmee via een editgrid meerdere objecten tegelijkertijd beschreven worden.

In dit paneel worden de editgrid templates onderhouden. Gepresenteerd wordt de lijst van editgrid templates.

**Filteren**

De lijst van editgrid templates wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een editgrid template wordt toegevoegd door in het paneel met editgrid templates op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst met de volgende velden:

-  **Editgrid template**: De naam van de editgrid template
-  **Template**: Selectie van de template.
-  **Hoofdtabel**: De hoofdtabel voor de template. 


**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij voor aanpassing.

**Verwijderen**

Een editgrid template wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__