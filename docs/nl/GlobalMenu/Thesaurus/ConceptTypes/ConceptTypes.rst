
Concept typen
-------------

Concept typen geeft aan welke concept typen voor de thesauri beschikbaar zijn. Een thesaurus bestaat uit nodes van een bepaald type. Ieder concept type zal van een bepaald niveau zijn en via een kleur worden aangegeven om visueel het onderscheid aan te geven.

In dit paneel worden de concept typen onderhouden. Gepresenteerd wordt de lijst van concept typen.

**Filteren**

De lijst van concept typen wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een concept type wordt toegevoegd door in het paneel met concept typen op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst met de volgende velden:

-  **cNaam**: Naam van het concept type.
-  **Kleur**: Kleur van het concept type. 
-  **Niveau**: Niveau van het concept type.

**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij voor aanpassing.

**Verwijderen**

Een concept type wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__