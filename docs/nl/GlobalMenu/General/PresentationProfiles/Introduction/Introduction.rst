
Presentatie Profielen
---------------------

Beheer Presentatieprofielen
^^^^^^^^^^^^^^^^^^^^^^^^^^^

In dit paneel worden de Presentatieprofielen gedefinieerd. Een Presentatieprofiel definieert hoe afbeeldingen behandeld worden binnen de applicatie. Dit betreffen dan instellingen voor:

- De kwaliteit, resolutie en bestandformaat voor presentatie op intra- en internet
- De kwaliteit, resolutie en bestandformaat voor het icoon
- Het watermerk, de transparantie en de plaatsing daarvan
- De beschikbare download resoluties voor diverse situaties

**Filteren**

De lijst van Presentatieprofielen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een Presentatieprofiel wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van het Presentatieprofiel ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van het Presentatieprofiel gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt het Presentatieprofiel, na bevestiging, verwijderd. Dit kan alleen als deze niet meer wordt gebruikt bij metadata modellen.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Presentatie profielen <../Concepts/Concepts.html#presentatie-profielen>`__ 