
Module instellingen
~~~~~~~~~~~~~~~~~~~

Soorten Depotplaatsen
---------------------

Beheer Soorten Depotplaatsen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Soorten depotplaatsen definieert de soorten depotplaatsen. Er kunnen vrij soorten depotplaatsen gedefinieerd worden. 

**Soorten depotplaatsen**

Het scherm voor de soorten depotplaatsen presenteert een paneel aan de linkerzijde. Dit geeft een overzicht van de beschikbare soorten depotplaatsen. 

**Filteren**

De lijst met soorten depotplaatsen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. 

**Toevoegen** 

Met het **toevoegen** pictogram boven de lijst met soorten depotplaatsen wordt een nieuwe soort depotplaats toegevoegd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de soort depotplaats wordt weergegeven voor invoer.

**Wijzigen** 

Met het **potlood** pictogram naast de naam van een soort depotplaats wordt een soort depotplaats gewijzigd. Hierbij wordt een paneel aan de rechterzijde geopend waarin de informatie van de soort depotplaats wordt weergegeven voor mutatie.

**Verwijderen** 

Met het **prullenbak** pictogram wordt een soort depotplaats verwijderd. Dit kan alleen als de soort depotplaats niet in gebruik is.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__