
Auteursrechthouders
-------------------

Beheer Auteursrechthouders
^^^^^^^^^^^^^^^^^^^^^^^^^^

Een auteursrechthouder is een persoon of een bedrijf die de exclusieve rechten van een werk bezit. Een organisatie maakt afspraken met de auteursrechthouder over het niveau van de toestemmingen die gegeven worden ten aanzien van het gebruik van hun werk. In dit paneel worden naast de contactgegevens dit niveau van toegestaan gebruik vastgelegd.

Bij het selecteren van de optie voor Auteursrechthouders uit het Globale Menu wordt een paneel geopend. Hierin wordt een lijst gepresenteerd van de geregistreerde auteursrechthouders.

**Filteren**

De lijst van Auteursrechthouders kan gefilterd worden door in het "Zoek hier" veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen en wijzigen**

Een auteursrechthouder wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van de auteursrechthouder ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de auteursrechthouder gewijzigd worden.


**Verwijderen**

Met het **prullenbak** pictogram wordt een auteursrechthouder, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Auteursrechthouders <../Concepts/Concepts.html#auteursrechthouders>`__