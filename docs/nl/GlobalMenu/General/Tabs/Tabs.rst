
Tabbladen
---------

In dit paneel worden Tabbladen gedefinieerd. Een tabblad wordt gebruikt bij de presentatie van een metadata object om gekoppelde objecten gegroepeerd weer te geven in een eigen tabblad. Gekoppelde objecten die niet in een hier gedefinieerd tabblad worden gepresenteerd, komen automatisch in het tabblad **Relaties**. Tabbladen worden toegewezen bij de definities van **Relaties** tussen metadata modellen.

**Filteren**

De lijst van tabbladen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Paginering**

De tabel van tabbladen is gepagineerd. Onderaan de tabel worden de **paginanummers** weergegeven. Hierop kan door te klikken direct deze pagina opgeroepen worden. Met de **>** en **<** pictogrammen wordt naar de volgende of vorige pagina gegaan. Met de **|<**en **>|** pictogrammen wordt naar de eerste en laatste pagina gegaan.

**Toevoegen en wijzigen**

Een tabblad wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de tabel waar de gegevens van het tabblad ingevoerd worden. Met het **potlood** pictogram komt de regel voor wijziging beschikbaar. 

De volgende velden zijn beschikbaar:

-  **Beschrijving**: De beschrijving van het tabblad
-  **Tabblad**: De naam van het tabblad.
-  **Alleen-lezen**: Geeft aan of de gepresenteerde koppelingen met objecten alleen gelezen kunnen worden en derhalve er geen koppelingen toegevoegd of verwijderd mogen worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt het tabblad, na bevestiging, verwijderd. Dit kan alleen als deze niet meer wordt gebruikt bij een **Relatie**.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__