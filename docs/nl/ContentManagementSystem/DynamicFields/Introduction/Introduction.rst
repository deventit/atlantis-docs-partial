
Dynamische Velden
-----------------

Beheer Dynamisch Veld
~~~~~~~~~~~~~~~~~~~~~

Aan de tabellen van de metadata modellen worden velden toegevoegd via Dynamische Velden. Deze velden komen op dezelfde manier beschikbaar als de standaard velden. Daarmee kunnen ze toegepast worden bij de definitie van Content Management Velden, in rapportages en in formulieren.

Bij het selecteren van de optie voor Dynamische Velden bij het metadata model wordt een paneel aan de rechterkant geopend. Hierin wordt een lijst gepresenteerd van de gedefinieerde Dynamische Velden.

**Filteren**

De lijst van Dynamische Velden kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een Dynamisch Veld wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van het Dynamische Veld ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van het Dynamische Veld gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt een Dynamisch Veld, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Metadata modellen <../Concepts/Concepts.html#metadata-modellen>`__