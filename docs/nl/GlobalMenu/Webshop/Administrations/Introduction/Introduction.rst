
Administraties
--------------

Beheer Administraties
^^^^^^^^^^^^^^^^^^^^^

Het scherm voor de Administraties presenteert een paneel aan de linkerzijde. Dit geeft een overzicht van de beschikbare administraties. Deze lijst kan  gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. 

**Functies op Administraties**

Door de muis over een administratie te bewegen komt een menu beschikbaar. Dit zijn mogelijkheden voor configuratie per Administratie. Dit betreft:

-  **Gesloten dagen**: De dagen van de opengestelde dagen waarop de studiezaal gesloten is.
-  **Open dagen**: De dagen van de week dat de studiezaal doorgaans geopend is.
-  **Dagdelen**: De dagdelen waaruit bij een aanvraag voor inzage geselecteerd kan worden.
-  **Standaard supplementen**: Standaard toeslagen per besteld artikel of per bestelling.
-  **Verzendmethoden**: De beschikbare verzendmethoden.
-  **Factuurnummer**: Start factuurnummer vanaf een opgegeven datum.

Door een menu-optie aan te klikken wordt de desbetreffende functie geopend in een paneel aan de rechterzijde van de lijst met administraties.

**Toevoegen en wijzigen** 

Een administratie wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van de administratie ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de download resolutie gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de administratie, na bevestiging, verwijderd. Dit kan alleen als deze verder niet in gebruik is.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__