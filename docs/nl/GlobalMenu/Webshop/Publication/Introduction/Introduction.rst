
Publicatiesoorten
-----------------

Bestellingen van reproductie zijn voor privégebruik of voor publicatie. Als het voor publicatie is, dan worden de daarvoor de beschikbare soorten van publicatie gedefinieerd. Per publicatiesoort worden kosten en eventueel metadata modellen aan toegekend.

De publicatiesoorten worden gepresenteerd in het paneel aan de linkerkant. 

**Filteren**

Zowel de lijst van publicatiesoorten kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen of wijzigen**

Een publicatiesoort wordt toegevoegd door in het paneel met de publicatiesoorten op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een extra paneel aan de rechterkant waarin de gegevens van de publicatiesoort ingevoerd wordt. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de publicatiesoort gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de publicatiesoort, na bevestiging, verwijderd. 

**Toewijzen van metadata modellen**

Met het **M** pictogram wordt een extra paneel aan de rechterzijde geopend voor het toewijzen van metadata modellen aan de publicatiesoort. Hiermee wordt komt de publicatiesoort alleen beschikbaar bij de toegewezen metadata modellen.

**Toewijzen publicatiekosten**

Met het **Publicatiekosten** pictogram wordt een extra paneel aan de rechterzijde geopend voor het toewijzen van de publicatiekosten aan de publicatiesoort. 

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__