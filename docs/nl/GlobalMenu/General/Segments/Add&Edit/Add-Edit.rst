
Invoer Segment
^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Beschrijving**: De naam van het segment. 

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Autorisatie <../Concepts/Concepts.html#autorisatie>`__
