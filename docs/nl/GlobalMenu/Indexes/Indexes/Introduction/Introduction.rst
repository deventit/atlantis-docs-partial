
Toegangen
---------

Beheer Toegangen
^^^^^^^^^^^^^^^^

In dit paneel worden de nadere toegangen gedefinieerd. Een nadere toegang is een verzameling van objecten waarvan de eigenschappen, zoals openbaarheidsbeperkingen en velden bepaald worden door de koppeling met een **soort nadere toegang**. In archieftermen wordt een nadere toegang ook wel aangeduid als **Akten en Registers**.

**Filteren**

De lijst van toegangen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een toegang wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van de toegang ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de toegang gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de toegang, na bevestiging, verwijderd. 

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__