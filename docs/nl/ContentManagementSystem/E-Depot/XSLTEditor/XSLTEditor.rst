
Invoer XSLT Content Management Velden
-------------------------------------

Bestanden kunnen bij invoer worden opgeslagen in een E-Depot. De multimedia bestanden zijn daarbij gekoppeld aan objecten van metadata modellen. Naast het multimediale bestand wordt doorgaans ook metadata over het object en het bestand in het E-Depot bewaard. Deze metadata komt vanuit het metadata object, daaraan gerelateerde objecten en technische metadata van het bestand.

Gedefinieerd dient te worden welke metadata in met welke opbouw in het E-Depot worden bewaard. Dit wordt gedefinieerd via de E-Depot optie bij het metadata model. Als voor deze optie gekozen wordt, dan wordt een rechterpaneel geopend waarin deze definitie gemaakt of gewijzigd wordt.

Of een metadata model opslag in het E-Depot toestaat, wordt aangegeven met het vinkveld **Beschikbaar voor E-Depot**. Via de XSLT-Editor wordt de definitie van de informatieoverdracht samengesteld.

**XSLTL** staat voor Extensible Stylesheet Language. Dit is een stylingtaal voor informatie in XML. XSLT wordt gebruikt om XML-objecten om te zetten in andere formaten, zoals HTML, CSV, XML, etc. Omdat de informatie over objecten beschikbaar is in XML is XSLT een geschikte manier om deze om te vormen naar rapportages, Content Management Velden en E-Depot mappings. XSLT is echter een programmeertaal, en daarmee is het omvormen een programmeeractiviteit. 

Om applicatiebeheerders de mogelijkheid te geven om dit zelf te kunnen uitvoeren, is de XSLT-Editor ontwikkeld. Dit biedt een RichText Editor (RTE) waarmee Teksten, opmaak, objectinformatie en condities gecombineerd kunnen worden zonder dat er geprogrammeerd hoeft te worden. Als er, voor geavanceerdere functies, toch geprogrammeerd moet worden is naast de RTE ook de programmacode (broncode) beschikbaar. 

De RTE genereert de XSLT programmacode. Dit gebeurt niet andersom. Dit betekent dat wanneer de broncode aangepast wordt, de RTE niet meer gebruikt kan worden. Bij het oproepen van de XSLT-Editor zal voor deze situatie een waarschuwing getoond worden. Dit geeft aan dat alleen met de broncode gewerkt kan worden. Wordt dan toch met de RTE gewerkt, dan wordt de broncode overschreven.

De RTE is beschikbaar in het tabblad **Rich editor**. De broncode is beschikbaar in het tabblad **Broncode**. De tabbladen bevinden zich onderaan het scherm.

In de kop van de RTE staan, aanvullend op de opmaak knoppen, knoppen om XSLT elementen op te nemen, dit betreffen:

-  **Metadata model**: Als de XSLT-Editor ingezet wordt om een rapport te maken, dan dient een metadata model gekozen te worden. 
-  **Invoegen**: Via deze knop wordt een veld van het metadata model, een iteratie over een lijst (subtabel), een navigatie over een relatie naar een gerelateerd metadata model, een tekst of een functie ingevoegd.
-  **Conditie**: Hiermee wordt een conditie in de vorm van een "Als" of een "Als anders" ingevoegd.
-  **Snippet**: Met deze knop wordt een standaard stukje XSLT code ingevoegd.

Deze bovenstaande functies voegen teksten toe in de RTE op de plaats van de cursor. Deze teksten worden ingesloten in "{{" en "}}" haken. Bijvoorbeeld een veld wordt ingevoegd als: "*{{\@TITEL}}*". Sommige onderdelen hebben een begin- en een eindtekst. Daarbinnen kan dan gewerkt worden. Bijvoorbeeld een keuze voor een metadata model wordt aangegeven door:

*{{meta:%Archieven}}*

*{{/meta}}*

Om binnen dit metadata model aan de slag te gaan, moet tussen de twee tekstblokken gewerkt worden. Onderstaand voorbeeld zet het veld Titel van een archief neer:

*{{meta:%Archieven}}*

*{{\@TITEL}}*

*{{/meta}}*

Voor alle onderdelen die een begin- en een eind tekstblok leveren moet binnen deze blokken de eigen informatie toegevoegd worden. De tekstblokken mogen nooit door directe invoer aangepast worden. Dit kan via de **Bijwerken** knop.

Naast bovenstaande knoppen zijn aanvullend de onderstaande XSLT knoppen beschikbaar:

-  **Bijwerken**: Door een XSLT tekstblok te selecteren kan met deze knop het tekstblok aangepast worden
-  **Voorvertoning (oog)**: Met deze knop kan de XSLT code van de RTE inhoud worden geraadpleegd zonder dat dit naar het **Broncode** tabblad geschreven wordt.

**Functies van de RTE**

Via de knoppen boven het tekstvak worden de volgende functies geleverd:

-  **Bold**: Geeft de tekst **vet** weer. Sneltoetscombinatie is CTRL + B.
-  **Italic**: Zorgt ervoor dat de geselecteerde tekst *cursief* gemaakt wordt. Sneltoetscombinatie is CRTL +.
-  **Strikethrough**: Haalt de geselecteerde tekst door.
-  **Underline**: Onderstreept de geselecteerde tekst. Sneltoetscombinatie is CTRL + U.
-  **Lists**: maakt de geselecteerde tekst op met als een lijst met opsommingstekens of nummers.
-  **Block Quote**: Maakt de geselecteerde tekst op als een citaat.
-  **Text Alignment**: Deze set van vier knoppen past de geselecteerde tekst aan om ofwel links uitgelijnd, gecentreerd, rechts uitgelijnd of uitgevuld te worden.
-  **Link**: Hiermee kan een hyperlink toegevoegd worden.
-  **Unlink**: Hiermee wordt een hyperlink verwijderd.
-  **Anchor**: Dit wordt gebruikt worden om een anker toe te voegen. Dit biedt een referentiepunt waar rechtstreeks via een URL naar verwezen kan worden.
-  **Expand**: De RTE zal hiermee over de volledige breedte en hoogte van de browser wordt weergegeven.
-  **Paragraph Format** De tekst wordt veranderd in naar de lettertypestijl die wordt geselecteerd.
-  **Font Size**: Hiermee wordt de lettergrootte van de geselecteerde tekst gewijzigd.
-  **Font Colour**: Hiermee wordt de kleur van de geselecteerde tekst gewijzigd.
-  **Background Colour**: Hiermee wordt de achtergrondkleur van de geselecteerde tekst gewijzigd.
-  **Paste as Plain Text**: Hiermee worden teksten geplakt zonder de opmaak mee te nemen.
-  **Paste from Word**: Hiermee worden teksten geplakt met behoud van opmaak.
-  **Remove Formatting**: Hiermee wordt de opmaak van de geselecteerde tekst verwijderd.
-  **Insert Image**:  Hiermee wordt een afbeelding ingevoegd via een URL, breedte, hoogte, kaders, Hspace, Vspace en uitlijning.
-  **Insert Table**: Hiermee wordt een tabel toegevoegd door het aangeven van het aantal rijen, kolommen, uitlijning, breedte, hoogte, cel afstand
   en witruimtes tussen de cellen.
-  **Insert Special Character/Symbol**: Hiermee wordt een speciaal teken of symbool ingevoegd.
-  **Text Indent** Deze twee toetsen regelen het niveau van het inspringen van de tekst, zowel naar rechts als naar links.
-  **Undo** Hiermee worden wijzigingen, stap voor stap, ongedaan gemaakt.
-  **Redo** Dit stelt de gebruiker in staat wijzigingen opnieuw toe te passen nadat ervoor gekozen was de wijzigingen ongedaan te maken.
-  **Page Break**: Dit voegt een pagina-ende toe.
-  **Source** Deze knop geeft de HTML-broncode weer.

**Keuze metadata model**

Als de XSLT-Editor gestart wordt vanuit de **Rapporten** in het **Dashboard**, dan moet eerst een metadata model gekozen worden waarvoor het rapport wordt opgesteld. Door op de **Metadata modellen** knop te klikken wordt een lijst met metadata modellen gepresenteerd. Hieruit kan een metadata model geselecteerd worden. Dit zal in de RTE de metadata model tekstblokken plaatsen. Van dit metadata model kunnen nu de velden uit de hoofdtabel van dit metadata model gebruikt worden. Als velden uit een subtabel nodig zijn moet een **lijst** daarvoor ingevoegd worden.

**Tabellen**

Met de **Table** knop kan een tabel worden ingevoegd. In de getoonde dialoog kunnen de eigenschappen aangegeven worden. Om na het invoegen eigenschappen toe te voegen of aan te passen dient op de tabel met de rechtermuisknop het context menu opgeroepen te worden. Dit biedt diverse mogelijkheden om tabel, rij en kolom eigenschappen aan te passen.

**Gebruik van de Source knop**

De **Source** knop kan niet gebruikt worden om aanpassingen uit te voeren. Deze worden niet meegenomen in het genereren van de XSLT broncode.

**Gebruik van velden, teksten en functies**
Op plaatsen waar een XSLT tekstblok wordt ingevoegd wordt een pop-up scherm getoond waarin extra parameters gevraagd worden. Als een parameter een enkelvoudige waarde is, kan doorgaans gekozen worden voor 3 opties:

-  **Veld**: Dit geeft de mogelijkheid om een veld uit de huidige tabel te selecteren. 
-  **Tekst**: Dit geeft de mogelijkheid om een vaste tekst in te voeren.
-  **Functie**: Dit geeft de mogelijkheid om een functie te selecteren. Als de functie geselecteerd is kunnen de parameters van de functie ingevoerd worden. Ook dit geeft doorgaans weer keuzen uit Veld, Tekst of Functie.

**Invoegen**

Moet deze knop kan een enkelvoudige waarde, een lijst of een relatie ingevoegd worden.

-  **Enkelvoudige waarde**: Dit is een keuze uit een veld, tekst of functie.
-  **Lijst**: Dit geeft de keuze uit een subtabel van de huidige tabel. Binnen de lijst wordt geïtereerd over de records van de subtabel. Dit betekent dat de code in het lijstdeel voor ieder betreffende record van de subtabel uitgevoerd zal worden.
-  **Relatie**: Hiermee wordt over een relatie genavigeerd waarmee bij de, via deze relatie, gerelateerde objecten terecht gekomen wordt. De huidige tabel wordt dan de hoofdtabel van het metadata model van de gerelateerde objecten. Binnen de relatie wordt geïtereerd over de gerelateerde objecten. Dit betekent dat de code in het relatiedeel voor ieder gerelateerd object uitgevoerd zal worden.

**Conditie**

Met deze knop kan een conditie worden toegevoegd. Dit kan een Als of een Als-anders conditie zijn. Bij een conditie wordt een Variabele, een Operator en een Waarde ingevoerd. Bij de Variabele en de Waarde kan gekozen worden voor een Veld, Tekst of Functies. Dit geeft een XSLT tekstblok zoals onderstaand weergegeven:

*{{choose}}*
*{{if: test="NUMMER = atlantis:GetAantalMultimedia(37920722)"}}*
*{{/if:}}*
*{{/choose}}*

Als gekozen wordt voor een Als-anders dan wordt 
*{{else}}*
*{{/else}}*
tussen de {{/if:....}} en {{/choose)) tekstblokken geplaatst.

Let op: Tussen de {{choose}} en de {{if: mag geen teksten o.i.d. tussengevoegd worden. Dat geldt ook voor de {{/...}} en {{/choose}} tekstblokken.

De code die dient te worden uitgevoerd staat tussen de {{if: ....}}  en  {{/if:....}} tekstblokken of tussen de {{else}} en {{/else}} tekstblokken.

**Snippets**
Met deze knop wordt een lijst van XSLT code snippets opgeroepen. Bv. De snippet "Enter" geeft de XSLT code voor het invoegen van een <ENTER> om naar de volgende regel te gaan. Deze lijst zal doorlopend aangevuld worden.

**Bijwerken**

Door een XSLT tekstblok te selecteren en op deze knop te klikken wordt het pop-up scherm opgeroepen met de huidige waarden ingevuld. Deze kunnen dan aangepast worden. Pas XSLT tekstblokken altijd op deze manier aan.

**XSLT code bekijken**

De XSLT code die gegenereerd wordt kan bekeken worden door op de **Voorvertoning (oog)** knop te klikken. De XSLT wordt dan niet bewaard naar het broncode tabblad.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__