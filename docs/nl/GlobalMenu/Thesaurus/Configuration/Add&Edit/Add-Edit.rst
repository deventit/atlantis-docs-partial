
Invoer Configuratie
^^^^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Extern**: Geeft aan of het topconcept voor dit veld afkomstig is uit een externe thesaurus. Als dit veld is aangevinkt, komt een extra veld **Selecteer externe thesaurus** beschikbaar voor de keuze van de externe thesaurus waar het topconcept uit gekozen wordt.
-  **Selecteer het topconcept**: Selecteer hier het topconcept uit de boomstructuur van de thesaurus.
-  **Kolom naam**: Selecteer de kolom uit de lijst van tabellen. De lijst van tabellen wordt gefilterd door een filterwaarde in te voeren boven de tabel. Er wordt steeds een groep van tabellen ingeladen en gepresenteerd met de knop **Laad meer gegevens**. Met de **+** knop voor een tabel wordt deze uitgeklapt voor presentatie van de kolommen. Hier kan een kolom geselecteerd worden door deze aan te klikken. De tabelnaam wordt dan ook direct ingevuld.
-  **Metadata model**: Hier wordt het metadata model geselecteerd waarvoor de veldtoewijzing van toepassing is. Als hier geen metadata model wordt geselecteerd, dan geldt de veldtoewijzing voor alle formulieren waarop dit veld gebruikt wordt.
-  **Tabelnaam**: Dit veld wordt automatisch gevuld als de kolom geselecteerd is.
-  **Negeer verbatim**: Door dit aan te vinken zal de ingevoerde waarde overschreven worden met de term van het geselecteerde concept uit de thesaurus. Daarnaast moet de ingevulde waarde altijd overeenkomen met een concept term uit de thesaurus. Is dit niet aangevinkt, dan mag in het veld een willekeurige term ingevuld worden.
-  **Nieuwe toevoegen**: Als dit veld is aangevinkt dan mogen er nieuwe termen aan de thesaurus toegevoegd worden. Dit zal ervoor zorgen dat wanneer een niet-bestaande term in het veld wordt ingevoerd, er een bewaarmogelijkheid geboden wordt. Bij het bewaren moet dan aangegeven worden wat de bovenliggende knoop in de thesaurus is. Dit is niet mogelijk bij een koppeling met concepten uit een externe thesaurus.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__