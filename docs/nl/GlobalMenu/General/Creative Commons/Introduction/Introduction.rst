
Creative Commons
----------------

Beheer Creative Commons
^^^^^^^^^^^^^^^^^^^^^^^

Creative Commons is een lijst met Creative Commons rechten die toegekend kunnen worden aan auteursrechthouders. 

Bij het selecteren van de optie voor Creative Commons uit het Globale Menu wordt een paneel geopend. Hierin wordt een lijst gepresenteerd van de Creative Commons rechtendefinities.

**Filteren**

De lijst van Creative Commons rechtendefinities kan gefilterd worden door in het "Zoek hier" veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen en wijzigen**

Een Creative Commons rechtendefinitie wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van de Creative Commons rechtendefinities ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de Creative Commons rechtendefinitie gewijzigd worden.


**Verwijderen**

Met het **prullenbak** pictogram wordt een Creative Commons rechtendefinities, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Auteursrechthouders <../Concepts/Concepts.html#auteursrechthouders>`__