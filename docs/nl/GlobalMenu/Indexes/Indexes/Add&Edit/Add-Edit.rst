
Invoer Toegangen
^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Titel**: De titel van de toegang.
-  **Plaats**: Aanduiding van de plaats. Dit betreft de plaats waar de met de toegang gekoppelde objecten betrekking op heeft.
-  **Soort Nadere Toegang**: tot welk type toegang dit behoort. Dit is een selectie uit de beschikbare **soorten nadere toegangen**.
-  **Startdatum**: De startdatum waar de met de toegang gekoppelde objecten betrekking op heeft. Hier wordt een volledige datum, alleen maand en jaar of alleen jaar ingevoerd.
-  **Einddatum**: De einddatum waar de met de toegang gekoppelde objecten betrekking op heeft. Hier wordt een volledige datum, alleen maand en jaar of alleen jaar ingevoerd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__