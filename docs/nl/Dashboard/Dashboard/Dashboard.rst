
Dashboard
*********

De Configuratie Cockpit start met het Dashboard. Dit biedt de meest gebruikte functies aan:

-  **Content Management Systeem**: voor het configureren van metadata modellen
-  **Keuzelijsten**: voor het definiëren van keuzelijsten op velden van metadata modellen
-  **Autorisatie**: voor het toekennen van rechten aan gebruikers
-  **Categorieën**: voor het definiëren van categoriesystemen.
-  **Rapporten**: voor het samenstellen van rapporten, overzichten en
   etiketten
-  **Zoekingangen**: voor het definiëren van zoekingangen voor de beheer- en publieksomgeving

Door op het desbetreffende vlak te klikken wordt de functie gestart.

De overige functies zitten in het **Globale Menu**. Deze is te vinden in de rechterbovenhoek (de vier vierkantjes).

Er kan altijd teruggekeerd worden naar dit schermdoor de op **afbeelding** in de linkerbovenhoek te klikken. Deze afbeelding wordt ingesteld via het toegepaste **Thema**.

**Zie ook**: `Algemene Bedieningselementen <../General/General.html#MainGeneral>`__
