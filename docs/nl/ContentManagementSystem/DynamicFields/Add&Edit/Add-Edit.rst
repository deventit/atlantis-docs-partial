
Invoer Dynamisch Veld
~~~~~~~~~~~~~~~~~~~~~

Voor het toevoegen of wijzigen van een Dynamisch veld zijn in dit paneel de volgende velden beschikbaar:

-  **Tabel**: Selecteer hier de tabel waar het Dynamisch Veld aan toegevoegd moet worden. Dit is alleen beschikbaar bij een nieuw Dynamisch Veld.
-  **Kolom naam**: Geef de naam van het Dynamisch Veld. Er zijn geen spaties toegestaan. Dit is alleen beschikbaar bij een nieuw Dynamisch Veld.
-  **Veld type**: Selecteer hier het type van het veld. Dit geeft de keuze uit:

       -  **String**: Tekstveld. Hierin kunnen alphanumerieke en speciale karakters in opgenomen worden.
       -  **Bool**: Bool staat voor Boolean en dit geeft een Ja/Nee of aanvinkveld. Dit veld is altijd één teken lang.
       -  **Datum**: Datumveld. Dit veld is altijd acht tekens lang (jjjjmmdd).
       -  **Numeriek**: Dit is een numeriek veld en kan alleen cijfers bevatten. Het is niet mogelijk om hier getallen met komma’s in te voeren. Dit veld heeft een maximale lengte van 38 tekens.
       
-  **Lengte veld**: Het aantal tekens van het veld.
-  **Hulp tekst**: De helptekst voor dit veld. Deze wordt gepresenteerd in formulieren als de muis over het veld wordt bewogen. De maximale lengte is 50 tekens.
-  **Koptekst**: Deze tekst wordt gebruikt als het veld toegepast wordt als koptekst voor tabellen. De maximale lengte is 50
   tekens.
-  **Label tekst**: De tekst die weergegeven wordt voor het veld. De maximale lengte is 50 tekens.
-  **Fouttekst**: Deze tekst die wordt weergegeven in formulieren als een foutieve waarde wordt ingevoerd of als verplicht veld niet is ingevoerd. De maximale lengte van dit veld is 50
   tekens.

  **Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Metadata modellen <../Concepts/Concepts.html#metadata-modellen>`__
