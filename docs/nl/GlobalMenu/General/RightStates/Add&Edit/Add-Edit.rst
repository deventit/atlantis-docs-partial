
Invoer rechten statussen
^^^^^^^^^^^^^^^^^^^^^^^^

Voor het wijzigen van een rechten status zijn in dit paneel de volgende velden beschikbaar:

-  **Beschrijving**: Omschrijving van de rechten status.
-  **Segment**: Selectie van het segment dat toegevoegd wordt aan het object.
-  **Hiërarchisch toepassen**: Vlag die aangeeft of toevoeging van het segment ook op hiërarchisch onderliggende objecten toegepast dient te worden.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Autorisatie <../Concepts/Concepts.html#autorisatie>`__