
Dynamische Tabellen
~~~~~~~~~~~~~~~~~~~

Aan de tabellen van de metadata modellen worden tabellen toegevoegd via Dynamische Tabellen. Deze tabellen komen op dezelfde manier beschikbaar als de standaard tabellen. Daarmee kunnen ze toegepast worden bij de definitie van Content Management Velden, in rapportages en in formulieren.

Bij het selecteren van de optie voor Dynamische Tabellen bij het metadata model wordt een paneel aan de rechterkant geopend. Hierin wordt een lijst gepresenteerd van de gedefinieerde Dynamische Tabellen.

**Filteren**

De lijst van Dynamische Tabellen kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen**

Een Dynamische Tabel wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel waarin de volgende informatie ingevoerd wordt:

- **Bovenliggende tabel**: Selectie van de bovenliggende tabel. 
- **Naam**: Naam van de dynamische tabel.


**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Metadata modellen <../Concepts/Concepts.html#metadata-modellen>`__
