
Uitvoerformaten
^^^^^^^^^^^^^^^

In dit paneel worden de uitvoer formaten voor een Open data Set gedefinieerd. Een uitvoerformaat definieert de bestandsextensie van het uitvoerbestand en de opbouw van dit bestand. De opbouw wordt gedefinieerd via de XSLT-Editor voor de **Kop**, **Inhoud** en **Voet** van het bestand.

**Filteren**

De lijst van uitvoerformaten kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een uitvoerformaat wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de tabel waarin de **Naam** van het uitvoerformaat en de **Extensie** van het uitvoerbestand ingevoerd worden. De definitie van wordt bewaard met de **Opslaan** knop. De invoer wordt geannuleerd met de “**Reset**” knop. Hiermee worden de veldwaarden teruggedraaid naar de veldwaarden bij het openen van dit scherm of de laatst bewaarde veldwaarden. 

**Opbouw van het uitvoerbestand**

Met het **XSLT-Editor** pictogram wordt de keuze gegeven voor de **Kop**, **Inhoud** of **Voet** van het uitvoerbestand. Keuze van één van deze opties opent de XSLT-editor in een paneel aan de rechterzijde. Hiermee wordt de opbouw van het uitvoerbestand gedefinieerd.

**Verwijderen**

Met het **prullenbak** pictogram wordt het uitvoerformaat, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Open data <../Concepts/Concepts.html#open-data>`__