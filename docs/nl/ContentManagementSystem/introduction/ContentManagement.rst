
Content Management Systeem
**************************

Metadata Modellen
~~~~~~~~~~~~~~~~~

Beheer Metadata Modellen
------------------------

De licentiesamenstelling bepaalt via de in gebruik zijnde modules welke metadata modellen beschikbaar zijn. Een metadata model is een samenstelling van tabellen en velden voor de registratie van de objectinformatie. Via het Content Management Systeem vindt op de metadata modellen configuratie plaats van:

-  Naamgeving en relaties tussen metadata modellen
-  Annotatievelden voor de bezoeker
-  Formulieren voor invoer en mutatie 
-  Eigen velden
-  Velden voor zoeken, presenteren zoekresultaten, sorteringen, filters, locaties, relevantie, etc.
-  Informatie-uitwisseling met een E-Depot

Welke mogelijkheden er beschikbaar zijn is afhankelijk van de licentiesamenstelling:

- Annotatie velden en Annotatie soorten: via de dienst **Annotaties**
- Formulieren: het zelf samenstellen en aanpassen hiervan via de dienst **Schermbouwer**
- Eigen velden en tabellen: via de dienst **Dynamische Velden**
- E-Depot: via de dienst **E-Depot**
- OAI-PMH Expressies: via **OAI-PMH harvester** licenties

**Metadata modellen**

Het scherm voor het Content Management Systeem presenteert een paneel aan de linkerzijde. Dit geeft een overzicht van de beschikbare metadata modellen. Deze lijst kan  gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast. 

**Functies op metadata modellen**

Door de muis over een metadata model te bewegen komt een menu beschikbaar. Dit zijn mogelijkheden voor configuratie per metadata model. Dit betreft:

-  **Relatiemodel**: Dit presenteert een grafische weergave van het metadata model met relaties naar andere metadata modellen. 
-  **Annotatie velden**: De definitie van de beschikbare velden voor de bezoekervoor  het maken van annotaties.
-  **Formulieren**: Definitie van formulieren voor invoer en mutatie.
-  **Dynamische Velden**: De definitie van eigen velden die toegevoegd worden aan het metadata model.
-  **Content Management Velden**: De definitie van velden voor zoeken, presenteren zoekresultaten, sorteringen, filters, locaties, relevantie, etc.
-  **E-Depot**: Definitie van de informatie-uitwisseling met het E-Depot.
-  **metadata modellen**: Definitie van metadata modellen.
-  **Soorten Annotaties**: Definitie van soorten van annotaties.
-  **OAI-PMH expressies**: Definitie van expressies voor OAI-PMH harvesting.
-  **Rechten statussen**: Definitie  van rechten statussen.

**Filteren**

De lijst van metadata modellen wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een metadata model wordt toegevoegd door in het paneel met metadata modellen op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit maakt een nieuw paneel aan de rechterzijde met de gegevens van een metadata model beschikbaar voor invoer.

**Muteren**

Met het “\ **potlood**\ ” pictogram wordt een een nieuw paneel aan de rechterzijde met de gegevens van een metadata model geopend voor invoer.

**Verwijderen**

Een metadata model wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel. Het metadata model mag daarvoor niet in gebruik zijn.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__