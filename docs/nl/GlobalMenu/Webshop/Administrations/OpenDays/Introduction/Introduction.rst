
Open dagen
----------

Met Open dagen worden de dagen van de week aangegeven waarop de studiezaal geopend is. Met **Gesloten dagen** is voor de regulier geopende dagen de studiezaal alsnog gesloten. 

Voor het definiëren wordt een paneel aan de rechterzijde geopend met een tabel van weekdagen. Met aanvinkvelden wordt aangegeven op welke dagen de studiezaal geopend is.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__