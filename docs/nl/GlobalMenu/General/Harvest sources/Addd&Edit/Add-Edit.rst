
Invoer Harvest bronnen
^^^^^^^^^^^^^^^^^^^^^^

Voor het wijzigen van een harvest bron zijn in dit paneel de volgende velden beschikbaar:

-  **Website**: URL naar de website en OAI-PMH toegang op deze website.
-  **Naam**: Omschrijving van de harvest bron.
-  **Adapter**: De adapter van Atlantis die de OAI-PMH koppeling realiseert.
-  **Import template**: XML template die de geïmporteerde data omzet naar Atlantis.
-  **Parameters**: Extra parameters voor de OAI-PMH toegang.
-  **XML schema**: XML schema waar de geïmporteerde data aan moet voldoen.
-  **Volgnummer**: Volgnummer van de harvest bron.
-  **Actief**: Vlag die aangeeft of de harvest bron in gebruik is.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `OAI-PMH <../Concepts/Concepts.html#oai-pmh>`__
