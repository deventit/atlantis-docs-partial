
Knoop splitsen
^^^^^^^^^^^^^^

Voor het splitsen van een knoop wordt van één knoop twee knopen gemaakt. 
  
Aan de rechterkant van de Verkenner komen de eigenschappen voor de nieuwe knoop beschikbaar. Hier worden een aantal tabellen gebruikt met de volgende functies:

- *Toevoegen*: Een nieuwe regel wordt toegevoegd door in de tabel op het **+** pictogram (nieuwe toevoegen) te klikken. 
- *Wijzigen*: Met het **potlood** pictogram komt de regel in de tabel beschikbaar voor wijzigen. 
- *Verwijderen*: Met het **prullenbak** pictogram wordt de regel, na bevestiging, verwijderd.
- *Bewaren*: Met de **Klaar** knop wordt de regel bewaard.
- *Annuleren*: Met de **Annuleer** knop worden de mutaties in de regel niet bewaard.

De eerste tabel definieert de term in verschillende talen. Dit geeft de volgende velden:
-  **Taal**: Selectie van de taal van de term
-  **Term**: De term voor de geselecteerde taal
-  **Voorkeur**: Geeft aan of de term de voorkeursterm is. Ieder taal moet één voorkeursterm hebben.
-  **Standaard**: Geeft aan of de term de standaardterm is. Ieder taal moet één standaardterm hebben.
-  **Volgorde**: Volgnummer van de term. Voor het presenteren van de termen in de gewenste volgorde.

De tweede tabel definieert aantekeningen in verschillende talen. Dit geeft de volgende velden:

-  **Taal**: Selectie van de taal van de aantekening
-  **Aantekening**: De aantekening voor de geselecteerde taal

Vervolgens worden de volgende eigenschappen aangegeven:

-  **Type knoop**: Selecteer hier het type van de knoop.
-  **Volgnummer**: Volgnummer van de knoop voor presentatie in de boom binnen de bovenliggende knoop. Deze wordt automatisch bepaald bij het aanmaken, verslepen en verplaatsen.  
-  **Concept ID**: Unieke identificatie van de knoop. Wordt automatisch gegenereerd.
-  **Details**: Als een objecten is gekoppeld aan de knoop, dan wordt de link naar het object hier gepresenteerd.
-  **Segmenten**: De segmenten die zijn toegewezen. 

De derde tabel geeft de relaties weer binnen de thesaurus voor deze knoop via de volgende velden:

-  **Semantisch**: Selecteer hier de semantische relatie die van toepassing is.
-  **Relatiemodel**: Selecteer hier de richting van de relatie. Bv. bij een hiërarchische relatie wordt gekozen voor boven- of onderliggend.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__