
Modules
-------

In dit paneel worden de modules uit de licentie voorzien van een eigen naam. Dit paneel bestaat dan ook uit twee tabellen. De eerste tabel presenteert de beschikbare modules, de tweede tabel de standaard module van Atlantis die hierbij hoort. 

**Filteren**

De lijst van modules wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een module wordt toegevoegd door in de tabel met modules op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst met de volgende velden:

-  **Naam**: De naam voor een module
-  **Basis-URL Publiek**: De basis URl voor het publiek indien deze niet standaard is.
-  **Publiek**: Vlag of deze module voor het publiek beschikbaar is.

Voor een geselecteerde module kan in de tweede tabel een standaard module worden geselecteerd. 

**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij voor aanpassing.

**Verwijderen**

Een module wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel.

De tweede tabel geeft bij een module behorende standaard module. 

**Filteren**

De lijst van standaard modules wordt gefilterd worden door in het “\ **Zoek hier**\ ” veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen**

Een standaard module wordt toegevoegd door in de tabel met standaard modules op het “\ **+**\ ” pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuwe regel in de lijst met de volgende velden:

-  **Naam**: De naam voor een module. Dit veld geeft een lijst van beschikbare standaard modules.

**Muteren**

Met het “\ **potlood**\ ” pictogram komt de regel vrij voor aanpassing.

**Verwijderen**

Een standaard module wordt verwijderd met het “\ **prullenmand**\ ”
pictogram in de desbetreffende regel.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__
