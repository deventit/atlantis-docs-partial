
Invoer Relatie
~~~~~~~~~~~~~~

Met dit paneel wordt een relatie tussen 2 metadata modellen toegevoegd of gewijzigd. Er is een onderscheid te maken tussen het linker- en rechterdeel van het paneel, voor beide zijden van de relatie. De linkerzijde wordt aangemerkt als zijnde "vooruit", de rechterzijde als "terug". Dit heeft te maken met de richting van de relatie. Vooruit betekent "onderliggend", terug betekent "bovenliggend". 

Dit paneel biedt de volgende informatie aan voor invoer en wijziging:

-  **Metadata model**: Hier wordt voor de linker- en rechterkant een metadata model geselecteerd, waarbij het metadata model links onderliggend en rechts bovenliggend is.
-  **Tabblad vooruit en terug**: Selectie van een tabblad voor onderliggende en bovenliggende objecten. Dit betekent dat als een tabblad voor onderliggende objecten wordt geselecteerd, in de beheeromgeving vanuit een bovenliggend object de gerelateerde objecten via deze relatie via dit tabblad worden gepresenteerd. Wordt geen tabblad geselecteerd, dan worden de gerelateerde objecten via het verzameltabblad "Relaties" gepresenteerd. In de Configuratie Cockpit is in het Globale Menu een optie "Tabbladen" beschikbaar om tabbladen te definiëren.
-  **URL vooruit en terug**: Hier worden URL's ingevoerd die worden aangeroepen als een gerelateerd object via deze relatie wordt gemuteerd.
-  **Relatienaam vooruit en terug**: De naam van de relatie bezien vanuit objecten van het gerelateerde metadata model. Relaties zijn bi-directioneel. Voor iedere richting heeft de relatie een naam om deze te benoemen. Bv. een relatie tussen ouder en kind zal vanuit de ouder de naam "Kinderen" en vanuit een kind de naam "Ouder" hebben.
-  **Maximaal nummervoor uit en terug**: Dit geeft het maximaal aantal te koppelen objecten aan. Een waarde van 0 betekent een onbeperkt aantal objecten die gerelateerd kunnen worden. Bv. bij een ouder (rechts) en kinderen (links) zal het maximaal nummervoor uit 0 zijn en terug 2 (voor een vader en een moeder).
-  **EditGrid template vooruit**: Selectie van een EditGrid template voor bulkgewijs beschrijven van gerelateerde lager gelegen objecten.
-  **EditGrid template terug**: Selectie van een EditGrid template voor bulkgewijs beschrijven van gerelateerde hoger gelegen objecten.
-  **Volume**: Indien het een relatie naar multimedia bestanden betreft, kan hier het volume voor de opslag van de bestanden gekozen worden.
-  **Extra velden om binnen deze relatie te beschrijven**: Hier kan een opsomming gegeven worden van de velden die binnen deze relatie extra beschreven kunnen worden. 

Naast deze gegevens zijn nog een aantal instellingen via aanvinkvelden aan te geven:

-  **Is hiërarchisch**: geeft aan de relatie tussen objecten van het metadata model links en rechts hiërarchisch is. 
-  **Is Multimedia**: deze wordt aangevinkt als de relatie van/naar een metadata model van multimediale objecten loopt. Deze staat dus aangevinkt, onafhankelijk van of het metadata model voor multimediale objecten boven- of onderliggend is. Deze relatie wordt op een speciale manier behandeld omdat via deze relatie multimediale objecten worden gepresenteerd. Hiervoor komt altijd een eigen tabblad beschikbaar.
-  **Is eigenaar**: deze wordt aangevinkt als de relatie een eigenaarsrelatie is. Ook hier staat deze aangevinkt op een eigenaarsrelatie vanuit beide zijden van de relatie.
-  **Verberg de relatie in het relatietabblad**: Vink deze aan indien de objecten rechts in het relatietabblad van de objecten links verborgen moeten worden.
-  **Kopie vooruit**: Als deze aangevinkt wordt, dan worden de onderliggende relaties bij het maken van een kopie van het via deze relatie bovenliggend object mee gekopieerd. De onderliggende objecten worden dan ook onderliggende objecten van de kopie van het bovenliggend object.
-  **Kopie terug**: Als deze aangevinkt wordt, dan worden de bovenliggende relaties bij het maken van een kopie van het via deze relatie onderliggend object mee gekopieerd. De bovenliggende objecten worden dan ook bovenliggende objecten van de kopie van het onderliggend object.
-  **Index vooruit**: Als deze aangevinkt wordt, dan worden de onderliggende objecten bij het muteren en bewaren van het via deze relatie bovenliggend object opnieuw geïndexeerd. 
-  **Index terug**: Als deze aangevinkt wordt, dan worden de bovenliggende objecten bij het muteren en bewaren van het via deze relatie onderliggend object opnieuw geïndexeerd. 
-  **Gebruik voor gedetailleerde navigatie**: Dit geeft aan dat wanneer een onderliggend object wordt geraadpleegd in de beheer- Of publieksomgeving het mogelijk is om door alle onderliggende objecten te bladeren. Bv. bij de pagina van een boek: Als een pagina wordt gepresenteerd, dan is het mogelijk om vanaf daar door alle pagina's van het boek te bladeren. 

**Specifieke namen**
Hiermee worden alternatieve namen voor de relatie gedefinieerd als namen vooruit en terug combinaties. Op een relatie wordt de standaard naam voor de relatie vooruit en terug hiermee overruled door een combinatie te selecteren.

**Kolom ID's**
Voor het opslaan van extra eigenschappen bij een relatie wordt via deze Kolom ID's aangegeven in welke velden dit gebeurt.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Relaties <../Concepts/Concepts.html#relaties>`__