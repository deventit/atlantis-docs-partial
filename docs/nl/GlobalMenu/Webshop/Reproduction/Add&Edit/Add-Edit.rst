
Invoer Reproductiesoorten
^^^^^^^^^^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Omschrijving**: Omschrijving van de reproductiesoort.
-  **Video formaat**: Selecteer hier, ingeval het een reproductie van bewegend beeld betreft, het video formaat. Alleen van toepassing indien het een digitale reproductie betreft.
-  **Bedrag**: Het bedrag voor de reproductie dat per stuk in rekening wordt gebracht (IN centen).
-  **Bevat metadata**- Geeft aan of een gecombineerd bestand met metadata en het object geleverd dient te worden afgeleverd. Dit gebeurt dan in MXF formaat. Alleen van toepassing indien het een digitale reproductie betreft.
-  **Is digitaal**: Geeft aan of het een digitale reproductie betreft.
-  **Is Hoge Resolutie**: Geeft aan of het originele bestand geleverd dient te worden. Alleen van toepassing indien het een digitale reproductie betreft.
-  **Directe Download**: Geeft aan of het bestand beschikbaar is voor directe download. Alleen van toepassing indien het een digitale reproductie betreft.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__