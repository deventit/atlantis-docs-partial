
Mediatypes
----------

Beheer Mediatypes
^^^^^^^^^^^^^^^^^

In dit paneel worden de Mediatypes gedefinieerd. Mediatypes definiëren welk icoon wordt gepresenteerd voor een digitaal bestand dat geen afbeelding is. Voor afbeeldingen wordt een icoon van de afbeelding gepresenteerd. Ook het standaard icoon dat gebruikt wordt als verder geen ander icoon bepaald kan worden wordt hier gedefinieerd.

Het icoon wordt bepaald volgens de onderstaande regels:

- is het een afbeelding, dan een icoon van de afbeelding
- is het geen afbeelding:
    - is er een icoon gedefinieerd voor de bestandsextensie bij de Mediatypes, dan dit icoon gebruiken
    - is er geen icoon gedefinieerd voor de bestandsextensie bij de Mediatypes, dan het icoon gebruiken dat aangemerkt staat als "standaard" bij de Mediatypes.

**Filteren**

De lijst van Mediatypes kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een Mediatype wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van het Mediatype ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van het Mediatype gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt het Mediatype, na bevestiging, verwijderd. 

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Mediatypes <../Concepts/Concepts.html#mediatypes>`__