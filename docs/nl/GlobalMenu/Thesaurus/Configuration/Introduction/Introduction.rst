
Configuratie
------------

Beheer Configuratie
^^^^^^^^^^^^^^^^^^^

In dit paneel worden de velden waarmee een koppeling met een thesaurus concept is gedefinieerd weergegeven. Als een veld van een metadata model een koppeling heeft met een thesaurus concept, dan wordt in formulieren de benodigde functies toegevoegd om deze koppeling aan te leggen of te raadplegen.

**Filteren**

De lijst van velden kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een veldtoewijzing wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van de veldkoppeling ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de veldkoppeling gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de veldtoewijzing, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__