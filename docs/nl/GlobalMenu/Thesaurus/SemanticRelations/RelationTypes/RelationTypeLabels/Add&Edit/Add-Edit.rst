
Invoer Relatie Type Labels
^^^^^^^^^^^^^^^^^^^^^^^^^^

Voor het wijzigen van een relatie type label zijn in dit paneel de volgende velden beschikbaar:

-  **Label voor term 1 naar term 2**: Naam van de relatie voor het betreffende relatie type van term 1 naar term 2 (vooruit).
-  **Label voor term 2 naar term 1**: Naam van de relatie voor het betreffende relatie type van term 2 naar term 1 (terug).
-  **Taal**: Selectie van de taal voor de relatie type labels.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__