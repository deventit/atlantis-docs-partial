
Invoer Sets
^^^^^^^^^^^

Voor het toevoegen of wijzigen wordt een Set aan de rechterzijde geopend met de volgende velden:

-  **Naam**: De naam van de set. 
-  **Beschrijving**: Beschrijving van de set.
-  **Set specificatie**: Nadere specificatie van de set.
-  **Volledig zonder cache**: Vlag die aangeeft of met of zonder cache wordt gewerkt.

Voor een nieuwe Set moeten deze waarden eerst worden bewaard voordat de onderstaande velden beschikbaar komen.

Vervolgens zijn 3 soorten van verzamelingen van objecten toe te wijzen:
- **Metadata model**: Alle objecten van een metadata model.
- **Werkset**: De objecten van een werkset.
- **Dataset**: De objecten van een dataset.

Per soort van verzameling wordt uit de lijst van waarden een keuze gemaakt. Meer dan één waarde selecteren is mogelijk. Met het **x** van een waarde wordt deze uit de selectie verwijderd. 

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `OAI-PMH <../Concepts/Concepts.html#oai-pmh>`__, `Datasets <../GlobalMenu/GlobalMenu.html#datasets>`__
