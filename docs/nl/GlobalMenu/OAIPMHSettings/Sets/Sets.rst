
Sets
----

Beheer Sets
^^^^^^^^^^^

Met Sets kan een verzameling van objecten aangeboden worden voor harvesting via het OAI-PMH protocol. Een set wordt toegewezen aan een metadata model. 

In dit paneel worden de sets gedefinieerd. 

**Filteren**

De lijst van sets kan gefilterd worden door in het **Zoek hier** veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.  

**Toevoegen en wijzigen**

Een set wordt toegevoegd door in het paneel op het **+** pictogram (nieuwe toevoegen) te klikken. Dit geeft een nieuw paneel aan de rechterzijde waarin de gegevens van de set ingevoerd worden. Met het **potlood** pictogram wordt een nieuw paneel aan de rechterzijde geopend waarmee de gegevens van de set gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de set, na bevestiging, verwijderd. 

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `OAI-PMH <../General/General.html#MainGeneral>`__

