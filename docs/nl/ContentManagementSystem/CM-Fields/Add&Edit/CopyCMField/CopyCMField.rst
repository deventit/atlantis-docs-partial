
Kopiëren Content Management Veld
--------------------------------

Dit scherm geeft de mogelijkheid om een content management veld te kopiëren naar een metdata model. Dit geeft de volgende velden:

-  **Bestemming Content Management Veld**: De naam van het content management veld als kopie van het geselecteerde content management veld.
-  **Metadata model**: Naam van het metadata model van het bestemming content management veld. Dit geeft alleen de keuze uit de metadata modellen met dezelfde toplevel tabel.
