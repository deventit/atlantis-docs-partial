
Kortingen
---------

Kortingen worden toegepast bij de bestellingen voor reproducties. Kortingen worden toegekend via een vast bedrag of een percentage per reproductiesoort. Kortingen kunnen daarnaast op basis van bestelde aantallen gestaffeld worden.

Voor het definiëren van kortingen wordt een paneel aan de rechterzijde geopend met een tabel van kortingen. Een korting wordt gedefinieerd via de volgende velden:

-  **Reproductiesoort**: De reproductiesoort waarvoor de korting geldt.
-  **Van**: Het aantal vanaf waarop de korting toegekend wordt.
-  **Tot**: Het aantal tot waarop de korting toegekend wordt.
-  **Vast bedrag**: Het vaste bedrag dat als korting toegekend wordt.
-  **Percentage**: Het percentage dat als korting toegekend wordt.

**Toevoegen en wijzigen**

Een korting wordt toegevoegd door in de tabel op het **+** pictogram (nieuwe toevoegen) te klikken. Hiermee komt een nieuwe regel beschikbaar waarin de gegevens van de korting ingevoerd worden. Met het **potlood** pictogram komt de regel beschikbaar waarin de gegevens van de korting gewijzigd worden.

**Verwijderen**

Met het **prullenbak** pictogram wordt de korting, na bevestiging, verwijderd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__