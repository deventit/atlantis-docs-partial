
Inleiding
*********

Dit is de gebruikersdocumentatie van de Configuratie Cockpit van Atlantis. De Configuratie Cockpit is dé omgeving voor de applicatiebeheerdervoor  de inrichting en configuratie van de Atlantis omgeving.

Deze omgeving biedt organisaties de mogelijkheid om zelf tal van zaken te regelen zonder tussenkomst van DEVENTit. Het **Beheerders** menu uit de beheeromgeving wordt hiermee vervangen. 

Omdat er zoveel mogelijkheden worden geboden bestaat ook het gevaar dat er functies stuk gemaakt kunnen worden. In de regel wordt deze omgeving beschikbaar gesteld na een diepgaande opleiding. Het gebruik is echtervoor  eigen risico. Herstel door DEVENTit valt **niet** onder het onderhoudscontract.

Deze documentatie bevat de contextgevoelige helpteksten die vanuit de bedieningsschermen van de Configuratie Cockpit wordt gepresenteerd. Daarnaast wordt waar nodig bredere informatie gegeven omtrent context en concepten. Dit object wordt continu uitgebreid en bijgewerkt.

Als er opmerkingen zijn over de documentatie, mail dit dan naar: **office@devenit.nl**. Bij voorbaat dank!

