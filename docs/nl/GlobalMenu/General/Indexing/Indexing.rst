
Indexeren
---------

Dit scherm is voor het (her)indexeren van objecten. Objecten die nog niet geïndexeerd zijn of aangemerkt zijn als "nog te indexeren" worden via dit scherm geïndexeerd.

Er kan één indexeersessie tegelijkertijd lopen. Om een indexeersessie te starten is het wachtwoord van de gebruiker "deventit" nodig of een "KeyForADay". Deze key wordt via de supportafdeling opgevraagd.

Op dit scherm zijn de volgende velden beschikbaar:

-  **Metadata modellen**: Selectie van de metadata modellen waarvoor de te indexeren objecten geïndexeerd dienen te worden. Als geen metadata modellen worden geselecteerd, dan worden de te indexeren objecten van alle metadata modellen geïndexeerd.
-  **Indexeer alleen CM-velden**: Als dit is aangevinkt worden alleen de content management velden van de te indexeren objecten geïndexeerd.
-  **Indexeer alleen documenten die nog niet zijn geïndexeerd**: Als dit is aangevinkt worden alleen de te indexeren objecten geïndexeerd. De content management velden worden overgeslagen.
-  **Wachtwoord**: Geef hier de KeyForADay of het deventit wachtwoord in. Met de KeyForADay kan alleen buiten werkuren geïndexeerd worden.

Met de **startknop** wordt het indexeren gestart. Als er al een indexeersessie loopt zal daar een melding van komen. Het indexeren wordt dan niet gestart.

Tijdens het indexeren wordt de voortgang gepresenteerd.


