
Invoer Statussen
^^^^^^^^^^^^^^^^^

Voor het wijzigen van een status zijn in dit paneel de volgende velden beschikbaar:

-  **Naam**: Naam van de status.
-  **Opmerking**: Omschrijving van de status
-  **Aantal dagen**: Aantal dagen waarvoor de openbaarheidsbeperking geldt.
-  **Aantal maanden**: Aantal maanden waarvoor de openbaarheidsbeperking geldt.
-  **Aantal jaren**: Aantal jaren waarvoor de openbaarheidsbeperking geldt.
-  **Einddatum**: De einddatum van de openbaarheidsbeperking.
-  **Niet bestelbaar**: Vlag die aangeeft of de objecten beschikbaar zijn voor bestellingen van reproducties.
-  **Niet aanvraagbaar**: Vlag die aangeeft of de objecten beschikbaar zijn voor aanvragen voor inzage.

Bij een openbaarheidsbeperking zijn de gekoppelde bestanden niet beschikbaar voor online inzage. Dit geldt voor de periode die daarvoor aangegeven is vanaf de datum van het object. Deze periode wordt aangegeven in dagen, maanden, jaren of een vaste einddatum.
Of de objecten gedurende deze periode beschikbaar zijn voor bestelling of aanvragen voor inzage wordt met de vlaggetjes hiervoor aangegeven.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__