
Knopen samenvoegen
^^^^^^^^^^^^^^^^^^

Voor het samenvoegen van knopen wordt een kopie van de thesaurusboom aan de aan de rechterkant geopend met daartussen een tabel waarin relaties worden aangegeven. Vervolgens kan er gekozen worden voor **Samenvoegen** of **Samenvoegen naar Equivalent**.

**Samenvoegen** betekent dat van twee knopen één nieuwe knoop wordt gemaakt. De geselecteerde knopen worden daarbij verwijderd. Alle informatie van de 2 knopen wordt in de nieuwe, samengevoegde knoop, overgenomen.

Na samenvoeging wordt een, nog niet opgeslagen, nieuwe knoop gepresenteerd waarin de informatie in aangepast kan worden. Er mag per taal bv. maar één voorkeursterm en één standaard term zijn. Als door samenvoegen daar nu 2 van zijn, kan dit aangepast worden.

Alle relaties van objecten naar de 2 samen te voegen knopen worden met de nieuwe knoop gelegd.

**Samenvoegen naar Equivalent** betekent dat de 2 geselecteerde knopen blijven bestaan. Er wordt een nieuw knoop samengesteld met de informatie uit de geselecteerde knopen. Daarnaast wordt een equivalentie relatie gelegd van de 2 knopen naar de nieuwe knoop. Na samenvoeging wordt een, nog niet opgeslagen, nieuwe knoop gepresenteerd waar de informatie in aangepast kan worden. 

De functies voor het samenvoegen worden verder geparameteriseerd  door aan te geven welke semantische relaties er mee moeten komen voor de nieuwe knoop. Dit gebeurt in de tabel tussen de bomen. Deze biedt de volgende functies:

- *Toevoegen*: Een nieuwe regel wordt toegevoegd door in de tabel op het **+** pictogram (nieuwe toevoegen) te klikken. 
- *Wijzigen*: Met het **potlood** pictogram komt de regel in de tabel beschikbaar voor wijzigen. 
- *Verwijderen*: Met het **prullenbak** pictogram wordt de regel, na bevestiging, verwijderd.
- *Opslaan*: Met de **Opslaan** knop wordt de regel bewaard.
- *Reset*: De invoer wordt geannuleerd met de “**Reset**” knop. Hiermee worden de veldwaarden teruggedraaid naar de veldwaarden bij het openen van dit scherm of de laatst bewaarde veldwaarden.

De relaties die meegenomen dienen te worden in de samenvoeging worden aangegeven via de velden:

-  **Semantisch**: Selecteer hier de semantische relatie die van toepassing is.
-  **Relatiemodel**: Selecteer hier de richting van de relatie. Bv. bij een hiërarchische relatie wordt gekozen voor boven- of onderliggend.

**Uitvoeren van de samenvoeging**

De samenvoeging wordt uitgevoerd via de knoppen **Samenvoegen** of **Samenvoegen naar Equivalent**. Dit geeft de presentatie van een nieuwe, nog niet opgeslagen, knoop. 

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__