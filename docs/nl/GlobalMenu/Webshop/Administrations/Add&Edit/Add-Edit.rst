
Invoer Administratie
^^^^^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen wordt een paneel aan de rechterzijde geopend met de volgende velden:

-  **Winkelwagen naam**: De naam van de Winkelwagen. 

-  **Ontvanger bestelling**: het emailadres van de ontvanger van de bestelling bij de organisatie.
-  **Afzender bestelling**: Het e-mailadres van de afzender van de e-mail aan de besteller.
-  **Onderwerp bestelling**: Het onderwerp van de e-mail aan de besteller en de organisatie.

-  **Ontvanger aanvraag**: Het e-mailadres van de ontvanger van de aanvraag bij de organisatie.
-  **Afzender aanvraag**: Het e-mailadres van de afzender van de e-mail aan de aanvrager.
-  **Onderwerp van de aanvraag**: Het onderwerp van de e-mail aan de aanvrager en de organisatie.

-  **Ontvanger aanvraag voor Scanning on Demand**: Het e-mailadres van de ontvanger van de aanvraag voor Scanning on Demand bij de organisatie.
-  **Afzender aanvraag voor Scanning on Demand**: Het e-mailadres van de afzender van de e-mail aan de aanvragervoor  Scanning on Demand.
-  **Onderwerp van aanvraag voor Scanning on Demand**: Het onderwerp van de e-mail aan de aanvragervoor  Scanning on Demand en de organisatie.

-  **Onderwerp van de SoD update notificatie**: Het onderwerp van de e-mail aan de aanvragervoor  Scanning voor een update notificatie.
-  **Onderwerp van de SoD afwijzing notificatie**: Het onderwerp van de e-mail aan de aanvragervoor  Scanning voor een afwijzing van de aanvraag notificatie.
-  **Onderwerp van de SoD aanvraag restitutie notificatie**: Het onderwerp van de e-mail aan de aanvragervoor  Scanning voor een notificatie van restitutie op de aanvraag.
-  **Onderwerp van de SoD afhandel notificatie**: Het onderwerp van de e-mail aan de aanvragervoor  Scanning voor een notificatie dat de aanvraag is afgehandeld.

-  **Maximum aantal in aanvraag**: Het maximum aantal items dat kan worden aangevraagd in één aanvraag.
-  **Maximum aantal in bestelling**: Maximum aantal items dat kan worden besteld in één bestelling.
-  **Maximum aantal in SoD aanvraag**: Maximum aantal items dat kan worden aangevraagd in één Scanning on Demand aanvraag.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__
