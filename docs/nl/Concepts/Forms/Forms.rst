
Formulieren
~~~~~~~~~~~

Een metadata model staat voor een beschrijvingsstructuur dat is ondergebracht in een relationele structuur van tabellen en velden. De beschrijvingsstructuur komt beschikbaar voor invoer en mutatie via formulieren. Voor het samenstellen van formulieren is de WYSIWYG formulierbouwer beschikbaar. 

**Rijen en kolommen**

Een formulier bestaat uit rijen en kolommen. Het aantal kolommen deelt het gepresenteerde formulier in in dat aantal kolommen, ieder met dezelfde breedte. De hoogte van een rij wordt bepaald door de hoogte van de inhoud van de cel die de meeste hoogte nodig heeft.

Een component dat op het formulier wordt geplaatst kan één of meer kolommen in beslag nemen.

**Formulieren en subformulieren**

Een formulier gaat over één tabel. Deze wordt geselecteerd bij het aanmaken. Vervolgens worden velden uit deze tabel op het formulier geplaatst. Als het formulier velden dient te bevatten van een andere tabel uit de beschrijvingsstructuur, dan wordt daarvoor een eigen formulier gemaakt. Dit formulier wordt dan als subformulier (aggregatie) opgenomen in het formulier. De aggregatie is een element dat op het formulier gesleept wordt. Vervolgens wordt aangegeven welk subformulier hiermee geplaatst wordt.

Een aggregatie kan enkelvoudig of meervoudig op een formulier geplaatst worden. Bij een enkelvoudige plaatsing wordt het subformulier in zijn geheel geplaatst op het formulier. Bij meervoudige plaatsting wordt een tabel geplaatst met een regel record van de aggregatie. bij het toevoegen of muteren van een record van de aggregatie klapt deze uit zodat het formulier wordt gepresenteerd. Voor de tabel wordt aangegeven welke velden van de subtabel gepresenteerd worden als een record van de aggregatie ingeklapt in de tabel wordt gepresenteerd.

**Gelinkte formulieren**

Formulieren die bij andere metadata modellen met dezelfde hoofdtabel gedefinieerd zijn, kunnen worden gelinkt aan het metadata model. Dit formulier is dan beschikbaar bij meerdere metadata modellen.

**Gekoppelde objecten**

Op een formulier worden gekoppelde objecten geplaatst door een metadata model element op het formulier te slepen. Vervolgens wordt de relatie geselecteerd waarmee de gekoppelde objecten gekoppeld zijn.

Gekoppelde objecten kunnen enkelvoudig of meervoudig op een formulier geplaatst worden. Bij een enkelvoudige plaatsing wordt een veld op het formulier geplaatst met functies voor het selecteren,nieuw object aanmaken en koppeling verbreken. Bij meervoudige plaatsting wordt een tabel geplaatst met een regel per gekoppeld object. Iedere regel bestaat uit een veld met functies voor het selecteren,nieuw object aanmaken en koppeling verbreken.

In het veld wordt een representatieve tekst geplaatst. Welke tekst dit is wordt bepaald door de selectie van een content management veld van het gekoppelde metadata model.

Gekoppelde objecten worden naast een mogelijke presentatie in een formulier ook weergegeven in de tabbladen van de relaties met objecten van andere metadata modellen. Een voordeel van het presenteren van gekoppelde objecten in een formulier is dat koppelingen met objecten via seriegewijs beschrijven in bulk toegekend kunnen worden.

**Tabbladen**

De rijen van een formulier kunnen ondergebracht worden in tabbladen. Hiervoor wordt een tabblad element op het formulier gesleept, benoemd en over de betreffende rijen geplaatst. Als met tabbladen wordt gewerkt is het zaak dat alle rijen in tabbladen ondergebracht zijn. De rijen die niet in tabbladen ondergebracht zijn zullen anders niet meer beschikbaar zijn voor invoer en mutatie.

**Veldgroepen**

De rijen van een formulier kunnen ondergebracht worden in veldgroepen. Hiervoor wordt een veldgroep element op het formulier gesleept, benoemd en over de betreffende rijen geplaatst.  Een veldgroep zal de betreffende rijen omkaderen met een naam in de linkerbovenhoek.

**Componenten**

Componenten zijn elementen waarmee informatie op een formulier kan worden geplaatst die geen onderdeel uitmaken van de beschrijvingsstructuur van een metadata model. Denk hierbij aan informatie omtrent generieke informatie, als audit informatie, veiligheidsniveaus, Scanning on Demand eigenschappen, etc. Dit is een bibliotheek van informatie die steeds verder uitgebreid wordt.

**Controls**

De controls die gebruikt worden voor invoer en mutatie kunnen niet aangegeven worden. Deze worden bepaald aan de hand van het type informatie. Een invoerveld voor (alpha)numeriek en datum velden, aanvinkvelden voor Ja/Nee, een kalender bij datumvelden, invoervelden met selectieknoppen bij stamgegevens, etc. 

**Eigenschappen van elementen**

Voor velden en gekoppelde objecten kan aangegeven worden of deze verplicht, alleen lezen of tekstblokken zijn. 

**Keuzelijsten en thesauruskoppelingen**

Als velden uit de beschrijvingsstructuur zijn voorzien van een keuzelijst of koppeling met een (interne of externe) thesaurus, wordt bij het plaatsen van deze velden op een formulier automatisch de keuzelijst of thesauruskoppeling toegevoegd. 

**Koppeling aan gebruikersgroepen**

Voor formulieren kan aangegeven worden voor welke gebruikersgroepen deze beschikbaar worden gesteld. Hiermee kan toegang op veldniveau geregeld worden. Gebruikers hebben op basis van lidmaatschap van gebruikersgroepen en daarmee gekoppelde formulieren alleen toegang tot die velden voor lezen, verplichte invoer die via de formulieren beschikbaar gesteld zijn.

**Responsive en bediening via het toetsenbord**

De formulieren die met de formulierbouwer zijn samengesteld zijn responsive. Dit betekent dat de presentatie zich aanpast aan de beschikbare breedte. Hiermee is een formulier te bedienen via PC/laptop, tablet en smartphone.

De formulieren zijn geoptimaliseerd voor bediening via het toetsenbord. Met de <TAB> toets wordt naar volgende velden en knoppen gesprongen worden, met de spatiebalk wordt een knop ingedrukt en velden waar stamgegevens, keuzelijsten of thesauri op van toepassing zijn worden suggesties gegeven voor selectie bij het intypen van de de eerste karakters.
