
Rapporten
*********

Met rapporten worden rapportages, overzichten en etiketten gedefinieerd en beschikbaar gesteld. Een rapport, overzicht of etiket wordt samengesteld via een **XSLT-editor**.

**Overzicht rapporten**

In het linkerpaneel wordt een overzicht gepresenteerd van de gedefinieerde rapporten.  

**Filteren**

De lijst wordt gefilterd door in het "Zoek hier" veld de filterwaarde in te voeren. Met het invoeren wordt het filter direct toegepast.

**Toevoegen en wijzigen**

Met het **+** pictogram boven het overzicht wordt een nieuw rapport toegevoegd. Met het **potlood** pictogram wordt een rapportdefinitie geopend voor wijziging. In beide gevallen wordt een paneel aan de rechterzijde geopend en klapt dit paneel automatisch dicht.

**Verwijderen**

Met het **prullenbak** pictogram wordt een rapport, na bevestiging, verwijderd.

**Opmaken van rapporten**

Met het **XSLT-Editor** pictogram wordt de keuze gegeven voor de **Kop**, **Inhoud** of **Voet** van het rapport. Keuze van één van deze opties opent de XSLT-editor in een paneel aan de rechterzijde. Hiermee wordt de opbouw van het rapport gedefinieerd.

**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__