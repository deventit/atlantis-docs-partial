
Kopiëren alle Content Management Velden
---------------------------------------

Dit scherm geeft de mogelijkheid om meerdere content management velden tegelijkertijd te kopiëren naar een ander metdata model. Dit geeft de volgende velden:

-  **Metadata model bestemming**: Naam van het metadata model waar de content management velden naartoe gekopieerd moeten worden. Hier wordt een keuze gegeven van metadata modellen met dezelfde toplevel tabel.


