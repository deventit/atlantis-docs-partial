
Invoer Creative Commons
^^^^^^^^^^^^^^^^^^^^^^^

Voor het toevoegen of wijzigen van een Creative Commons rechtendefinitie zijn in dit paneel de volgende velden beschikbaar:

-  **Creative Commons**: Voer hier de afkorting van het Creative Commons recht in.
-  **Beschrijving**: Geef hier de beschrijving van het Creative Commons recht
-  **Icoon**: Selecteer hier een icoon voor het Creative Commons recht.


**Zie ook**: `Algemene
Bedieningselementen <../General/General.html#MainGeneral>`__, `Creative Commons <..//GlobalMenu/GlobalMenu.html#invoer-creative-commons>`__
