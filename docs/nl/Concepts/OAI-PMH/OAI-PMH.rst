
OAI-PMH
~~~~~~~

OAI-PMH staat voor "Open Archives Initiative Protocol for Metadata Harvesting". Dit is een protocol voor het harvesten van metdata tussen systemen. Het systeem dat metadata harvest is een harvest server (harvester), het systeem dat de metadata levert is de harvest client (harvestee). Atlantis kan zowel een harvest server als client zijn.

Via OAI-PMH wordt de metadata in een afgesproken formaat, veelal in XML, uitgewisseld.

**Harvest server**

Als Atlantis ingericht wordt als een harvest server, dan worden daarvoor harvest bronnen ingericht. Een harvest bron is dan een client van een extern systeem dat metadata zal aanleveren. De metadata die aangeleverd wordt, zal worden vertaald naar beschrijvingsstructuren en gekoppelde bestanden van metadata model objecten in Atlantis. De gekoppelde bestande zullen dan veelal in de vorm van URL's naar het externe systeem geleverd en opgenomen zijn zodat deze niet bij het leverende systeem en in Atlantis opgeslagen worden.

Een extern systeem zal een URL als ingang beschikbaar stellen. De harvest bron wordt gekoppeld aan een adapter in Atlantis. Deze adapter zal in staat zijn om de metadata van de bron te vertalen naar metadata in Atlantis. Voor de bron kunnen extra parameters ingesteld worden, dit op aangeven van de bron. Voor de verwerking van de metadata kan naast de adapter een import template en een xml schema, ter controle van de metadata die aangeleverd wordt, ingesteld worden.

De geharveste metadata wordt ondergebracht in objecten van metadata modellen. Met deze data kan derhalve op dezelfde manier omgegaan worden als met de metadata die in Atlantis wordt beheerd. Dit betekent dat formulieren, rapporten, Content Management Velden, publicatie via de publieksomgeving, etc. onverkort toegepast kunnen worden.

De harvesting vindt periodiek plaats. Dit gebeurt door een scheduler die op ingestelde momenten een harvest start. Op een harvest bron zijn de harvest sessies die uitgevoerd zijn en eventuele fouten die daarbij opgetreden zijn te raadplegen.

**Harvest client**

Als Altantis ingericht wordt als een harvest client, dan wordt daarvoor een repository gedefinieerd. De repository definieert parameters die van belang zijn voor het protocol alsook de URL ingang voor een harvest server.

Via de URL ingang worden metadata prefixen gedefinieerd. Dit geeft aan in welk format de metadata beschikbaar gesteld zal worden. Naast de naam is dit een namespace en een XML schema aanduiding.

Op een metadata model dat metadata voor harvesting aanbiedt, worden per metadata prefix de expressies gedefinieerd voor de ListIdentifiers, ListRecords en GetRecord. Dit zijn aanroepen die de harvest server kan uitvoeren om de metdata te harvesten.

De metadata die wordt aangeboden voor harvesting wordt gedefinieerd via werksets of datasets. Een werkset bestaat uit een verzameling van objecten die in de collectiebeheeromgeving ondergebracht zijn in een werkset. Er kunnen naar believen werksets worden samengesteld. Een dataset is een verzameling van objecten als resultaat van een query op de database. 

Via de definitie van een set wordt aangegeven op welke metadata modellen de set betrekking heeft met daarbij de definitie van de bijbehorende werkset of dataset.





